��    (      \  5   �      p  &   q  
   �     �     �  	   �     �  '   �  	         
  �     :   �     �     �     �          
           ?     H     L  �   O  	   �     �     �     �  
     
             (     =  S   L  >   �     �     �     �            F   2  X   y  ;  �  (   	     7	     G	     V	     b	      v	  '   �	     �	     �	  �   �	  ?   �
  	   �
     �
     �
     �
     �
  ,        9     B     E  �   I     �     �          #     (  
   4     ?     W     l  ^   {  E   �           5     A     U     m  Y   �  f   �                                    %                                         #             $   !   (         	   
                             "            &                 '          Additional description and details at  Admin Sort Administrator Author Auto Sort Auto Sort Description Category Order and Taxonomy Terms Order Check our Contributor Did you find this plugin useful? Please support our work with a donation or write an article about this plugin in your blog with a link to our site Did you know there is an Advanced Version of this plug-in? Editor General General Settings Hide Items Order Updated Minimum Level to use this plugin Nsp-Code OFF ON Order Categories and all custom taxonomies terms (hierarchically) and child terms using a Drag and Drop Sortable javascript capability. Read more Save Settings Settings Saved Show Subscriber Taxonomies Taxonomy Order Taxonomy Terms Order Taxonomy Title This plugin can't work without javascript, because it's use drag and drop and AJAX. This will change the order of terms within the admin interface Total Posts Update global setting http://www.nsp-code.com https://www.nsp-code.com plugin which allows to custom sort all posts, pages, custom post types plugin which allows to custom sort categories and custom taxonomies terms per post basis PO-Revision-Date: 2024-09-04 16:20:24+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/4.0.1
Language: sv_SE
Project-Id-Version: Plugins - Category Order and Taxonomy Terms Order - Stable (latest release)
 Ytterligare beskrivning och detaljer på Admin-sortering Administratör Författare Sortera automatiskt Automatisk sorteringsbeskrivning Category Order and Taxonomy Terms Order Kolla vårt Bidragsgivare Tyckte du att det här tillägget var användbart? Stöd vårt arbete genom en donation eller skriv en artikel om detta tillägg i din blogg med en länk till vår webbplats Vet du om att det finns en avancerad version av detta tillägg? Redaktör Allmänt Allmänna inställningar Dölj Ordning uppdaterad Miniminivå för att använda detta tillägg Nsp-Code AV PÅ Sortera kategorier och alla anpassade taxonomitermer (hierarkiska) och underordnade termer genom att använda en dra-och-släpp-funktionalitet med Javascript. Läs mer Spara inställningar Inställningarna har sparats Visa Prenumerant Taxonomier Sortering av taxonomier Taxonomy Terms Order Taxonomirubrik Detta tillägg fungerar inte utan JavaScript, eftersom det använder dra och släpp samt AJAX. Detta kommer att ändra ordningen på termer inom admin-gränssnittet Totalt antal inlägg Uppdatering global inställning http://www.nsp-code.com https://www.nsp-code.com tillägg som tillåter anpassad sortering av alla inlägg, sidor, anpassade inläggstyper tillägg som tillåter anpassad sortering av kategorier och anpassade taxonomitermer på inläggsbasis 