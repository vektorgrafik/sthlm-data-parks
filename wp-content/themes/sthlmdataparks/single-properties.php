<?php get_header(); ?>

	<div class="property-wrapper">
		<?php get_template_part( 'templates/property', 'single' ); ?>
	</div>

<?php get_footer(); ?>