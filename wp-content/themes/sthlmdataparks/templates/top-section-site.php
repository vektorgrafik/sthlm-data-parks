<?php 
	
	// Define default section class
	$section_class = '';

	if ( is_front_page() ) {
		// If front page
		$section_class = ' top-section--home';

	} else {

		// Get background image
		$queried_object = get_queried_object();
	 	$attachment_id = get_post_thumbnail_id( $queried_object->ID );
		$image = wp_get_attachment_image_src( $attachment_id, 'fullscreen');

		if ( $image ) {
			$section_class = ' top-section--image';
		} 

		// Get top section content
		$content_fields = get_field('page_content', get_queried_object()->ID );
		$content_empty = true;

		foreach ( $content_fields as $field ) {

			if ( ! empty( $field ) ) {
				$content_empty = false;
			}
		}

	} 

	if ( $background = get_field('bg_color') ) {

		$section_class .= ' bg-' . $background;

	}

?>

<div class="top-section <?=$section_class;?>">

	<?php if ( $image ) : ?>
		<div class="top-section__bg" style="background-image: url(<?php echo $image[0]; ?> );"></div>
	<?php endif; ?>



	<?php
	
	//	GET PROPERTY SITES
	// Get the property terms
	$sites = get_terms( array(

		'taxonomy' 	=> 'sites',
		'orderby' 	=> 'name',
		'order' 	=> 'ASC',
		'hide_empty' => false,

	));
	
	if ( $sites ) : ?>
			
		<span class="scroll-menu__anchor">
			<div class="scroll-menu property-list-wrap">
				<div class="row collapse">
					<div class="column small-12">

						<ul class="scroll-menu__inner property-list list--clean">

							<?php foreach ( $sites as $site ) : ?>


								<?php 
									//SITE 

									$site_id = $site->term_id;
									$site_title = $site->name;
									$site_slug = $site->slug;

								?>

								<li class="property-list__item">
									<a href="<?php echo get_term_link( $site->term_id, 'sites' );?>" class="property-list__link"><?= $site_title ;?></a>
								</li>

							<?php endforeach; ?>

						</ul> <!-- end .property-list -->
			
					</div> <!-- end .column -->
				</div> <!-- end .row -->

				<button class="scroll-menu__nav scroll-menu__nav--left"></button>
				<button class="scroll-menu__nav scroll-menu__nav--right"></button>

			</div> <!-- end .property-list-wrap -->
		</span>

	<?php endif; ?>


		<?php if ( $content_empty == false ) : 

			$content_layout = get_field( 'top_section_layout' );

			if ( $content_layout == 'one' ) {
				$section_class = 'top-section__inner--one-col';
				$row_class = 'space space--large';
				$column_class = 'small-12 medium-10 large-8 medium-offset-1 large-offset-2 text-center';
			} else {
				// assume two is selected

				$section_class = 'top-section__inner--two-col';
				$row_class = 'space large-unstack';
				$column_class = '';
			}


			$content_title = $content_fields['content_title'] ?? null;
			$content_intro = $content_fields['content_intro'] ?? null;
			$content_text = $content_fields['content_text'] ?? null;

			//dd($content_layout);


		?>
			<div class="top-section__inner <?=$section_class;?>">	
				<div class="row <?=$row_class;?>">

					<?php if ( $content_title ) : ?>

						<div class="column <?=$column_class;?>">
							<h1 class="top-section__title h1"><?php echo $content_title; ?></h1>		
						</div> <!-- end .column -->

					<?php endif; ?>

					<?php if ( $content_intro || $content_text ) : ?>

						<div class="column <?=$column_class;?>">

							<?php if ( $content_intro ) : 
								$content_intro = str_replace('<p>', '<p class="intro">', $content_intro); 
							?>

								<div class="top-section__intro color-primary">
									<?php echo $content_intro; ?>
								</div> <!-- end .top-section__intro -->

							<?php endif; ?>

							<?php if ( $content_text ) : ?>

								<div class="top-section__text">
									<?php echo $content_text; ?>
								</div> <!-- end .top-section__text -->

							<?php endif; ?>

						</div> <!-- end .column -->

					<?php endif; ?>
						
				</div> <!-- end .row -->
			</div> <!-- end .top-section__inner -->
		<?php endif; ?>
	

</div> <!-- end .top-section -->