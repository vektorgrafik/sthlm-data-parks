
<?php

if( have_rows('flexible_content') ):

  // loop through the rows of data
  while ( have_rows('flexible_content') ) : the_row();

    get_template_part('templates/flexible-layouts/' . get_row_layout() );

  endwhile;

else :

    // no layouts found

endif;