<?php
		
		//	GET PROPERTY SITES
		// Get the property terms
		$sites = get_terms( array(

			'taxonomy' 	=> 'sites',
			'orderby' 	=> 'name',
			'order' 	=> 'ASC',
			'hide_empty' => true,

		));

?>

<?php if ( $sites ) : ?>
		
	<span class="scroll-menu__anchor">
		<div class="scroll-menu property-list-wrap">
			<div class="row collapse">
				<div class="column small-12">

					<ul class="scroll-menu__inner property-list list--clean">

						<?php foreach ( $sites as $site ) : ?>


							<?php 
								//SITE 

								$site_id = $site->term_id;
								$site_title = $site->name;
								$site_slug = $site->slug;

							?>

							<li class="property-list__item">
								<a href="<?php echo get_term_link( $site->term_id, 'sites' );?>" class="property-list__link"><?= $site_title ;?></a>
							</li>

						<?php endforeach; ?>

					</ul> <!-- end .property-list -->
		
				</div> <!-- end .column -->
			</div> <!-- end .row -->

			<button class="scroll-menu__nav scroll-menu__nav--left"></button>
			<button class="scroll-menu__nav scroll-menu__nav--right"></button>

		</div> <!-- end .property-list-wrap -->
	</span>

	<section class="sites-section">

		<?php foreach ( $sites as $site ) : ?>

			<article class="site-item">

				<div class="row small-up-1 medium-up-2 space">	
					
						<div class="column">

							<h1 class="site-item__title">
								<a href="<?php echo get_term_link( $site->term_id, 'sites' );?>" class="color-dark">
									<?php echo $site->name; ?>
								</a>
							</h1>
				
						</div> <!-- end .column -->

						<div class="column">

							<div class="site-item__desc color-primary">

								<?php if ( $desc = $site->description ) : ?>
									
									<p class="strong"><?php echo $desc; ?></p>
								
								<?php endif; ?>

								<a href="<?php echo get_term_link( $site->term_id, 'sites' );?>" class="color-dark"><p><?php echo __('Go to Site', 'vektor'); ?></p></a>							

							</div> <!-- end .site-item__desc -->
					
						</div> <!-- end .column -->

				</div> <!-- end .row -->

			</article> <!-- end .site-item -->

		<?php endforeach; ?>

	</section>

<?php endif; ?>