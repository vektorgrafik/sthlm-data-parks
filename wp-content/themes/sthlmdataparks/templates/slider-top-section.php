<?php 
	
	$slider = get_field( 'top_section_slider' );
	$sliderCount = count($slider);

	$slider_class = ( $sliderCount > 1 ) ? 'top-section__slider owl-carousel' : '';	

?>

<?php
if ( $slider ) : ?>

	<div class="slider <?=$slider_class ;?>">
		
		<?php $x=0; foreach ( $slider as $slide ) :

				$image = $slide['top_section_slider_image'];
				$slider_pagelink = $slide['slider_pagelink'] ?? null;
				$slider_anchor = $slide['slider_anchor_link'] ?? null;

				if ( $image ) {
					//$slide_bg = 'style="background-image:url(' . $image['sizes']['fullscreen'] . ');"';
					$slide_bg = '';
				} else {
				 	$slide_bg = '';
				}

		?>

			<div class="slider__item" <?=$slide_bg;?> >
				
				<?php if ( !$slider_pagelink && !$slider_anchor ) { ?> <a style="cursor:default;"> <?php } else { ?>
				
				<a href="<?php if ( $slider_pagelink ) { echo $slider_pagelink; } if ( $slider_anchor ){ echo '#' . $slider_anchor; } ?>">
				
				<?php
				} ?>
				
					<?php if ( $image ) {

						if( $x == 0 ) {

							echo '<div class="slider__content">';
								echo '<div class="slider__content-wrap">';
								echo '<h1 class="slider__header">Stockholm <span>Data Parks</span></h1>';
								echo '<h2 class="slider__subheader">Green Computing Redefined</h2>';
								echo '</div>';
							echo '</div>';

						}

						echo '<img src="' . $image['sizes']['fullscreen'] . '"/>';

					} ?>

				</a>

			</div>

		<?php $x++; endforeach; ?>

	</div> <!-- end .slider -->

<?php
endif;

$partners_args = array(

		'post_type' 		=> 'partners',
		'posts_per_page' 	=> -1,
		'orderby'			=> 'menu_order',
		'order'				=> 'ASC',
		'meta_key'			=> 'partner_show_home',
		'meta_value'		=> true

	);

$partners_query = new WP_Query( $partners_args );

if ( $partners_query->have_posts() ) : ?>

	<div class="top-section__logos partner-logos partner-logos--top-section space--small">
		
			<div class="relative">
				<div class="partner-logos__list">
				
					<?php while ( $partners_query->have_posts() ) : $partners_query->the_post(); ?>

						<?php 

							$partner_fields = get_field_objects($post->post_ID);
							$logo_white = $partner_fields['partner_logo_white']['value'] ?? null;

						?>

						<div class="partner-logos__image-wrap">
							<img src="<?= $logo_white['sizes']['medium'] ;?>" alt="<?= $logo_white['alt'] ;?>" class="partner-logos__image">
						</div>

					<?php endwhile; wp_reset_postdata(); ?>	

				</div> <!-- end .partner-logos__list -->

			</div>

	</div> <!-- end .partner-logos -->

<?php endif; ?>