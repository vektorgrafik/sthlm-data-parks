<?php 
	
	$external_cat = get_field('blog_external_category', 'option');
	$cats = get_the_category();
	$is_external = false;
	$attachment_image = wp_get_attachment_image( get_post_thumbnail_id() , 'large');

	if ( $attachment_image ) {
		$image = $attachment_image;
	} else {
		$image = '<div class="news-article__image-fallback">';
		$image .= '<h2 class="news-article__image-fallback-title color-white">';
		$image .= $cats[0]->name;
		$image .= '</h2>';
		$image .= '</div>';
	}

	foreach ( $cats as $cat ) {

		// Check if any one of categories matches external category id. 
		if ( $cat->term_id == $external_cat ) {

			// Even if only one of categories is external cat this takes priority and we set is_external variable to true
			$is_external = true;
		}

	}

?>


<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article news-article" class="space" itemscope itemtype="http://schema.org/BlogPosting">
						
	<header class="article-header">	
		<?php get_template_part( 'templates/part', 'byline' ); ?>
		<?php //get_template_part( 'templates/part', 'share' ); ?>
		<h1 class="entry-title single-title" itemprop="headline"><?php the_title(); ?></h1>
    </header> <!-- end article header -->
					
    <section class="entry-content" itemprop="articleBody">
		<?php the_post_thumbnail('full'); ?>

		<?php if ( $is_external == false ) : ?>
			<?php the_content(); ?>
	    <?php endif; ?>
	
		<?php if ( !$is_external == false ) : ?>
			<footer class="news-article__footer">
	    		<p><a href="<?php echo get_field('external_article_link'); ?>" class="link-external" target="_blank">Read article</a></p>
	    	</footer>
	    <?php endif; ?>

	</section> <!-- end article section -->
	
</article> <!-- end article -->	
	


<!-- more-news containers -->
	
<!--
<section class="more-news">
	<div class="row">
		<div class="more-news-section_column columns small-12 large-6 clickable">
		<div class="icon-arrow_forward"></div>
			<?php	
			//if( ($title_box_1 = get_field('title_box_1'))  && ($url_box_1 = get_field('url_box_1')) ) { ?>
				<a class="clickable_link_source" href="<?php //echo $url_box_1?>"> <?php //echo $title_box_1?> </a>	
			<?php
			//} else { ?>
			<a class="clickable_link_source" href="<?php //echo get_permalink(1395);?>"> Partner </br> Insights </a>	
			<?php	
			//} ?>	
		</div>
			
		<div class= "more-news-section_column columns small-12 large-6 clickable">	
		<div class="icon-arrow_forward"></div>			
			<?php	
			//if( ($title_box_2 = get_field('title_box_2'))  && ($url_box_2 = get_field('url_box_2')) ) { ?>
			<a class="clickable_link_source" href="<?php //echo $url_box_2?>"> <?php //echo $title_box_2?> </a>		
			<?php
			//} else { ?>
			<a class="clickable_link_source" href="<?php //echo get_permalink(13);?>"> More </br> News </a>			
			<?php	
			//}	?>	
			
		</div>		
</section>
-->
	
	
	
																

