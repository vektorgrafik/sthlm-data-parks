<?php
	$subject = get_field('mail_subject', 'option') ?? "Tip from stockholmdataparks.com";
	$message = get_field('mail_body', 'option') ?? get_permalink();
	
	$message = str_replace('%title%', '"' . get_the_title() . '"', $message);
	$message = str_replace('%link%', get_permalink( get_the_id() ), $message);
	
	$subject = strip_tags( $subject );
	$message = strip_tags( $message );
	
	$message = nl2br( $message );
	$message = str_replace( '<br />', '%0D%0A', $message);			
?>


<div class="social-share">
	<a href="#" class="social-link fb" data-type="facebook" data-href="<?php the_permalink(); ?>"><i class="icon icon-facebook"></i></a>
	<a href="#" class="social-link twitter" data-type="twitter" data-href="<?php the_permalink(); ?>"><i class="icon icon-twitter"></i></a>
	<a href="#" class="social-link linkedin" data-type="linkedin" data-href="<?php the_permalink(); ?>"><i class="icon icon-linkedin"></i></a>
	<a href="mailto:?subject=<?=$subject?>&body=<?=htmlentities($message);?>" class="social-link mail"><i class="icon icon-mail"></i></a>
</div>