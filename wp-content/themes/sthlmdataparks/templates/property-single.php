<?php 

	if ( is_singular() ) {

		$property_ID = get_queried_object_id();

	} else {

		$property_ID = ( defined( 'DOING_AJAX' )) ? $property_id : get_queried_object_id();

	}

	// Defining and fetching taxonomy and this specific propertys terms
	$taxonomy = 'sites';
	$terms =  wp_get_object_terms( $property_ID, $taxonomy, array('fields'=>'ids') );
	$term_list = implode(", ", $terms);


	// Get property fields (ACF)
	$property_fields = get_field_objects( $property_ID );

	$property_title 	= $property_fields['content_title']['value'] ?? null;
	$property_intro 	= $property_fields['content_intro']['value'] ?? null;
	$property_content 	= $property_fields['content_text']['value'] ?? null;

	$property_details_repeater 	= $property_fields['property_details_repeater']['value'] ?? null;
	$property_details_count = count($property_details_repeater);

	$property_resource 			= $property_fields['property_resource'] ?? null;
	$property_resource_desc 	= $property_fields['property_resource_desc'] ?? null;
	$property_owner 			= $property_fields['property_owner'] ?? null;
	$property_contact_object 	= $property_fields['property_contact_object'] ?? null;

	// Get background image
 	$attachment_id = get_post_thumbnail_id( $property_ID );
	$image = wp_get_attachment_image_src( $attachment_id, 'x-large');

?>

<?php if ( $image ) : ?>

	<div class="top-section top-section--image">
		<div class="top-section__bg" style="background-image: url(<?php echo $image[0]; ?> );"></div>
	</div>

<?php endif;

$properties_args = array(

		'post_type' 		=> 'properties',
		'posts_per_page' 	=> -1,
		'orderby'			=> 'menu_order',
		'order'				=> 'ASC',
		'tax_query' => array(
				array(
					'taxonomy' => $taxonomy,
					'field'    => 'term_id',
					'terms'    => array( $term_list ),
				),
			),

	);

$properties_query = new WP_Query( $properties_args );
if ( $properties_query->have_posts() ) : ?>

	<span class="scroll-menu__anchor">
		<div class="scroll-menu property-list-wrap">
			<div class="row collapse">
				<div class="column small-12">

					<ul class="scroll-menu__inner property-list list--clean">

						<?php while ( $properties_query->have_posts() ) : $properties_query->the_post(); ?>

							<?php 
								//PROPERTY 

								$property_list_id = $post->ID;
								$property_list_title = $post->post_title;
								$property_list_slug = $post->post_name;

								if ( $property_ID == $property_list_id ) { 
									$link_class = 'is-active';
								} else {
									$link_class = '';
								}
							?>

							<li class="property-list__item">
								<a href="<?php echo get_permalink( $property_list_id ); ?>" data-property-id="<?=$property_list_id?>" class="property-list__link js-ajax <?= $link_class ;?>"><?= $property_list_title ;?></a>
							</li>

						<?php endwhile; ?>

					</ul>
		
				</div>
			</div>

			<button class="scroll-menu__nav scroll-menu__nav--left"></button>
			<button class="scroll-menu__nav scroll-menu__nav--right"></button>

		</div>
	</span>

<?php endif; wp_reset_postdata();?>

<?php if ( $property_landing_page = get_field('page_properties', 'options') ) : ?>

	<section class="breadcrumb">
		<div class="row small-up-1">
			<div class="column"><a href="<?= $property_landing_page ;?>" class="breadcrumb__link"><p><span class="breadcrumb__arrow icon-angle-left"></span><?php echo __('Back to Parks', 'vektor'); ?><p></a></div>
		</div>
	</section>

<?php endif; ?>

<section class="property-item space space--large">
	<div class="row small-up-1 large-up-2">
		
		<div class="column">

			<?php if ( $property_title ) : ?>
				<h1 class="property-item__title"><?= $property_title; ?></h1>
			<?php endif; ?>

			<?php if ( $property_intro ) : ?>
				<?php $new_property_intro = str_replace('<p>', '<p class="strong color-primary">', $property_intro); ?>
				<div class="property-item__intro"><?= $new_property_intro; ?></div>
			<?php endif; ?>
			
			<?php if ( $property_content ) : ?>
				<div class="property-item__content"><?= $property_content; ?></div>
			<?php endif; ?>

		</div>

		<?php if ( $property_details_count > 0 ) : ?>

			<?php

				// Main counter
				$i = 1;

				// Counter for column 1
				$col_1_i = 1;

				// Counter for column 2
				$col_2_i = 2;

				$column_count = 2;
				$column_items_count = ceil($property_details_count / $column_count);

			?>

			<div class="column">
				
				<div class="property-details">
					<div class="row small-up-1 medium-up-<?= $column_count ;?> space">

						<?php if( is_array( $property_details_repeater )) foreach ( $property_details_repeater as $detail_item ) : ?>

							<?php 
								if ( $i <= $column_items_count ) {
									// If first column this item order is the following
									$detail_item_order = 'order-' . $col_1_i;

									// Add iteration for following item in same column
									$col_1_i = $col_1_i + 2;
									
								} else {
									// If second column this item order is the following
									$detail_item_order = 'order-' . $col_2_i;

									// Add iteration for following item in same column
									$col_2_i = $col_2_i + 2;

								}

							?>

							<div class="column <?php echo $detail_item_order; ?>">
							
								<div class="property-detail-item">

									<h4 class="property-detail-item__heading h5 color-primary"><?php echo $detail_item['property_detail_title']; ?></h4>
									<p class="property-detail-item__value intro"><?php echo $detail_item['property_detail_value']; ?></p>

								</div> <!-- end .property-detail-item -->
							
							</div> <!-- end .column -->
							
						<?php $i++; endforeach; ?>
						
					</div>
				</div> <!-- end .property-details -->

			</div> <!-- end .column -->

		<?php endif; ?>
	
	</div> <!-- end .row -->

</section>

<section class="property-connections space">
	
	<div class="row small-up-1 medium-up-2">

		<?php if ( $property_resource['value'] ) : ?>

			<div class="column">
				<div class="property-connections__item">
					<h2 class="h3"><?php echo $property_resource['label']; ?></h2>
				</div>
			</div>

			<div class="column">
				<div class="property-connections__item">
					<?php 				

						$resource_id = $property_resource['value'][0]->ID ?? null;
						$resource_name = $property_resource['value'][0]->post_title ?? null;

						$resource_pdf = get_field('resource_pdf', $resource_id );
						if ( $resource_pdf ) {
							echo '<p class="clear-margin-b"><a href="' . $resource_pdf['url'] . '" class="property__resource link-download" target="_blank">' . $resource_name .' (PDF)</a></p>';
						}
					?>

					<?php if ( $property_resource_desc ) : ?>
						<p class="text-small color-secondary"><?php echo $property_resource_desc['value']; ?></p>
					<?php endif; ?>

				</div>
			</div>

		<?php endif; ?>

		<?php if ( $property_owner['value'] ) : ?>

			<div class="column">
				<div class="property-connections__item">
					<h2 class="h3"><?php echo $property_owner['label']; ?></h2>
				</div> <!-- end .property-connections__item -->
			</div> <!-- end .column -->

			<div class="column">
				<div class="property-connections__item">
					<p><?php echo $property_owner['value']; ?></p>
				</div> <!-- end .property-connections__item -->
			</div> <!-- end .column -->

		<?php endif; ?>

		<?php if ( $property_contact_object['value'] ) : ?>

			<div class="column">
				<div class="property-connections__item">
					<h2 class="h3"><?php echo $property_contact_object['label']; ?></h2>
				</div>
			</div>

			<div class="column">
				<div class="property-connections__item">

					<?php

						$contact_id	= $property_contact_object['value']->ID ?? null;
						$post_title = $property_contact_object['value']->post_title ?? null;

						$first_name = substr( $post_title, 0, strrpos($post_title, ' ') );
					
						$employee_object = get_field_objects($contact_id);

						$employee_title = $employee_object['employee_title']['value'] ?? null;
						$employee_company = $employee_object['employee_company']['value'] ?? null;
						$employee_phone = $employee_object['employee_phone']['value'] ?? null;
						$employee_mail = $employee_object['employee_mail']['value'] ?? null;

					?>

					<ul class="list list--clean">

						<?php if ( $post_title ) : ?>
							<li class="list__item list__item--highlighted"><?=$post_title;?></li>
						<?php endif; ?>

						<?php if ( $employee_title ) : ?>
							<li class="list__item"><?=$employee_title;?></li>
						<?php endif; ?>

						<?php if ( $employee_company ) : ?>
							<li class="list__item"><?=$employee_company;?></li>
						<?php endif; ?>
					
					</ul>

					<ul class="list list--clean">
									
						<?php if ( $employee_phone ) : ?>
							<li class="list__item list__item--highlighted"><?=$employee_phone;?></li>
						<?php endif; ?>

						<?php if ( $employee_mail ) : ?>
							<li class="list__item">
								<a href="mailto:<?= $employee_mail ;?>" class="list__link property-connections__mail-link"><?php echo _e('Email ' ,'vektor') . $first_name; ?></a>
							</li>
						<?php endif; ?>

					</ul>

			 	</div>

		 	</div>

		<?php endif; ?>

	</div>

</section>