<?php 
	
	// Get id for term selected as external category in Other Options
	$external_cat = get_field('blog_external_category', 'option');

	// Define is external value (is_external) and catogory output (article_category) for later 
	$is_external = false;
	$article_category = '';

	// Get the post categories and define array for later
	$categories = get_the_category();
	$cat_array = array();

	// Loop to categories
	foreach ( $categories as $cat ) {

		// Check if any one of categories matches external category id. 
		if ( $cat->term_id == $external_cat ) {

			// Even if only one of categories is external cat this takes priority and we set is_external variable to true
			$is_external = true;
		}

		// To let us loop out categories ( will only be user if external cat is not set ) we add each looped cat to array
		array_push($cat_array, '<a href="' . get_term_link($cat->term_id) . '">' . $cat->name . '</a>');

	}

	if ( $is_external == true ) {

		// Use the text field from post instead of a cat, it will be echoed out
		$article_category = get_field('article_source', $post->ID);

	} else {

		// The array we filled in previous foreach loop is now made into a comma separeted string so we can echo it out
		$article_category = implode(", ", $cat_array);
	}

?>

<p class="news-article__meta text-small <?php echo (is_blog()) ? 'inline-display-large-up' : ''; ?>">
	By <span class="entry-author"><?php the_author_posts_link(); ?></span> 
	- 
	<time class="entry-date"><?php the_time('M j, Y') ?></time> 
	| 
	<span class="entry-cat"><?php echo $article_category; ?></span>
</p>	
