<?php 
	
	$external_cat = get_field('blog_external_category', 'option');
	$cats = get_the_category();
	$is_external = false;
	$attachment_image = wp_get_attachment_image( get_post_thumbnail_id() , 'large');

	if ( $attachment_image ) {
		$image = $attachment_image;
	} else {
		$image = '<div class="news-article__image-fallback">';
		$image .= '<h2 class="news-article__image-fallback-title color-white">';
		$image .= $cats[0]->name;
		$image .= '</h2>';
		$image .= '</div>';
	}

	foreach ( $cats as $cat ) {

		// Check if any one of categories matches external category id. 
		if ( $cat->term_id == $external_cat ) {

			// Even if only one of categories is external cat this takes priority and we set is_external variable to true
			$is_external = true;
		}

	}

?>


<article id="post-<?php the_ID(); ?>" class="news-article" role="article">
	

	<figure class="news-article__image">
		<?php echo $image;?>
	</figure>

	<header class="news-article__header">
		
		<?php get_template_part( 'templates/part', 'byline' ); ?>
		
		<?php get_template_part( 'templates/part', 'share' ); ?>
		
		<h2 class="news-article__heading h5"><?php the_title(); ?></h2>
		
	</header>

	<?php if ( $is_external == false ) : ?>
		<section class="news-article__content entry-content" itemprop="articleBody">
			<?php //the_content(); ?>	
		</section>
	<?php endif; ?>
	
	
	<footer class="news-article__footer">
		


		<?php if ( $is_external == false ) : ?>
	 
	    			<a href="<?php echo get_permalink( get_the_ID() ); ?>" data-post-id="post-<?php the_ID(); ?>">Read more</a>
	 
	    <?php else : ?>
	    
	    	<p><a href="<?php echo get_field('external_article_link'); ?>" class="link-external" rel="nofollow" target="_blank">Read article</a></p>

	    <?php endif; ?>
	   
	    

	</footer>
</article>
