<?php

	$bg_color 			= get_sub_field('bg_color') ?? null;
	$video_placement	= get_sub_field('video_align') ?? null;
	$video_type			= get_sub_field('video_type') ?? null;
	$video 				= get_sub_field('video') ?? null;
	$text 				= get_sub_field('text') ?? null;
	
if($text || $video) { ?>

	<section class="content-section video-text-section space space--large bg-<?=$bg_color?>">
		
		<div class="row">
	
			<?php 
			if( $video_placement == 'left' ) { ?>
				
				<div class="column small-12 medium-6">
					
					<div style="position: relative; padding-bottom: 56.25%; padding-top: 30px; height: 0; overflow: hidden;">
					
						<?php 
						if( $video_type == 'Youtube' ) { ?>
									
							<iframe style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;" class="video-container__video" width="500" height="300" src="https://www.youtube.com/embed/<?php echo $video; ?>?&autoplay=0&showinfo=0&vq=highres&rel=0&loop=0&playlist=<?php echo $video; ?>" allowfullscreen frameborder=0></iframe>
					
						<?php 
						} 
						
						if( $video_type == 'Vimeo' ) { ?>
							
							<iframe style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;" src="https://player.vimeo.com/video/<?php echo $video; ?>?autoplay=0&loop=0&title=0&byline=0&portrait=0" width="500" height="300" frameborder="0" allow="autoplay" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
				
						<?php 
						} ?>
				
					</div>
				
				</div>
				
			<?php 
			} ?>
	
			<div class="column small-12 medium-6">
				
				<p><?= $text; ?></p>			
			
			</div> 
	
			<?php 
			if($video_placement == 'right') { ?>
				
				<div class="column small-12 medium-6">
					
					<div style="position: relative; padding-bottom: 56.25%; padding-top: 30px; height: 0; overflow: hidden;">
					
						<?php 
						if( $video_type == 'Youtube' ) { ?>
				
							<iframe style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;" class="video-container__video" width="500" height="300" src="https://www.youtube.com/embed/<?php echo $video; ?>?&autoplay=0&showinfo=0&vq=highres&rel=0&loop=0&playlist=<?php echo $video; ?>" allowfullscreen frameborder=0></iframe>
						
						<?php 
						}

						if( $video_type == 'Vimeo' ) { ?>
							
							<iframe style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;" src="https://player.vimeo.com/video/<?php echo $video; ?>?autoplay=0&loop=0&title=0&byline=0&portrait=0" width="500" height="300" frameborder="0" allow="autoplay" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
									
						<?php 
						} ?>
				
					</div>
				
				</div>
			
			<?php 
			} ?>
	
		</div> <!-- end .row -->
		
	</section>

<?php
} ?>
