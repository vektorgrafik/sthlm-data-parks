<?php
	
	$text 				= get_sub_field('text') ?? null;
	$image 				= get_sub_field('image') ?? null;
	$image_placement	= get_sub_field('image_align') ?? null;
	$bg_color 			= get_sub_field('bg_color') ?? null;

?>

<?php if($text || $image) : ?>
<section class="content-section image-text-section space space--large bg-<?=$bg_color?>">
	<div class="row">

		<?php if($image_placement == 'left') : ?>
			<div class="column small-12 medium-6">
				<img src="<?= $image['sizes']['large']; ?>" />
			</div>
		<?php endif; ?>

		<div class="column small-12 medium-6">
			<p><?= $text; ?></p>			
		</div> 

		<?php if($image_placement == 'right') : ?>
			<div class="column small-12 medium-6">
				<img src="<?= $image['sizes']['large']; ?>" />
			</div>
		<?php endif; ?>

	</div> <!-- end .row -->
</section>
<?php endif; ?>