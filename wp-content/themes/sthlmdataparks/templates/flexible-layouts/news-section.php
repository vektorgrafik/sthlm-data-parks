<?php

	$fields = get_row('news-section');

	$news_objects 			= $fields['news_objects'] ?? null;
	$section_title 			= $fields['section_title'] ?? null;
	$news_read_more 		= $fields['news_read_more_text'] ?? null;

	$blog_page_id			= get_option( 'page_for_posts' );

?>

<?php if ( $news_objects ) : ?>

	<section class="news-section space space--large">
		
		<div class="row medium-unstack">

			<?php if ( $section_title ) : ?>

				<div class="column">
					<h2 class="news-section__title h2"><?=$section_title;?></h2>
				</div> <!-- end .column -->

			<?php endif; ?>

			<div class="column">
				
				<?php if ( $blog_page_id && $news_read_more ) : ?>
					<a href="<?php echo get_permalink( $blog_page_id ); ?>" class="news__page-link"><p><?= $news_read_more ;?></p></a>
				<?php endif; ?>

			</div> <!-- end .row -->

		</div> <!-- end .row -->

		<div class="news row medium-unstack">

			<?php foreach ( $news_objects as $news_object ) : 

				$post_ID 	= $news_object->ID ?? null;
				$post_title = $news_object->post_title ?? null;
				$post_date 	= $news_object->post_date ?? null;

				$post_type 	= $news_object->post_type ?? null;

				//Reformatting Post Date
				$date 		= date_create($post_date);
				$new_date 	= date_format($date, "M, Y");

			?>

				<div class="column">
					
					<div class="news__item <?= 'news__item--' . $post_type ;?>">

						<?php
							$image_id = get_post_thumbnail_id( $post_ID );
							$image = wp_get_attachment_image_src( $image_id, "medium_large" );

							if ( $image ) {
								$bg = 'style=" background-image: url(' . $image[0] . ');"';
							} else {
								$bg = '';
							}
						?>
						
						
						<?php if ( $post_type != 'resources' ) { ?>
						<div class="clickable news__item-image relative" <?=$bg;?> >
							
							<a href="<?php echo get_permalink( $post_ID ); ?>" class="clickable_link_source" style="display:none;"> </a>
						<?php
						}
							
						if ( $post_type == 'resources' ) : ?>
						<div class="clickable resource-item--whitepapers resource-item--presentations news__item-image relative" <?=$bg;?> >
							<?php	
							$term_array = get_the_terms( $post_ID, 'resources_tax');
							$term_list = array();

							if ( $term_array ) : foreach ( $term_array as $term ) {
								array_push($term_list, $term->name);

								echo '<span class="news__item-image-text strong link-download">' . implode(", ", $term_list) . '</span>';

							} endif;

							$post_format = get_post_format( $post_ID ) ? false : 'standard';

							$name = "";
							if ( $post_format == 'standard') {

								$pdf = get_field( 'resource_pdf', $post_ID );
								$link = $pdf['url'];
								$name = get_the_title( $post_ID );

							} else {

								$link = get_field( 'resource_url', $post_ID );

						 	} ?>

							<a href="<?php echo $link; ?>" class="news__item-permalink resource-item__pdf resource-item--<?=$term_array[0]->slug?>" data-type="<?=$term_array[0]->name?>" data-name="<?=$name?>" style="display:none;"> </a>


						<?php endif; ?>

						</div>

						<div class="news__item-content">

							<div class="news__item-meta">

								<?php if ( $post_type == 'post' ) : 

									echo '<time class="news__date">' . $new_date . '&nbsp;</time>';

									$cat_array = get_the_category( $post_ID );
									$cat_list = array();

									if ( $cat_array ) : foreach ( $cat_array as $cat ) {
										array_push($cat_list, '<a class="cat_ref" href="'  . get_category_link( $cat->cat_ID ) .  '">' . $cat->cat_name . ' </a>');
									} endif;
									
									?>
									
									<span class="news__cat"> <?php echo ' | ' . implode(", ", $cat_list); ?></span>

								<?php else : //Assume it's a resource

									//dump($term_array);

									if ( $term_array ) {

										echo '<span class="news__date">' . implode(", ", $term_list) . ' – ' . $new_date .'&nbsp;|&nbsp;</span>';

									}

									if ( $resources_page = get_field('page_resources', 'options') ) {
										echo '<a class="cat_ref" href="' . $resources_page . '">' . __('Resources', 'vektor') . '</a>';
									}

 								?>

								<?php endif; ?>

							</div>

							<?php if ( $post_type != 'resources' ) { ?>
							<div class="clickable">
							<?php
							} else {?>
							<div class="clickable resource-item--whitepapers resource-item--presentations">
							<?php } ?>
							
								<h3 class="news__item-title"><?php echo $post_title; ?></h3>
	
								<?php if ( $post_type == 'post' ) : ?>
									<a href="<?php echo get_permalink( $post_ID ); ?>" class="clickable_link_source news__item-permalink"><?php echo _e( 'Read more ', 'vektor' ); ?></a>
	
								<?php else : //Assume it's a resource ?>
	
									<?php 
	
										$post_format = get_post_format( $post_ID ) ? false : 'standard';
	
										$name = "";
										if ( $post_format == 'standard') {
	
											$pdf = get_field( 'resource_pdf', $post_ID );
											$link = $pdf['url'];
											$name = get_the_title( $post_ID );
	
										} else {
	
											$link = get_field( 'resource_url', $post_ID );
	
									 	}
	
									?>
	
									<a href="<?php echo $link; ?>" class="news__item-permalink resource-item__pdf resource-item--<?=$term_array[0]->slug?>" data-type="<?=$term_array[0]->name?>" data-name="<?=$name?>" target="_blank"><?php echo _e( 'Download', 'vektor' ); ?></a>
	
								<?php endif; ?>
								
							</div><!-- end . -->

						</div> <!-- end .news__item-content -->

					</div> <!-- end .news__item -->


				</div>

			<?php endforeach; ?>

		</div> <!-- end .row -->

	</section>

<?php endif; ?>