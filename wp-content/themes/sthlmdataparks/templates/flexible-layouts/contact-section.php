<?php

	$fields = get_row('news-section');

	$section_title = $fields['section_title'] ?? null;
	$section_intro = $fields['section_intro'] ?? null;

	$contact_objects = $fields['contact_objects'] ?? null;

	//dd($fields);

?>

<?php if ( $contact_objects ) : ?>

	<section class="contact-section">

		<?php if ( $section_title || $section_intro ) : ?>
			<div class="row space space--large medium-unstack contact__heading-row">

				<?php if ( $section_title ) : ?>
					<div class="column">
						<h2 class="contact-section__title h2"><?=$section_title;?></h2>
					</div> <!-- end .column -->
				<?php endif; ?>
				
				<?php if ( $section_intro ) : ?>
					<div class="column">
						<div class="contact-section__intro">
						<?php echo $section_intro; ?>
						</div>
					</div>
				<?php endif; ?> 

			</div> <!-- end .row -->

		<?php endif; ?>

		<div class="contact-section__item-wrap row small-up-2 space space--large">

			<?php foreach ( $contact_objects as $contact_object ) : 

				$post_ID 	= $contact_object->ID ?? null;
				$post_title = $contact_object->post_title ?? null;

				$first_name = substr( $post_title, 0, strrpos($post_title, ' ') );
				
				$employee_object = get_field_objects($post_ID);

				//dd($employee_object);

				$employee_title = $employee_object['employee_title']['value'] ?? null;
				$employee_company = $employee_object['employee_company']['value'] ?? null;
				$employee_phone = $employee_object['employee_phone']['value'] ?? null;
				$employee_mail = $employee_object['employee_mail']['value'] ?? null;
				$employee_vcard = $employee_object['employee_vcard']['value'] ?? null;

				$employee_twitter = $employee_object['employee_twitter']['value'] ?? null;
				$employee_linkedin = $employee_object['employee_linkedin']['value'] ?? null;

				$image_id = get_post_thumbnail_id( $post_ID );
				$image = wp_get_attachment_image_src( $image_id, "large" )[0]; 

				//dd($image);

			?>

				<div class="column">

					<div class="contact__item">

						<?php
							if ( $image ) {
								$bg = 'style=" background-image: url(' . $image . ');"';
							} else {
								$bg = '';
							}
						?>

						<div class="contact__item-image" <?=$bg;?> ></div>

						<div class="contact__item-content">

							<ul class="list list--clean">
								
								<?php if ( $post_title ) : ?>
									<li class="list__item list__item--highlighted"><?=$post_title;?></li>
								<?php endif; ?>

								<?php if ( $employee_title ) : ?>
									<li class="list__item"><?=$employee_title;?></li>
								<?php endif; ?>

								<?php if ( $employee_company ) : ?>
									<li class="list__item"><?=$employee_company;?></li>
								<?php endif; ?>

							</ul>

							<ul class="list list--clean">
								
								<?php if ( $employee_phone ) : $stripped_phone = str_replace(' ', '', $employee_phone); ?>
									<li class="list__item list__item--highlighted"><a href="tel:<?=$stripped_phone;?>" class="list__link"><?=$employee_phone;?></a></li>
								<?php endif; ?>

								<?php if ( $employee_mail ) : ?>
									<li class="list__item">
										<a href="mailto:<?= $employee_mail ;?>" class="list__link list__link--highlighted"><?php echo _e('Email ' ,'vektor') . $first_name; ?></a>
									</li>
								<?php endif; ?>

							</ul>

							<ul class="contact__social-list list list--clean list--horizontal">
								
								<?php if ( $employee_twitter ) : ?>
									<li class="list__item"><a href="<?=$employee_twitter;?>" class="contact__social-list-icon"><span class="icon-twitter"></span></a></li>
								<?php endif; ?>

								<?php if ( $employee_linkedin ) : ?>
									<li class="list__item"><a href="<?=$employee_linkedin;?>" class="contact__social-list-icon"><span class="icon-linkedin"></span></a></li>
								<?php endif; ?>

							</ul>


						</div> <!-- end .contact__item-content -->

					</div> <!-- end .contact__item -->

				</div>

			<?php endforeach; ?>

		</div> <!-- end .row -->
	</section>

<?php endif; ?>