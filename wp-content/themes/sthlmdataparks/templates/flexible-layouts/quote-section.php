<?php 

	$fields = get_row('quote-section');

	$quote_author 			= $fields['quote_author'] ?? null;
	$quote_author_position 	= $fields['quote_author_position'] ?? null;
	$quote_text				= $fields['quote_text'] ?? null;

	//dd($quote_author);

?>


<section class="quote-section space">
	<div class="row small-up-1">

		<div class="column">
			
			<?php if ( $quote_author ) { 
				
				echo '<h4 class="quote__author">';
				echo $quote_author; 

				if ( $quote_author_position ) {

					echo ', <span class="quote__author-position">' . $quote_author_position . '</span>';
				}

				echo '</h4>';

			} ?>

		</div>
		
		<div class="column">

			<?php if ( $quote_text ) : ?>

				<p class="quote__text h2"><?=$quote_text;?></p>

			<?php endif; ?>
			
		</div>

	</div>
</section>