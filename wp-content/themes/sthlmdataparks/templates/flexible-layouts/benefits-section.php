<?php
	
	$fields = get_row('benefits-section');

	$section_content = $fields['section_content'] ?? null;

	//dd($fields);

?>

<section class="f-benefits-section space space--large">
	<div class="row large-unstack">

		<div class="column">
			
			<?php echo vektor_content_field($section_content, "benefits"); ?>

		</div> <!-- end .column -->

		<div class="column space"> 

			<?php $benefits_args = array(

					'post_type' 		=> 'benefits',
					'posts_per_page' 	=> -1,
					'orderby' 			=> 'menu_order',
					'order' 			=> 'ASC'

				);

			$benefits_query = new WP_Query( $benefits_args );

			if ( $benefits_query->have_posts() ) : ?>

				<div class="f-benefits row small-up-2 medium-up-4 large-up-4">

					<?php while ( $benefits_query->have_posts() ) : $benefits_query->the_post(); ?>
					
						<div class="column">
							
							<?php if ( $benefits_page = get_field('page_benefits', 'options') ) : ?>
								<a href="<?= $benefits_page . '#' .  $post->post_name; ?>" class="f-benefits__link">
							<?php endif; ?>

							<div class="f-benefits__item">
								
								<?php $icon = get_field('benefit_icon', $post->ID ); ?>
								<?php if ( $icon ) : ?>
									<div class="f-benefits__icon"><?php echo '<div class="f-benefits__icon-inner ' .  $icon . '"></div>'; ?></div>					
								<?php endif; ?>

								<h3 class="f-benefits__title text-small text-uppercase"><?php echo $post->post_title; ?></h3>

							</div> <!-- end .benefits__item -->

							<?php if ( $benefits_page ) : ?>
								</a> <!-- end .benefits__link -->
							<?php endif; ?>

						</div> <!-- end .column -->

					<?php endwhile; ?>

				</div> <!-- end .row -->

			<?php endif; wp_reset_postdata(); ?>

		</div> <!-- end .column -->
		
	</div> <!-- end .row -->
</section>