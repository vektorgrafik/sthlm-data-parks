<?php
	
	$fields = get_row('map-section');

	$section_title 		= $fields['section_title'] ?? null;
	$section_intro 		= $fields['section_text'] ?? null;
	$map_image 			= $fields['map_image'] ?? null;
	$map_image_small 	= $fields['map_image_small'] ?? null;

?>

<section class="map-section">

	<?php if ( $section_title || $section_intro ) : ?>
		<div class="row space--small large-unstack">

			<?php if ( $section_title ) : ?>
				<div class="column shrink">
					<h2 class="map-section__title h2"><?=$section_title;?></h2>
				</div> <!-- end .column -->
			<?php endif; ?>
			
			<?php if ( $section_intro ) : ?>
				<div class="column">
					<div class="map-section__intro">
						<?php echo $section_intro; ?>
					</div>
				</div>
			<?php endif; ?>

		</div> <!-- end .row -->

	<?php endif; ?>

	<?php if ( $map_image ) : ?>

		<?php if ( $property_landing_page = get_field('page_properties', 'options') ) : ?>
			<a href="<?= $property_landing_page ; ?>">
		<?php endif; ?>
			
			<img class="map-section__image map-section__image--large" src="<?php echo $map_image['url'] ; ?>" alt="<?php echo $map_image['alt'] ; ?>">

				<?php if ( $map_image_small ) : ?>
					<img class="map-section__image--small" src="<?php echo $map_image_small['url'] ; ?>" alt="<?php echo $map_image_small['alt'] ; ?>">
				<?php endif; ?>

		<?php if ( $property_landing_page = get_field('page_properties', 'options') ) : ?>
			</a>
		<?php endif; ?>

	<?php endif; ?>

<!-- 	<div id="main-map"
		data-lat="59.3262106"
		data-lng="17.9173349"
		data-pins='<?=get_site_locations([
			'asJson' => true,
			'forMap' => true
		]);?>'>
	</div>
 -->
</section>