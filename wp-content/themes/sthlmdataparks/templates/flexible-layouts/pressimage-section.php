<?php
	
	$fields 			= get_row('pressimage-section');

	$section_title = $fields['section_title'] ?? null;
	$section_intro = $fields['section_intro'] ?? null;

	$bg_color 			= $fields['bg_color'] ?? null;
	$image_repeater		= $fields['image_repeater'] ?? null;

?>

<?php if ( $image_repeater ) : ?>

	<section class="pressimage-section gallery space bg-<?=$bg_color?>">

		<?php if ( $section_title || $section_intro ) : ?>
			<div class="row space medium-unstack">

				<?php if ( $section_title ) : ?>
					<div class="column">
						<h2 class="pressimage-section__title h2"><?=$section_title;?></h2>
					</div> <!-- end .column -->
				<?php endif; ?>
				
				<?php if ( $section_intro ) : ?>
					<div class="column">
						<div class="pressimage-section__intro">
						<?php echo $section_intro; ?>
						</div>
					</div>
				<?php endif; ?> 

			</div> <!-- end .row -->

		<?php endif; ?>

		<div class="row small-up-2 medium-up-4 large-up-5">
			
			<?php $i = 0; foreach ( $image_repeater as $image_item ) : 

				$image = $image_item['press_image'];
				$caption = $image_item['press_caption'] ?? '';

			?>
				<div class="column">

					<figure class="gallery__image-wrap" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
						<a href="<?php echo $image['url']; ?>" style="background-image: url(<?php echo $image['sizes']['large']; ?>);" class="gallery__image bg--fill" data-index="<?php echo $i; ?>" data-size="<?php echo $image['width'] . 'x' . $image['height']; ?>" data-caption="<?= $caption ;?>"></a>

						<?php if ( $caption ) {

							echo '<figcaption itemprop="caption description"><p class="text-small">' . $caption . '</p></figcaption>';

						} ?>

					</figure>
								
				</div> <!-- end .column -->
			<?php $i++; endforeach; ?>

		</div> <!-- end .row -->
	</section>

	<?php get_template_part('templates/pswp'); ?>

<?php endif; ?>