<?php
	
	$fields 			= get_row('content-section');

	$content_align 		= $fields['content_align'] ?? null;
	$section_content 	= $fields['section_content'] ?? null;
	$bg_color 			= $fields['bg_color'] ?? null;

?>

<section class="content-section bg-<?=$bg_color?>">
	<div class="row space space--large">
		
		<div class="column small-12 medium-10 large-10 medium-offset-1 large-offset-1 <?= 'text-' . $content_align ;?> no-overflow">

			<?php echo vektor_content_field($section_content, "content"); ?>
						
		</div> <!-- end .column -->

	</div> <!-- end .row -->
</section>