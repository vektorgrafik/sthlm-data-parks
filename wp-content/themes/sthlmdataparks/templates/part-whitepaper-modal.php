<?php 

?>

<div class="whitepaper-modal__inner">

	<button class="close-modal">Close</button>

	<div class="row">
		<div class="small-12 medium-6 columns">
			<?php #if ( $modal_title = get_field('whitepaper_modal_title', 'options') ) : ?>
				<h3>Download <?=$whitepaper['type']?></h3>
			<?php #endif; ?>

			<?php #if ( $modal_text = get_field('whitepaper_modal_text', 'options') ) : ?>
				Fill out the form to download the <?=$whitepaper['type']; ?>
			<?php #endif; ?>
		</div>
		<div class="small-12 medium-6 columns">
			
			<div class="whitepaper-errors">
				<?php if ( $modal_error = get_field('whitepaper_modal_error_text', 'options') ) : ?>
					<?php echo $modal_error; ?>
				<?php endif; ?>
			</div>

			<form action="#" id="whitepaper-form" onsubmit="return false;">
				<input type="hidden" id="_nonce" name="_nonce" value="<?=wp_create_nonce( 'whitepaper-nonce' );?>">
				
				<label for="name">Name</label>
				<input type="text" name="name">
				
				<label for="name">Company</label>
				<input type="text" name="company">

				<label for="name">Position</label>
				<input type="text" name="position">

				<label for="name">E-mail address</label>
				<input type="text" name="email">

				<input type="submit" value="Get <?=$whitepaper['type']?>">

			</form>

			<div class="thanks-message">
				<?php if ( $modal_thanks = get_field('whitepaper_modal_thankyou-text', 'options') ) : ?>
					<?php echo $modal_thanks; ?>
				<?php endif; ?>
			</div>

			<a href="#" target="_blank" style="display: none;" class="button whitepaper-download-button">Download <?=$whitepaper['type']?></a>

		</div>
	</div>
</div>