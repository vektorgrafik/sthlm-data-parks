<?php
global $socialMedia, $li;

// Fetch tweets
//$twitterData = $socialMedia->Twitter->byAccount('sthlmdataparks')->load()->get(3);

// Fetch WP Posts
$news = get_news_posts([
	'posts_per_page' => 3
]);


$linkedinData = [];
// Fetch LinkedIn Posts
if ( class_exists( 'Linkedin_Company_Updates' ) ) {

	$args = array(
	    'con_class' => 'linkedin-container',
	    'item_class' => 'linkedin-item',
	    'company' => '15262855',
	    'limit' => '3',
	    'raw_data' => true
	);
	$linkedInData = $li->get_updates( $args );
} else { $linkedInData = false; }

?>

<div class="page-header__top-bar">
	<div class="social-feed">
		<div class="social-feed__slider owl-carousel">

			<?php $i = 0; while( $i < 3 ) : ?>

				<div class="social-feed__slider-item row expanded small-up-3 collapse">
		
					<div class="column">
						<?php if( !empty($twitterData) && array_key_exists($i, $twitterData)) : ?>
						<a class="social-feed__item" href="<?=get_permalink( get_option( 'page_for_posts' ) );?>">
							<i class="icon icon-twitter color-primary"></i>
							<time><?=date('M, Y', strtotime($twitterData[$i]->created_at));?> |&nbsp;</time>
							<span class="social-feed__text"><?=htmlentities( $twitterData[$i]->text, ENT_QUOTES ) ?? null;?></span>
						</a>
						<?php endif; ?>
					</div>

					<div class="column">
						<?php if( is_array($linkedInData) && array_key_exists($i, $linkedInData)) : ?>
						<a class="social-feed__item" href="<?=get_permalink( get_option( 'page_for_posts' ) );?>">
							<i class="icon icon-linkedin color-primary"></i>
							<time><?=date('M, Y', (int) ($linkedInData[$i]['timestamp'] / 1000));?> |&nbsp;</time>
							<span class="social-feed__text"><?=htmlentities( $linkedInData[$i]['updateContent']['companyStatusUpdate']['share']['comment'], ENT_QUOTES ) ?? null;?></span>
						</a>
						<?php endif; ?>
					</div>

					<div class="column">
						<a class="social-feed__item" href="<?=get_permalink( get_option( 'page_for_posts' ) );?>">
							<i class="icon icon-article color-primary"></i>
							<time><?=date('M, Y', strtotime( $news->posts[$i]->post_date_gmt ));?> |&nbsp;</time>
							<span class="social-feed__text"><?=$news->posts[$i]->post_title ?? null;?></span>
						</a>
					</div>

				</div>

			<?php $i++; endwhile; ?>
	
		</div>
	</div>
</div>