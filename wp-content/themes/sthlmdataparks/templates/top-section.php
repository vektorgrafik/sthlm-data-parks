<?php 
	
	// Define default section class
	$section_class = '';

	if ( is_front_page() ) {
		// If front page
		$section_class = ' top-section--home';

	} else {

		// Get background image
		$queried_object = get_queried_object();
	 	$attachment_id = get_post_thumbnail_id( $queried_object->ID );
		$image = wp_get_attachment_image_src( $attachment_id, 'fullscreen');

		if ( $image ) {
			$section_class = ' top-section--image';
		} 

		// Get top section content
		$content_fields = get_field('page_content', get_queried_object()->ID );
		$content_empty = true;

		foreach ( $content_fields as $field ) {

			if ( ! empty( $field ) ) {
				$content_empty = false;
			}
		}

	} 

	if ( $background = get_field('bg_color', get_queried_object()->ID ) ) {

		$section_class .= ' bg-' . $background;

	}
		

?>


<?php if ( is_front_page() && get_field( 'fullscreen_video' ) ) : ?>
	<?php $fullscreen_video = get_field( 'fullscreen_video' ); ?>
	<?php $fullscreen_video_type = get_field( 'fullscreen_video_type' ); ?>
		<div class="video-container">
			
<!-- 			<div class="temporary-banner"><a href="https://stockholmdataparks.com/resources/#whitepapers"><img src="<?php //echo get_stylesheet_directory_uri() . '/assets/images/sdp-banner-whitepaper-jan-2020-black.png';?>"/></a></div> -->
			
			
<!-- 			<div class="temporary-banner-second"><a href="https://stockholmdataparks.com/energy-smart-stockholm-2020/" target="_blank" rel="nofollow"><img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/SDP-banner-post-event.png';?>"/></a></div> -->
			

			<?php if($fullscreen_video_type == 'Youtube'): ?>
				<iframe class="video-container__video" width="1920" height="1080" src="https://www.youtube.com/embed/<?php echo $fullscreen_video; ?>?&autoplay=0&showinfo=0&vq=highres&rel=0&loop=0&playlist=<?php echo $fullscreen_video; ?>" allowfullscreen frameborder=0></iframe>
			<?php endif; ?>

			<?php if($fullscreen_video_type == 'Vimeo'): ?>
				<iframe src="https://player.vimeo.com/video/<?php echo $fullscreen_video; ?>?autoplay=0&loop=0&title=0&byline=0&portrait=0" width="1920" height="1080" frameborder="0" allow="autoplay" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
			<?php endif; ?>
			
		</div>

<?php else : ?>

<div class="top-section <?=$section_class;?>">

	<?php if ( is_front_page() && get_field( 'top_section_slider' ) ) : ?>

		<?php echo get_template_part( 'templates/slider', 'top-section' ); ?>

		<div class="top-section__inner row">	
		</div> <!-- end .top-section__inner -->

	<?php else : ?>

		<?php if ( $image ) : ?>
			<div class="top-section__bg" style="background-image: url(<?php echo $image[0]; ?> ); <?php if( is_page(1863) ) { echo 'position:relative; background-position: 70% center;';  }?>"> <?php
				
				if( is_page(1863) ) { /* quick fix for campaign page 20200420 */ ?>
					
					<div class="campain-2020-banner"><img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/campaign_page_banner.jpg';?>"/></div> <?php
				
				} ?>
			
				
			</div>
		<?php endif; ?>

		<?php if ( $content_empty == false ) : 

			$content_layout = get_field( 'top_section_layout', get_queried_object()->ID );

			if ( $content_layout == 'one' ) {
				$section_class = 'top-section__inner--one-col';
				$row_class = 'space space--large';
				$column_class = 'small-12 medium-10 large-10 medium-offset-1 large-offset-1 text-center';
			} else {
				// assume two is selected

				$section_class = 'top-section__inner--two-col';
				$row_class = 'space large-unstack';
				$column_class = '';
			}


			$content_title = $content_fields['content_title'] ?? null;
			$content_intro = $content_fields['content_intro'] ?? null;
			$content_text = $content_fields['content_text'] ?? null;

			//dd($content_layout);


		?>
			<div class="top-section__inner  <?=$section_class;?>">	
				<div class="row <?=$row_class;?>">

					<?php if ( $content_title ) : ?>

						<div class="column <?=$column_class;?>">
							
							<?php $h1_color = ''; if( $background == 'primary' || $background == 'black' ) { $h1_color = 'style="color:#fff;"'; } ?>
							
							<h1 class="top-section__title h1" <?php echo $h1_color; ?>><?php echo $content_title; ?></h1>		
						</div> <!-- end .column -->

					<?php endif; ?>

					<?php if ( $content_intro || $content_text ) : ?>

						<div class="column <?=$column_class;?>">

							<?php if ( $content_intro ) : 
								$content_intro = str_replace('<p>', '<p class="intro">', $content_intro); 
							?>

								<div class="top-section__intro color-primary">
									<?php echo $content_intro; ?>
								</div> <!-- end .top-section__intro -->

							<?php endif; ?>

							<?php if ( $content_text ) : ?>

								<div class="top-section__text">
									<?php echo $content_text; ?>
								</div> <!-- end .top-section__text -->

							<?php endif; ?>

						</div> <!-- end .column -->

					<?php endif; ?>
						
				</div> <!-- end .row -->
			</div> <!-- end .top-section__inner -->
		<?php endif; ?>

	<?php endif; ?>
<?php endif; ?>	

</div> <!-- end .top-section -->