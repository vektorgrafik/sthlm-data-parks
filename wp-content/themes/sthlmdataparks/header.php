<?php
//define('VG_GOOGLE_ANALYTICS_ID', 'UA-90723861-1');
?>

<!doctype html>

  <html class="no-js" <?php language_attributes(); ?>>

	<head>
		<meta charset="utf-8">

		<!-- Force IE to use the latest rendering engine available -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta class="foundation-mq">

		<!-- If Site Icon isn't set in customizer -->
		<?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) { ?>

			<!-- Icons & Favicons -->
			<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
			<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/apple-touch-icon.png">
			<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon-32x32.png" sizes="32x32">
			<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon-16x16.png" sizes="16x16">
			<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/manifest.json">
			<link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/safari-pinned-tab.svg" color="#29201d">
			<meta name="theme-color" content="#ffffff">

			<!--[if IE]>
				<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
			<![endif]-->
			<meta name="msapplication-TileColor" content="#f01d4f">
	    <?php } ?>

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyA3R05fd-vPWsgtSVoFypiRalkNAZ9W1f8&language=sv" type="text/javascript"></script>

		<?php wp_head(); ?>

				<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-K9PQD5D');</script>
		<!-- End Google Tag Manager -->

		<?php //vg_google_analytics(); ?>


		<!--
		/**
		 * @license
		 * MyFonts Webfont Build ID 3310052, 2016-11-17T09:20:47-0500
		 *
		 * The fonts listed in this notice are subject to the End User License
		 * Agreement(s) entered into by the website owner. All other parties are
		 * explicitly restricted from using the Licensed Webfonts(s).
		 *
		 * You may obtain a valid license at the URLs below.
		 *
		 * Webfont: NHaasGroteskTXStd-55Rg by Linotype
		 * URL: http://www.myfonts.com/fonts/linotype/neue-haas-grotesk/std-text-55-roman/
		 *
		 * Webfont: NHaasGroteskTXStd-65Md by Linotype
		 * URL: http://www.myfonts.com/fonts/linotype/neue-haas-grotesk/std-text-65-medium/
		 *
		 *
		 * License: http://www.myfonts.com/viewlicense?type=web&buildid=3310052
		 * Licensed pageviews: 250,000
		 * Webfonts copyright: Copyright &#x00A9; 2013 Monotype Imaging Inc. All rights
		 * reserved.
		 *
		 * © 2016 MyFonts Inc
		*/

		-->
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/fonts/NHaasGroteskTXStd/MyFontsWebfontsKit.css">


        <!----- Cookiebot---->
        <script id="Cookiebot" src="https://consent.cookiebot.com/uc.js" data-cbid="906626e8-981d-47b9-8645-b99f2f9d3013" type="text/javascript" async></script>


	</head>

	<!-- Uncomment this line if using the Off-Canvas Menu -->

	<body <?php body_class(); ?>>

				<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K9PQD5D"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->

		<div class="default-overlay"></div>

		<div class="whitepaper-modal">

		</div>

		<div class="off-canvas-wrapper">

			<div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>

				<?php get_template_part( 'templates/content', 'offcanvas' ); ?>

				<header class="page-header header" role="banner">

					<?php //get_template_part('templates/top-section', 'social'); ?>

					<?php //get_template_part('templates/top-section', 'banner'); ?>

					<div class="page-header__main-bar">

						<div class="row">

							<div class="column shrink">

								<a href="<?php echo get_bloginfo('url'); ?>" class="page-header__logo"><?php echo get_bloginfo('name'); ?></a>

							</div> <!-- end .column -->

							<div class="column">

								<?php vektor_main_nav(); ?>

							</div> <!-- end .column -->

						 </div> <!-- end .row -->

						 <button class="menu-toggle" data-toggle="off-canvas">
						 	<span class="menu-toggle__bar"></span>
						 	<span class="menu-toggle__bar"></span>
						 	<span class="menu-toggle__bar"></span>
						 </button>

					</div>	<!-- end .page-header__main-bar -->

				</header> <!-- end .header -->

				<div class="off-canvas-content" data-off-canvas-content>
