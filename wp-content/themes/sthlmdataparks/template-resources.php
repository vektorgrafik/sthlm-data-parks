 <?php
/*
* Template Name: Resources - page
*/

?>

<?php get_header(); ?>

	<?php get_template_part( 'templates/top', 'section' ); ?>

	<?php 

	$taxonomy = 'resources_tax';
	$resource_terms = get_terms( array(

			'taxonomy' 		=> $taxonomy,
			'orderby' 		=> 'menu_order',
			'order' 		=> 'ASC',
			'hide_empty' 	=> true,
			'meta_key'		=> 'hide_on_page',
			'meta_value'	=> 0

		) );

	
	if ( $resource_terms ) : ?>

		<section class="resources">

		<?php foreach ( $resource_terms as $term ) : ?>

			<?php 

				$term_id = $term->term_id;
				$term_slug = $term->slug;
				$term_name = $term->name;
				$term_desc = $term->description;
				$term_bg = get_field('bg_color', $taxonomy . '_' . $term_id) ?? null;
				
				$lead_form = get_field('lead_form', $taxonomy . '_' . $term_id) ?? false;

				if ( $term_bg ) {
					$bg_class = 'bg-' . $term_bg;
				} else {
					$bg_class = '';
				}

			?>

			<div id="<?= $term_slug ;?>" class="resource-term <?= $bg_class ;?>">
				<div class="row space space--large">

					<div class="column small-12 large-5">
						
						<h2 class="resource-term__title h3"><?=$term_name;?></h2>
						<div class="resource-term__desc">
							<p><?=$term_desc;?></p>
						</div>

					</div> <!-- end .column -->

					
					
					<?php // Get resources posts
						$resources_args = array(
							'post_type'		=> 	'resources',
							'orderby' 		=> 'menu_order',
							'order' 		=> 'ASC',
							'tax_query' 	=> 	array(
											array(
												'taxonomy' => 'resources_tax',
												'field'    => 'term_id',
												'terms'    => $term_id,
											),
										),

						);
						$resources_query = new WP_Query( $resources_args );
					?>

					<?php if ( $resources_query->have_posts() ) : ?>

						<div class="column small-12 large-6 large-offset-1">

							<?php while ( $resources_query->have_posts() ) : $resources_query->the_post(); ?>

								<div class="resource-item">

									<?php 

										$resource_id = $post->ID;
										$resource_name = $post->post_title;

										$resource_format = get_post_format( $resource_id ) ? : 'standard';

										if ( $resource_format == 'standard') {

											$resource_fields = get_field_objects( $resource_id );

											$resource_pdf = $resource_fields['resource_pdf']['value'] ?? null;
											$resource_desc = $resource_fields['resource_desc']['value'] ?? null;
											
											if ( $lead_form /* $term_slug == 'whitepapers' || $term_slug == 'presentations' */ ) {
												
												echo '<a href="#" class="resource-item__pdf partner__resource resource-item--' . $term_slug . ' lead-form link-download" data-type="' . $term_name . '" data-name="' . $resource_name .'" data-file="'.$resource_pdf['filename'].'" target="_blank">' . $resource_name .' (PDF)</a>';
												
											} else {
												
												echo '<a href="' . $resource_pdf['url'] . '" class="resource-item__pdf partner__resource resource-item--' . $term_slug . ' link-download" data-type="' . $term_name . '" data-name="' . $resource_name .'" target="_blank">' . $resource_name .' (PDF)</a>';

											}

											if ( $resource_desc ) {
												echo str_replace('<p>', '<p class="resource-item__desc resource__external">', $resource_desc); 
											}

										} else if ( $resource_format == 'link' ) {

											$resource_fields = get_field_objects( $resource_id );
											$resource_url = $resource_fields['resource_url']['value'] ?? null;

											echo '<p class="resource__external resource-item__url"><a href="' . $resource_url . '" class="link__external" target="_blank">' . $resource_name .'</a></p>';
										}

									?>	

								</div> <!-- resource-item -->	
								
							<?php endwhile; ?>
						
						</div>  <!-- end .column -->

					<?php endif; ?>
						
					

				</div>  <!-- end .row -->
			</div>  <!-- end .resource-term -->

		<?php endforeach; ?>

		</section> <!-- end .resources -->

	<?php endif ; ?>

	

<?php get_footer(); ?>