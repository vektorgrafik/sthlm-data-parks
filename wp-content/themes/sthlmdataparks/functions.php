<?php

define('ASSETS_FOLDER', get_template_directory() . '/assets/' );
define('FUNCTIONS_FOLDER', ASSETS_FOLDER . '/functions/' );
define('TRANSLATIONS_FOLDER', ASSETS_FOLDER . '/translation/' );
define('CLASS_FOLDER', ASSETS_FOLDER . '/classes/' );

// Constants used throughout site
require_once ( FUNCTIONS_FOLDER . 'constants.php' );

// Engage output-buffering
require_once ( FUNCTIONS_FOLDER . 'outputbuffer.php' );

// Various helper functions
require_once( FUNCTIONS_FOLDER . 'helpers.php' ); 

// Theme support options
require_once( FUNCTIONS_FOLDER . 'theme-support.php' ); 

// WP Head and other cleanup functions
require_once( FUNCTIONS_FOLDER . 'cleanup.php' ); 

// Register scripts and stylesheets
require_once( FUNCTIONS_FOLDER . 'enqueue-scripts.php' ); 

// Register custom menus and menu walkers
require_once( FUNCTIONS_FOLDER . 'menu.php' ); 
require_once( FUNCTIONS_FOLDER . 'menu-walkers.php' ); 

// Register sidebars/widget areas
require_once( FUNCTIONS_FOLDER . 'sidebar.php' ); 

// Makes WordPress comments suck less
require_once( FUNCTIONS_FOLDER . 'comments.php' ); 

// Replace 'older/newer' post links with numbered navigation
require_once( FUNCTIONS_FOLDER . 'page-navi.php' ); 

// Adds support for multiple languages
require_once( TRANSLATIONS_FOLDER . 'translation.php' ); 

// Adds site styles to the WordPress editor
require_once(get_template_directory().'/assets/functions/editor-styles.php'); 

// Related post function - no need to rely on plugins
// require_once(get_template_directory().'/assets/functions/related-posts.php'); 

// Use this as a template for custom post types
require_once( get_template_directory() . '/assets/functions/custom-post-type.php' );

// Customize the WordPress login menu
require_once( get_template_directory() . '/assets/functions/login.php' ); 

// Customize the WordPress admin
require_once( get_template_directory() . '/assets/functions/admin.php' ); 

// Sthlm Data Parks custom functions
require_once( FUNCTIONS_FOLDER . 'site.php' );

// Vektor Maintenance mode
require_once( FUNCTIONS_FOLDER . 'plugins/vektor-maintenance-mode.php' );