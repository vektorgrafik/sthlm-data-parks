<?php get_header(); ?>

<?php 

	$site = get_queried_object(); 

	$site_fields = get_field_objects( 'term_' . $site->term_id );
	//dd($site_fields);

	// Get background image
	$image = null;
 	$site_image = $site_fields['site_image']['value'] ?? null;

 	if ( $site_image ) {

		$image = $site_image['sizes']['fullscreen'];

	} else if ( $property_landing_page = get_field('page_properties', 'options') ) {

		$landing_page_id = url_to_postid( $property_landing_page );
		$landing_attachment_id = get_post_thumbnail_id( $landing_page_id );
		$image = wp_get_attachment_image_src( $landing_attachment_id, 'fullscreen')[0];
	}

	$property_details_repeater 	= $site_fields['property_details_repeater']['value'] ?? null;
	$property_details_count = count($property_details_repeater);


?>

	<?php if ( $image ) : ?>

		<div class="top-section top-section--image">
			<div class="top-section__bg" style="background-image: url(<?php echo $image; ?> );"></div>
		</div>

	<?php endif; ?>

	<?php $properties_args = array(

		'post_type' 		=> 'properties',
		'posts_per_page' 	=> -1,
		'orderby'			=> 'menu_order',
		'order'				=> 'ASC',
		'tax_query' 		=> 	array(
									array(
										'taxonomy' => 'sites',
										'field'    => 'term_id',
										'terms'    => $site->term_id,
									),
								),
	);

	$properties_query = new WP_Query( $properties_args );
	if ( $properties_query->have_posts() ) : ?>
		
		<span class="scroll-menu__anchor">
			<div class="scroll-menu property-list-wrap">
				<div class="row collapse">
					<div class="column small-12">

						<ul class="scroll-menu__inner property-list list--clean">

							<?php while ( $properties_query->have_posts() ) : $properties_query->the_post(); ?>

								<?php 
									//PROPERTY 

									$property_list_id = $post->ID;
									$property_list_title = $post->post_title;
									$property_list_slug = $post->post_name;

									if ( isset($property_ID) && $property_ID == $property_list_id ) { 
										$link_class = 'is-active';
									} else {
										$link_class = '';
									}
								?>

								<li class="property-list__item">
									<a href="<?php echo get_permalink( $property_list_id ); ?>" class="property-list__link <?= $link_class ;?>"><?= $property_list_title ;?></a>
								</li>

							<?php endwhile; ?>

						</ul> <!-- end .property-list -->
			
					</div> <!-- end .column -->
				</div> <!-- end .row -->

				<button class="scroll-menu__nav scroll-menu__nav--left"></button>
				<button class="scroll-menu__nav scroll-menu__nav--right"></button>

			</div> <!-- end .property-list-wrap -->
		</span>

	<?php endif; wp_reset_postdata(); ?>

	<?php if ( $property_landing_page = get_field('page_properties', 'options') ) : ?>

		<section class="breadcrumb">
			<div class="row small-up-1">
				<div class="column"><a href="<?= $property_landing_page ;?>" class="breadcrumb__link"><p><span class="breadcrumb__arrow icon-angle-left"></span><?php echo __('Back to Parks', 'vektor'); ?><p></a></div>
			</div>
		</section>

	<?php endif; ?>
			
	<section class="sites-section">

		<article class="site-item">

			<div class="row small-up-1 medium-up-2 space">	
				
				<div class="column">

					<h1 class="site-item__title"><?php echo $site->name; ?></h1>

					<?php if ( $desc = $site->description ) : ?>
						<div class="site-item__desc">
							<p><?php echo $desc; ?></p>
						</div>
					<?php endif; ?>
		
				</div> <!-- end .column -->

				<?php if ( $property_details_count > 0 ) : 

						// Main counter
						$i = 1;

						// Counter for column 1
						$col_1_i = 1;

						// Counter for column 2
						$col_2_i = 2;

						$column_count = 2;
						$column_items_count = ceil($property_details_count / $column_count);

				?>

					<div class="column">

						<div class="property-details">
							<div class="row small-up-1 medium-up-<?= $column_count ;?> space">	

								<?php foreach ( $property_details_repeater as $detail_item ) : ?>

									<?php 
										if ( $i <= $column_items_count ) {
											// If first column this item order is the following
											$detail_item_order = 'order-' . $col_1_i;

											// Add iteration for following item in same column
											$col_1_i = $col_1_i + 2;
											
										} else {
											// If second column this item order is the following
											$detail_item_order = 'order-' . $col_2_i;

											// Add iteration for following item in same column
											$col_2_i = $col_2_i + 2;

										}

									?>

									<div class="column <?php echo $detail_item_order; ?>">
									
										<div class="property-detail-item">

											<h4 class="property-detail-item__heading h5 color-primary"><?php echo $detail_item['property_detail_title']; ?></h4>
											<p class="property-detail-item__value intro"><?php echo $detail_item['property_detail_value']; ?></p>

										</div> <!-- end .property-detail-item -->
									
									</div> <!-- end .column -->
									
								<?php $i++; endforeach; ?>

							</div> <!-- end .row -->
						</div> <!-- end .property-details -->

					</div> <!-- end .column -->
					
				<?php endif; ?>

			</div> <!-- end .row -->

		</article> <!-- end .site-item -->

	</section>

<?php get_footer(); ?>
