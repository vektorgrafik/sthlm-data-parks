// Grab our gulp packages
var gulp = require('gulp'),
    sass = require('gulp-sass'),
    gutil = require('gulp-util'),
    bower = require('gulp-bower'),
    babel = require('gulp-babel'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    replace = require('gulp-replace'),
    plumber = require('gulp-plumber'),
    stylish = require('jshint-stylish'),
    cleancss = require('gulp-clean-css'),
    sourcemaps = require('gulp-sourcemaps'),
    livereload = require('gulp-livereload'),
    autoprefixer = require('gulp-autoprefixer');
    //browserSync = require('browser-sync').create();


// Compile Sass, Autoprefix, add Source Maps and Minify
gulp.task('styles', function() {
  return gulp.src('./assets/scss/**/*.scss')
    .pipe(plumber(function(error) {
            gutil.log(gutil.colors.red(error.message));
            this.emit('end');
    }))
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(autoprefixer({
            browsers: ['last 3 versions'],
            cascade: false
        }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./assets/css/'))
    .pipe(rename({suffix: '.min'}))
    .pipe(cleancss())
    .pipe(gulp.dest('./assets/css/'))
    //.pipe(browserSync.stream());
    .pipe(
      livereload({
        start: true
      })
    );
});

// JSHint, concat, and minify JavaScript
gulp.task('site-js', function() {
  return gulp.src([

        // Vektor App
  		'./assets/js/source/app/intro.js',
        './assets/js/source/app/module-resize-scroll.js',

        /*
         * ------------------------------------------------------------
         * Modules go here
         * ------------------------------------------------------------
        */

             // Newsfeed
            './assets/js/source/app/module-news-feed.js',

             // Properties
            './assets/js/source/app/module-properties.js',

             // Smooth scroll
            './assets/js/source/app/module-smooth-scroll.js',

             // Sliders
            './assets/js/source/app/module-sliders.js',

             // Overlay
            './assets/js/source/app/module-overlay.js',

             // Whitepaper Modal
            './assets/js/source/app/module-whitepaper-modal.js',

             // Newsletter Signup
            './assets/js/source/app/module-newsletter-signup.js',

            // Scroll menu
            './assets/js/source/app/module-scroll-menu.js',

             // Maps
            './assets/js/source/app/module-maps.js',

             // PhotoSwipe
            './assets/js/source/app/module-photoswipe.js',

             //Clickable
            './assets/js/source/app/module-clickable.js',

              //Social share
            './assets/js/source/app/module-share.js',

        /*
        * -------------------------------------------------------------
        */

        // Init and Outro
        './assets/js/source/app/init.js',
        './assets/js/source/app/outro.js',

        // Core init
        './assets/js/source/init-foundation.js',
        './assets/js/source/wp-foundation.js',

  ])
    .pipe(concat('app.js'))
    .pipe(babel({
        presets: ['es2015']
    }))
    .pipe(gulp.dest('./assets/js'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify().on('error', gutil.log))
    .pipe(gulp.dest('./assets/js'))
    //.pipe(browserSync.stream())
});

// JSHint, concat, and minify Foundation JavaScript
gulp.task('vendor-js', function() {
  return gulp.src([

  		// Foundation core - needed if you want to use any of the components below
         './vendor/foundation-sites/js/foundation.core.js',
         './vendor/foundation-sites/js/foundation.util.*.js',

        // Pick the components you need in your project

        // './vendor/foundation-sites/js/foundation.abide.js',
        // './vendor/foundation-sites/js/foundation.accordion.js',
        // './vendor/foundation-sites/js/foundation.accordionMenu.js',
        // './vendor/foundation-sites/js/foundation.drilldown.js',
        // './vendor/foundation-sites/js/foundation.dropdown.js',
        // './vendor/foundation-sites/js/foundation.dropdownMenu.js',
        // './vendor/foundation-sites/js/foundation.equalizer.js',
        // './vendor/foundation-sites/js/foundation.interchange.js',
        // './vendor/foundation-sites/js/foundation.magellan.js',
        './vendor/foundation-sites/js/foundation.offcanvas.js',
        // './vendor/foundation-sites/js/foundation.orbit.js',
        // './vendor/foundation-sites/js/foundation.responsiveMenu.js',
        // './vendor/foundation-sites/js/foundation.responsiveToggle.js',
        // './vendor/foundation-sites/js/foundation.reveal.js',
        // './vendor/foundation-sites/js/foundation.slider.js',
        // './vendor/foundation-sites/js/foundation.sticky.js',
        // './vendor/foundation-sites/js/foundation.tabs.js',
        // './vendor/foundation-sites/js/foundation.toggler.js',
        // './vendor/foundation-sites/js/foundation.tooltip.js',


        // Owl Carousel 2 (bower)
        './vendor/owl-carousel2/dist/owl.carousel.js',

        // Enllax (custom)
        // './vendor/enllax/dist/enllax.js',

        // Masonry (bower)
        // './vendor/masonry/dist/masonry.pkgd.js',

        // Pickadate (bower)
        // './vendor/pickadate/lib/picker.js', // required base
        // './vendor/pickadate/lib/picker.date.js',
        // './vendor/pickadate/lib/picker.time.js',

        // PhotoSwipe JS (bower)
        './vendor/photoswipe/dist/photoswipe.js',
        './vendor/photoswipe/dist/photoswipe-ui-default.js',

        // FastClick (bower)
        // './vendor/fastclick/lib/fastclick.js',

        // Masonry (bower)
        // './vendor/masonry/dist/masonry.pkgd.js',

        // Tipsy (bower)
        // './vendor/tipsy/src/javascripts/jquery.tipsy.js',

        // ScrollReveal (bower)
        './vendor/scrollreveal/dist/scrollreveal.js',

        // Vektor Custom Vendor Scripts
        './assets/js/source/vendor/**/*.js',
  ])
    .pipe(babel({
        presets: ['es2015'],
         ignore: [
           './vendor/photoswipe/dist/photoswipe.js',
           './vendor/photoswipe/dist/photoswipe-ui-default.js',
           'photoswipe.js',
         ]
    }))
    .pipe(replace(/'use strict';/g, ''))
    .pipe(concat('vendor.js'))
    .pipe(gulp.dest('./assets/js'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify().on('error', gutil.log))
    .pipe(gulp.dest('./assets/js'))
    //.pipe(browserSync.stream())
});


// Update Foundation with Bower and save to /vendor
gulp.task('bower', function() {
  return bower({ cmd: 'update'})
    .pipe(gulp.dest('vendor/'))
});

// Create a default task
gulp.task('default', function() {
  gulp.start('styles', 'site-js', 'vendor-js');
});

// Watch files for changes
gulp.task('watch', function() {

  // browserSync.init({
  //   files: ['{lib,templates}/**/*.php', '*.php'],
  //   proxy: 'sthlmdataparks.vgdev.se',
  //   snippetOptions: {
  //     whitelist: ['/wp-admin/admin-ajax.php'],
  //     blacklist: ['/wp-admin/**']
  //   }
  // });

  //livereload.listen({start: true});

  // Watch .scss files
  gulp.watch('./assets/scss/**/*.scss', ['styles']);

  // Watch site-js files
  gulp.watch('./assets/js/source/**/*.js', ['site-js', 'vendor-js']);

});
