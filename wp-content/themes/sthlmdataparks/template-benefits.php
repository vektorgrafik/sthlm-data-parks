<?php
/*
* Template Name: Benefits - page
*/

?>

<?php get_header(); ?>

	<?php get_template_part( 'templates/top', 'section' ); ?>

	<?php $benefits_args = array(

			'post_type' 		=> 'benefits',
			'posts_per_page' 	=> -1,
			'orderby'			=> 'menu_order',
			'order'				=> 'ASC'

		);

	$benefits_query = new WP_Query( $benefits_args );

	if ( $benefits_query->have_posts() ) : ?>
		<?php while ( $benefits_query->have_posts() ) : $benefits_query->the_post(); ?>

			<?php 

				// BENEFIT FIELDS

				$benefit_fields = get_field_objects($post->post_ID);

				$benefit_icon = $benefit_fields['benefit_icon']['value'] ?? null;
				$benefit_text = $benefit_fields['benefit_text']['value'] ?? null;
				
				$benefit_image = $benefit_fields['benefit_image']['value'] ?? '';
				$benefit_image_desc = $benefit_fields['benefit_image_desc']['value'] ?? null;
				$benefit_image_layout = $benefit_fields['benefit_image_layout']['value'] ?? null;

				//dump($benefit_image); 

			?>

			<section id="<?=$post->post_name;?>" class="benefit benefit--<?=$post->post_name;?>">

				<?php if ( $benefit_image_layout == 'standard' ) : ?>

					<div class="row small-up-1 medium-up-2 space space--large">

						<div class="column">

							<div class="benefit__content">
								
								<?php if ( $benefit_icon ) : ?>
									<figure class="benefit__icon">
										<?php echo '<div class="benefit__icon-inner ' .  $benefit_icon . '"></div>'; ?>
									</figure>
								<?php endif; ?>

								<h2 class="benefit__title h3"><?php echo $post->post_title; ?></h2>
								
								<?php if ( $benefit_text ) : ?>
									<div class="benefit__content-text"><?= $benefit_text ;?></div>
								<?php endif; ?>

							</div> <!-- end .benefit__content -->
							
						</div> <!-- end .column -->

						<?php if ( $benefit_text ) : ?>
							<div class="column">

								<div class="benefit__image-wrap">
									
									<img src="<?= $benefit_image['sizes']['large'] ;?>" alt="<?= $benefit_image['alt'] ;?>" class="benefit__image">
									
								</div> <!-- end .benefit__content -->
								
							</div> <!-- end .column -->
						<?php endif; ?>

					</div> <!-- end .row -->

				<?php else : ?>

					<div class="row row--fullscreen collapse medium-unstack">

						<div class="column">

							<div class="benefit__content benefit__content--full space space--large">
								
								<?php if ( $benefit_icon ) : ?>
									<figure class="benefit__icon">
										<?php echo '<div class="benefit__icon-inner ' .  $benefit_icon . '"></div>'; ?>
									</figure>
								<?php endif; ?>

								<h2 class="benefit__title h3"><?php echo $post->post_title; ?></h2>
								
								<?php if ( $benefit_text ) : ?>
									<div class="benefit__content-text"><?= $benefit_text ;?></div>
								<?php endif; ?>

							</div> <!-- end .benefit__content -->
							
						</div> <!-- end .column -->

						<div class="column relative">

							<?php if ( $benefit_image_desc ) : ?>
								<div class="benefit__image-wrap benefit__image-wrap--full">
									<div class="row align-middle">
										<div class="column">
											<h4 class="benefit__image-text"><?php echo $benefit_image_desc; ?></h4>
										</div>
									</div>
								</div>
							<?php endif; ?>

						</div> <!-- end .column -->


					</div> <!-- end .row -->

				<?php endif; ?>

			</section> <!-- end .benefit -->

		<?php endwhile; ?>
	<?php endif; wp_reset_postdata(); ?>

	<?php get_template_part('templates/flexible-content'); ?>

<?php get_footer(); ?>