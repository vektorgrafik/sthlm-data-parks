<?php
	
	global $socialMedia, $li;

	$twitterData = $socialMedia->Twitter->byAccount('sthlmdataparks')->load()->get(3);

	$linkedinData = [];
	
	// Fetch LinkedIn Posts
	if ( class_exists( 'Linkedin_Company_Updates' ) ) {
		$args = array(
		    'con_class' => 'linkedin-container',
		    'item_class' => 'linkedin-item',
		    'company' => '15262855',
		    'limit' => '3',
		    'raw_data' => true
		);
		$linkedInData = $li->get_updates( $args );
	}

?>


<aside class="sidebar sidebar--blog column small-order-2 medium-order-1">
	
	<div class="row small-up-1 sidebar-block">
		
		<div class="column">
			<h2 class="sidebar__title blog-section__heading h3"><?php _e('Linkedin', 'vektor'); ?><span class="sidebar__title-icon icon-linkedin"></span></h2>
			
	
	
	
		</div> <!-- end .column -->
		
	</div> <!-- end .row -->
	
	
	
			
			
	<div class="row small-up-1 sidebar-block">
		
		<div class="column">
			
			
			

			<?php foreach( $linkedInData as $linkedInItem ) :  ?>

		
				<div class="sidebar__item social-item social-item--linkedin">
			
					<p class="social-item__meta text-small">
						By <span class="entry-author">Stockholm Data Parks</span> - 
						<time class="entry-date"><?=date('M, Y', (int) ($linkedInItem['timestamp'] / 1000));?></time> |&nbsp;<span class="entry-cat"><?php echo __('LinkedIn', 'vektor'); ?></span>
					</p>

					<span class="text-small"><p class="social-item__text strong"><?=htmlentities( $linkedInItem['updateContent']['companyStatusUpdate']['share']['comment'], ENT_QUOTES ) ?? null;?></p></span>
					<a class="social-item__link" href="<?php echo 'https://www.linkedin.com/company/15262855/' ?>" rel="nofollow" target="_blank"><?php echo __('View on LinkedIn', 'vektor'); ?></a>

				
				</div>


			<?php endforeach;  ?>

			</div> <!-- end .column -->
		
		</div> <!-- end .row -->
		
		
		
		
	<div class="row small-up-1 sidebar-block">			
		
 		<div class="column">
	 		
			<h2 class="sidebar__title blog-section__heading h3"><?php _e('Twitter', 'vektor'); ?><span class="sidebar__title-icon icon-twitter"></span></h2>
			
			
			
		</div> <!-- end .column -->
		
	</div> <!-- end .row -->
		
 
		
		
		<div class="row small-up-1 sidebar-block">
		
			<div class="column">
				
				
			
			<?php foreach( $twitterData as $tweet ) :  ?>

		
				<div class="sidebar__item social-item social-item--twitter">
			
					<p class="social-item__meta text-small">
						By <span class="entry-author"><?php echo $tweet->user->name; ?></span> - 
						<time class="entry-date"><?=date('M, Y', strtotime($tweet->created_at));?></time> |&nbsp;<span class="entry-cat"><?php echo __('twitter', 'vektor'); ?></span>
					</p>

					<span class="text-small"><p class="social-item__text strong"><?=htmlentities( $tweet->text, ENT_QUOTES ) ?? null;?></p></span>
					<a class="social-item__link" href="<?php echo 'https://twitter.com/statuses/' . $tweet->id; ?>" rel="nofollow" target="_blank"><?php echo __('View on twitter', 'vektor'); ?></a>
				
				</div>


			<?php endforeach;  ?>

			
			
			
			

		</div> <!-- end .column -->
		
	</div> <!-- end .row -->
	
</aside> <!-- end .sidebar -->