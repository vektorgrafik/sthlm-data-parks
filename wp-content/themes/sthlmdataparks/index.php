<?php get_header();?>

	<?php get_template_part( 'templates/top', 'section' ); ?>

	<section class="blog-section">

		<div class="row">

			<?php  //get_sidebar( 'blog' ); ?>
	
		    <main id="main" class="blog-section__main small-12 large-10 medium-10 small-order-1 medium-order-2 columns" role="main" style="margin:0 auto;">
		    
				<div class="news-feed">
					<h2 class="news-feed__heading h3"><?php echo __('Articles and blog posts', 'vektor'); ?></h2>
					
					
<!--
					
					<?php echo $GLOBALS['wp_query']->request; ?>
					
-->
					
				    <?php
					       					  
				 
				 
				 //wp_reset_query();
				    
				    //error_log('POSTS PER PAGE: ' . get_option( 'posts_per_page' ));
				    

				   // if (/* $posts-> */have_posts()) : while (/* $posts-> */have_posts()) : /* $posts-> */the_post();
				    
				    
				 if (have_posts()) : while (have_posts()) : the_post(); 

						get_template_part( 'templates/loop', 'news' );
						
				
					    
					endwhile;
					
					//wp_reset_postdata();
					?>
				</div>
				
				
				<?php
					
					 // Fetch news posts
				    $posts = get_news_posts([
					    
				    	'posts_per_page' => NEWS_POSTS_PER_PAGE

				    ]);

?>
								
				
				<?php
				if( $posts->found_posts > $posts->post_count )
					echo '<a href="#" data-offset="' .  get_option( 'posts_per_page' ) . '" id="load-more-news" class="button button--primary"> <div class="news-button">' . __('Load more', 'vektor') . '</div></a>'
				?>

<!-- 					<?php vektor_page_navi(); ?> -->
					
				<?php else : ?>
											
					<?php get_template_part( 'templates/content', 'missing' ); ?>
						
				<?php endif; ?>
																								
		    </main> <!-- end #main -->

		</div> <!-- end .row -->

	</section> <!-- end .blog -->

	<?php 
		
		global $post;

		$qo = get_queried_object();

		$post->ID = $qo->ID;

		get_template_part('templates/flexible-content'); 

	?>

<?php get_footer(); ?>