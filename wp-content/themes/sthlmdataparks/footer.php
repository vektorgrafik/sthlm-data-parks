					<?php 

						if ( function_exists('get_field') ) {

							$company_name = get_field('company_name', 'option') ?? '';
							$company_address = get_field('company_address', 'option') ?? '';
							$company_post_code = get_field('company_post_code', 'option') ?? '';

							$contact_mail = get_field('contact_mail', 'option') ?? '';
							$contact_phone = get_field('contact_phone', 'option') ?? '';

						}

					?>
                    <section class="newsletter-section" style="display:none;">
                        <div class="row">
                            <div class="small-12 medium-10 large-9">
                                <div id="newsletter-wraper" class=" small-12 columns">

                                    <h5 style="margin-top: 0; margin-bottom: .5em;">Sign up for our newsletter</h5>
                                    <p>Sign up for our newsletter to receive news and information about Stockholm Data Parks and invitations to events that we organize.</p>
                                    <div id="newsletter-btn">
<!--                                         <a target="_blank" href="http://eepurl.com/dsIPpz"> Sign up! -->
                                        </a>
                                    </div>

                                </div>

                            </div>
                        </div>

                    </section>

					<footer class="page-footer footer" role="contentinfo">

						<div id="page-footer__inner" class="row page-footer__inner">

							<div class="large-6 small-12 columns">
								
								<ul class="list list--clean">

									<?php if ( $company_name ) : ?>
										<li class="list__item list__item--highlight"><?=$company_name;?></li>
									<?php endif; ?>

									<?php if ( $company_address ) : ?>
										<li class="list__item"><?=$company_address;?></li>
									<?php endif; ?>

									<?php if ( $company_post_code ) : ?>
										<li class="list__item"><?=$company_post_code;?></li>
									<?php endif; ?>

									<?php if ( $contact_mail ) : ?>
										<li class="list__item"><a href="mailto:<?=$contact_mail;?>" class="list__link"><?php echo $contact_mail;?></a></li>
									<?php endif; ?>

									<?php if ( $contact_phone ) : ?>
										<li class="list__item list__item--highlight">
											<a href="<?php echo 'tel:' . str_replace(' ', '', $contact_phone); ?>" class="list__link"><?php echo $contact_phone;?></a>
										</li>
									<?php endif; ?>

								</ul>

							</div>

							<div class="large-6 small-12 columns">
								<nav role="navigation">
		    						<?php vektor_footer_links(); ?>
		    					</nav>
		    				</div>

		    				<div class="large-6 small-12 columns">

								<ul class="contact__social-list list list--clean list--horizontal">
								
									<?php if ( $social_twitter = get_field('social_twitter', 'options') ) : ?>
										<li class="list__item"><a href="<?=$social_twitter;?>" class="contact__social-list-icon"><span class="icon-twitter"></span></a></li>
									<?php endif; ?>

									<?php if ( $social_linkedin = get_field('social_linkedin', 'options') ) : ?>
										<li class="list__item"><a href="<?=$social_linkedin;?>" class="contact__social-list-icon"><span class="icon-linkedin"></span></a></li>
									<?php endif; ?>

								</ul>

		    				</div>




    						<?php $partners_args = array(

									'post_type' 		=> 'partners',
									'posts_per_page' 	=> -1,
									'orderby'			=> 'menu_order',
									'order'				=> 'ASC',
									'meta_key'			=> 'partner_show_footer',
									'meta_value'		=> true

								);

							$partners_query = new WP_Query( $partners_args );

							if ( $partners_query->have_posts() ) : ?>

								<div class="large-12 columns">
			    					<div class="partner-logos space--small">
			    						<!--<p class="partner-logos__title"><?php //echo _e('Partners', 'vektor'); ?></p>-->

			    						<div class="partner-logos__list">
										
											<?php while ( $partners_query->have_posts() ) : $partners_query->the_post(); ?>

													<?php 

														$partner_fields = get_field_objects($post->post_ID);
														$logo_white = $partner_fields['partner_logo_white']['value'] ?? null;

														//dump($logo_white);

													?>

													<div class="partner-logos__image-wrap">
														<img src="<?= $logo_white['sizes']['medium'] ;?>" alt="<?= $logo_white['alt'] ;?>" class="partner-logos__image">
													</div>

											<?php endwhile; wp_reset_postdata(); ?>	

										</div> <!-- end .partner-logos__list -->

			    					</div> <!-- end .partner-logos -->
			    				</div> <!-- end .columns -->

							<?php endif; ?>

						</div> <!-- end #inner-footer -->
					</footer> <!-- end .footer -->
				</div>  <!-- end .main-content -->
			</div> <!-- end .off-canvas-wrapper-inner -->
		</div> <!-- end .off-canvas-wrapper -->
		<?php wp_footer(); ?>
	</body>
</html> <!-- end page -->