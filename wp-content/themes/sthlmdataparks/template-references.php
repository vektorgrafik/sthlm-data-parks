<?php
/*
* Template Name: References - page
*/

?>

<?php get_header(); ?>

	<?php get_template_part( 'templates/top', 'section' ); ?>

	<?php 

		//GET REFERENCES
		// Get the reference terms
		$reference_terms = get_terms( array(

			'taxonomy' 	=> 'references_tax',
			'orderby' 	=> 'name',
			'order' 	=> 'ASC'

		) );

		if ( $reference_terms ) : foreach ( $reference_terms as $term ) : ?>

			<section id="<?= 'reference-term--' . $term->term_id ;?>" class="reference-term">
				
				
			<!-- 	<div class="row">
					<div class="column">
						<h2 class="reference-term__title h3"><?php echo $term->name; ?></h2>
					</div> 
				</div>	 -->

				

				<?php // Get reference posts
					$reference_args = array(
						'post_type'		=> 	'references',
						'orderby' 		=> 'menu_order',
						'order' 		=> 'ASC',
						'tax_query' 	=> 	array(
										array(
											'taxonomy' => 'references_tax',
											'field'    => 'term_id',
											'terms'    => $term->term_id,
										),
									),

					);
					$reference_query = new WP_Query( $reference_args );
				?>

				<?php if ( $reference_query->have_posts() ) : ?>

					<div class="row small-up-1 large-up-2">

						<?php while ( $reference_query->have_posts() ) : $reference_query->the_post(); 

							$resource_fields = get_field_objects( $post->ID );

							$reference_logo = $resource_fields['reference_logo_white']['value'] ?? null;
							$reference_title = $resource_fields['reference_title']['value'] ?? null;
							$reference_text = $resource_fields['reference_text']['value'] ?? null;
							$reference_resource = $resource_fields['reference_resource']['value'] ?? null;

							//dump($resource_fields);

						?>

							<div class="column">
								
								<div class="reference-item">

									<div class="reference-item__image" style="max-width: 250px; margin: 0 auto; background-image: url('<?= $reference_logo['sizes']['medium'] ;?>')">
									</div> <!-- end .reference-item__image -->

									<?php if ( $reference_title ) : ?>
										<h3 class="reference-item__title"><?= $reference_title ;?></h3>
									<?php endif; ?>

									<?php if ( $reference_text ) : ?>
										<div class="reference-item__content">
											<?= $reference_text ;?>
										</div> <!-- end .reference-item__content -->
									<?php endif; ?>

									<?php if ( $reference_resource ) : 

										$resource_id = $reference_resource[0]->ID;
										$resource_name = $reference_resource[0]->post_title;

										$resource_format = get_post_format( $resource_id ) ? : 'standard';

										if ( $resource_format == 'standard') {

											$resource_pdf = get_field('resource_pdf', $resource_id );

											//dd($resource_pdf);

											echo '<a href="' . $resource_pdf['url'] . '" class="partner__reference link-fat link-download" target="_blank">' . $resource_name .' (PDF)</a>';

										} ?>

									<?php endif; ?>

								</div> <!-- end .reference-item -->

							</div>	<!-- end .column -->

						<?php endwhile; ?> 

					</div>	<!-- end .row -->

			<?php endif; wp_reset_postdata(); ?>

	

			</section>

	<?php endforeach; endif; wp_reset_postdata(); ?>

	<?php get_template_part('templates/flexible-content'); ?>

<?php get_footer(); ?>