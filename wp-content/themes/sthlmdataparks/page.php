<?php

	$fields = get_field('page_content');
	$fields_empty = true;


	foreach ( $fields as $field ) {

		if ( ! empty( $field ) ) {
			$fields_empty = false;
		}
	}

?>

<?php get_header(); ?>

	<?php get_template_part( 'templates/top', 'section' ); ?>

	<?php get_template_part( 'templates/flexible-content' ); ?>

<?php get_footer(); ?>