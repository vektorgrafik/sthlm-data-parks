<?php
/*
* Template Name: About - page
*/

?>

<?php get_header(); ?>

	<?php get_template_part( 'templates/top', 'section' ); ?>

	<?php $partners_args = array(

			'post_type' 		=> 'partners',
			'posts_per_page' 	=> -1,
			'orderby'			=> 'menu_order',
			'order'				=> 'ASC'

		);

	$partners_query = new WP_Query( $partners_args );

	if ( $partners_query->have_posts() ) : ?>
		<?php while ( $partners_query->have_posts() ) : $partners_query->the_post(); ?>


			<?php 

				// PARTNER FIELDS

				$partner_fields = get_field_objects($post->post_ID);

				$logo_default = $partner_fields['partner_logo_default']['value'] ?? null;
				$logo_white = $partner_fields['partner_logo_white']['value'] ?? null;
				$about_text = $partner_fields['partner_about']['value'] ?? '';
				
				$resource = $partner_fields['partner_resource']['value'] ?? null;

				//dd($partner_fields);

			?>


			<section class="partner partner--<?=$post->post_name;?>">
				<div class="row large-unstack space space--large">
					
					<?php if ( $logo_default ) : ?>

						<div class="column">

							<div class="partner__logo-wrap">
								<img src="<?= $logo_default['url'] ;?>" alt="<?= $logo_default['alt'] ;?>" class="partner__logo">
							</div>
							
						</div> <!-- end .column -->

					<?php endif; ?>

					<div class="column">

						<div class="partner__content">
							<h2 class="partner__title h3"><?php echo $post->post_title; ?></h2>
							<?php if ( $about_text ) : ?>
								<div class="partner__content-text"><?= $about_text ;?></div>


								<?php if ( $resource ) : 

									$resource_id = $resource[0]->ID;
									$resource_name = $resource[0]->post_title;

									$resource_format = get_post_format( $resource_id ) ? : 'standard';

									if ( $resource_format == 'standard') {

										$resource_pdf = get_field('resource_pdf', $resource_id );
										$resource_desc = get_field('resource_desc', $resource_id );

										if ( $resource_desc ) {
											echo str_replace('<p>', '<p class="partner__external">', $resource_desc); 
										}
										if ( $resource_pdf ) {
											echo '<a href="' . $resource_pdf['url'] . '" class="partner__resource link-download" target="_blank">' . $resource_name .' (PDF)</a>';
										}
									}

								?>

								<?php endif; ?>

							<?php endif; ?>
						</div> <!-- end .partner__content -->
						
					</div> <!-- end .column -->
				</div> <!-- end .row -->
			</section>

		<?php endwhile; ?>
	<?php endif; wp_reset_postdata(); ?>

	<?php get_template_part('templates/flexible-content'); ?>

<?php get_footer(); ?>