<?php
// Register menus
register_nav_menus(
	array(
		'main-nav' => __( 'The Main Menu', 'vektor' ),   // Main nav in header
		'footer-links' => __( 'Footer Links', 'vektor' ) // Secondary nav in footer
	)
);

// The Top Menu
function vektor_main_nav() {
	 wp_nav_menu(array(
        'container' => true,                           // Remove nav container
        'menu_class' => 'main-menu menu',               // Adding custom nav class
        'items_wrap' => '<ul id="%1$s" class="%2$s" data-responsive-menu="accordion medium-dropdown">%3$s</ul>',
        'theme_location' => 'main-nav',        			// Where it's located in the theme
        'depth' => 5,                                   // Limit the depth of the nav
        'fallback_cb' => false,                         // Fallback function (see below)
        'walker' => new Topbar_Menu_Walker()
    ));
} /* End Top Menu */

// The Off Canvas Menu
function vektor_off_canvas_nav() {
	 wp_nav_menu(array(
        'container' => true,                           // Remove nav container
        'menu_class' => 'offcanvas-menu menu',                // Adding custom nav class
        'items_wrap' => '<ul id="%1$s" class="%2$s" data-accordion-menu>%3$s</ul>',
        'theme_location' => 'main-nav',        			// Where it's located in the theme
        'depth' => 5,                                   // Limit the depth of the nav
        'fallback_cb' => false,                         // Fallback function (see below)
        'walker' => new Off_Canvas_Menu_Walker()
    ));
} /* End Off Canvas Menu */

// The Footer Menu
function vektor_footer_links() {
    wp_nav_menu(array(
    	'container' => 'false',                         // Remove nav container
    	'menu' => __( 'Footer Links', 'vektor' ),   	// Nav name
    	'menu_class' => 'footer-menu menu',      					// Adding custom nav class
    	'theme_location' => 'footer-links',             // Where it's located in the theme
        'depth' => 0,                                   // Limit the depth of the nav
    	'fallback_cb' => ''  							// Fallback function
	));
} /* End Footer Menu */

// Header Fallback Menu
function vektor_main_nav_fallback() {
	wp_page_menu( array(
		'show_home' => true,
    	'menu_class' => '',      // Adding custom nav class
		'include'     => '',
		'exclude'     => '',
		'echo'        => true,
        'link_before' => '',                            // Before each link
        'link_after' => ''                             // After each link
	) );
}

// Footer Fallback Menu
function vektor_footer_links_fallback() {
	/* You can put a default here if you like */
}


/*
 * ------------------------------------------------------------
 * Vektor Breadcrumbs function
 * 0.9, still needs a bit of love before completely complete.
 * ------------------------------------------------------------
*/

function vektor_breadcrumbs( $args = array() ) {

    global $post;

    $args = wp_parse_args($args, array(
        
        'parent_id' => $post->ID,
        'include_front_page' => true,
        'is_category' => false
        
    ));

    $bc_html    = "";
    $ancestor   = get_post( $args['parent_id'] );
    $ancestors  = get_post_ancestors( $args['parent_id'] );

    // Include front page if defined
    $start_page = ( $args['include_front_page'] ) ? '<a href="/home/">Home</a>' : null; // TO DO: make dynamic.

    // Loop out all parents
    if( is_array( $ancestors )) foreach( $ancestors as $page ) {
        
        if( $page !== 2 )
            $bc_html .= '<a href="' . get_permalink( $page ) . '">' . get_the_title( $page ) . '</a>';
    }

    // Attach closest parent
    if( $args['parent_id'] !== $post->ID )
        $bc_html .= '<a href="' . get_permalink( $ancestor->ID ) . '">' . $ancestor->post_title . '</a>';

    // If category, then attach that, else just get current page.
    if( $args['is_category'] ) : 
        $bc_html .= '<a class="active" href="' . get_category_link( $args['is_category'][0]->ID ) . '">' . $args['is_category'][0]->name . '</a>';
    else :
        $bc_html .= '<a class="active" href="' . get_permalink( $post->ID ) . '">' . $post->post_title . '</a>';
    endif;

    $bc_html = $start_page . $bc_html;

    // Get template and spit it out.
    ob_start();
    include get_template_directory() . '/templates/part-breadcrumbs.php';

    echo ob_get_clean();

}

/*
* -------------------------------------------------------------
*/