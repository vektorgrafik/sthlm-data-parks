<?php

function vektor_post_types() { 
	

	register_post_type( 'benefits',
		array('labels' => array(
			'name' => __('Benefits', 'vektor'),
			'singular_name' => __('Benefit', 'vektor'),
			'all_items' => __('All Benefits', 'vektor'),
			'add_new' => __('Add new', 'vektor'),
			'add_new_item' => __('Add new benefit', 'vektor'),
			'edit' => __( 'Edit', 'vektor' ),
			'edit_item' => __('Edit benefit', 'vektor'),
			'new_item' => __('New benefit', 'vektor'),
			'view_item' => __('View benefit', 'vektor'),
			'search_items' => __('Search Benefits', 'vektor'),
			'not_found' =>  __('Nothing found in the Database.', 'vektor'),
			'not_found_in_trash' => __('Nothing found in Trash', 'vektor'),
			'parent_item_colon' => ''
			),
			'public' => true,
			'publicly_queryable' => false,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 4, 
			'menu_icon' => 'dashicons-thumbs-up', 
			'rewrite'	=> array( 'slug' => __('benefit', 'vektor'), 'with_front' => false ), 
			'has_archive' => 'benefit', 
			'capability_type' => 'post',
			'hierarchical' => true,
			
			'supports' => array( 'title' )
	 	)
	);

	register_post_type( 'partners',
		array('labels' => array(
			'name' => __('Partners', 'vektor'),
			'singular_name' => __('Partner', 'vektor'),
			'all_items' => __('All Partners', 'vektor'),
			'add_new' => __('Add new', 'vektor'),
			'add_new_item' => __('Add new partner', 'vektor'),
			'edit' => __( 'Edit', 'vektor' ),
			'edit_item' => __('Edit partner', 'vektor'),
			'new_item' => __('New partner', 'vektor'),
			'view_item' => __('View partner', 'vektor'),
			'search_items' => __('Search Partners', 'vektor'),
			'not_found' =>  __('Nothing found in the Database.', 'vektor'),
			'not_found_in_trash' => __('Nothing found in Trash', 'vektor'),
			'parent_item_colon' => ''
			),
			'public' => true,
			'publicly_queryable' => false,
			'exclude_from_search' => true,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 4, 
			'menu_icon' => 'dashicons-networking', 
			'rewrite'	=> array( 'slug' => __('partner', 'vektor'), 'with_front' => false ), 
			'has_archive' => 'partner', 
			'capability_type' => 'post',
			'hierarchical' => true,
			
			'supports' => array( 'title', 'editor' )
	 	)
	);

	register_post_type( 'properties',
		array('labels' => array(
			'name' => __('Properties', 'vektor'),
			'singular_name' => __('Property', 'vektor'),
			'all_items' => __('All Properties', 'vektor'),
			'add_new' => __('Add new', 'vektor'),
			'add_new_item' => __('Add new property', 'vektor'),
			'edit' => __( 'Edit', 'vektor' ),
			'edit_item' => __('Edit property', 'vektor'),
			'new_item' => __('New property', 'vektor'),
			'view_item' => __('View property', 'vektor'),
			'search_items' => __('Search Properties', 'vektor'),
			'not_found' =>  __('Nothing found in the Database.', 'vektor'),
			'not_found_in_trash' => __('Nothing found in Trash', 'vektor'),
			'parent_item_colon' => ''
			),
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 4, 
			'menu_icon' => 'dashicons-admin-multisite', 
			'rewrite'	=> array( 'slug' => __('property', 'vektor'), 'with_front' => false ), 
			'has_archive' => 'property', 
			'capability_type' => 'post',
			'hierarchical' => true,
			
			'supports' => array( 'title', 'editor', 'thumbnail' )
	 	)
	);

	register_post_type( 'references',
		array('labels' => array(
			'name' => __('References', 'vektor'),
			'singular_name' => __('Reference', 'vektor'),
			'all_items' => __('All references', 'vektor'),
			'add_new' => __('Add new', 'vektor'),
			'add_new_item' => __('Add new reference', 'vektor'),
			'edit' => __( 'Edit', 'vektor' ),
			'edit_item' => __('Edit reference', 'vektor'),
			'new_item' => __('New reference', 'vektor'),
			'view_item' => __('View reference', 'vektor'),
			'search_items' => __('Search References', 'vektor'),
			'not_found' =>  __('Nothing found in the Database.', 'vektor'),
			'not_found_in_trash' => __('Nothing found in Trash', 'vektor'),
			'parent_item_colon' => ''
			),
			'public' => true,
			'publicly_queryable' => false,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 4, 
			'menu_icon' => 'dashicons-format-chat', 
			'rewrite'	=> array( 'slug' => __('reference', 'vektor'), 'with_front' => false ), 
			'has_archive' => 'reference', 
			'capability_type' => 'post',
			'hierarchical' => true,
			
			'supports' => array( 'title', 'editor', 'thumbnail' )
	 	)
	);

	register_post_type( 'resources',
		array('labels' => array(
			'name' => __('Resources', 'vektor'),
			'singular_name' => __('Resources', 'vektor'),
			'all_items' => __('All Resources', 'vektor'),
			'add_new' => __('Add new', 'vektor'),
			'add_new_item' => __('Add new resources', 'vektor'),
			'edit' => __( 'Edit', 'vektor' ),
			'edit_item' => __('Edit resources', 'vektor'),
			'new_item' => __('New resources', 'vektor'),
			'view_item' => __('View resources', 'vektor'),
			'search_items' => __('Search Resources', 'vektor'),
			'not_found' =>  __('Nothing found in the Database.', 'vektor'),
			'not_found_in_trash' => __('Nothing found in Trash', 'vektor'),
			'parent_item_colon' => ''
			),
			'public' => true,
			'publicly_queryable' => false,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 4, 
			'menu_icon' => 'dashicons-format-chat', 
			'rewrite'	=> array( 'slug' => __('resource', 'vektor'), 'with_front' => false ), 
			'has_archive' => 'resource', 
			'capability_type' => 'post',
			'hierarchical' => true,
			
			'supports' => array( 'title', 'editor', 'thumbnail', 'post-formats' )
	 	)
	);

	register_post_type( 'employees',
		array('labels' => array(
			'name' => __('Employees', 'vektor'),
			'singular_name' => __('Employee', 'vektor'),
			'all_items' => __('All employees', 'vektor'),
			'add_new' => __('Add new', 'vektor'),
			'add_new_item' => __('Add new employee', 'vektor'),
			'edit' => __( 'Edit', 'vektor' ),
			'edit_item' => __('Edit employees', 'vektor'),
			'new_item' => __('New employees', 'vektor'),
			'view_item' => __('View employees', 'vektor'),
			'search_items' => __('Search employees', 'vektor'),
			'not_found' =>  __('Nothing found in the Database.', 'vektor'),
			'not_found_in_trash' => __('Nothing found in Trash', 'vektor'),
			'parent_item_colon' => ''
			),
			'public' => true,
			'publicly_queryable' => false,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 4, 
			'menu_icon' => 'dashicons-businessman', 
			'rewrite'	=> array( 'slug' => __('employees', 'vektor'), 'with_front' => false ), 
			'has_archive' => 'employees', 
			'capability_type' => 'post',
			'hierarchical' => true,
			
			'supports' => array( 'title', 'editor', 'thumbnail' )
	 	)
	);

	
	/* this adds your post categories to your custom post type */
	// register_taxonomy_for_object_type('category', 'custom_type');
	
	/* this adds your post tags to your custom post type */
	// register_taxonomy_for_object_type('post_tag', 'custom_type');
	
} 
add_action( 'init', 'vektor_post_types');
	
// Template for Custom Taxonomies
register_taxonomy( 'sites', 
	array('properties'),
	array('hierarchical' => true,    
		'labels' => array(
			'name' => __( 'Sites', 'vektor' ),
			'singular_name' => __( 'Site', 'vektor' ),
			'search_items' =>  __( 'Search Sites', 'vektor' ),
			'all_items' => __( 'All Sites', 'vektor' ),
			'parent_item' => __( 'Parent Site', 'vektor' ),
			'parent_item_colon' => __( 'Parent Site:', 'vektor' ),
			'edit_item' => __( 'Edit Site', 'vektor' ),
			'update_item' => __( 'Update Site', 'vektor' ),
			'add_new_item' => __( 'Add New Site', 'vektor' ),
			'new_item_name' => __( 'New Site Name', 'vektor' )
		),
		'show_admin_column' => true, 
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'site' ),
	)
);

register_taxonomy( 'resources_tax', 
	array('resources'),
	array('hierarchical' => true,    
		'labels' => array(
			'name' => __( 'Categories', 'vektor' ),
			'singular_name' => __( 'Category', 'vektor' ),
			'search_items' =>  __( 'Search Categories', 'vektor' ),
			'all_items' => __( 'All Categories', 'vektor' ),
			'parent_item' => __( 'Parent Category', 'vektor' ),
			'parent_item_colon' => __( 'Parent Category:', 'vektor' ),
			'edit_item' => __( 'Edit Category', 'vektor' ),
			'update_item' => __( 'Update Category', 'vektor' ),
			'add_new_item' => __( 'Add New Category', 'vektor' ),
			'new_item_name' => __( 'New Category Name', 'vektor' )
		),
		'show_admin_column' => true, 
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'resources_tax' ),
	)
);

register_taxonomy( 'references_tax', 
	array('references'),
	array('hierarchical' => true,    
		'labels' => array(
			'name' => __( 'Categories', 'vektor' ),
			'singular_name' => __( 'Category', 'vektor' ),
			'search_items' =>  __( 'Search Categories', 'vektor' ),
			'all_items' => __( 'All Categories', 'vektor' ),
			'parent_item' => __( 'Parent Category', 'vektor' ),
			'parent_item_colon' => __( 'Parent Category:', 'vektor' ),
			'edit_item' => __( 'Edit Category', 'vektor' ),
			'update_item' => __( 'Update Category', 'vektor' ),
			'add_new_item' => __( 'Add New Category', 'vektor' ),
			'new_item_name' => __( 'New Category Name', 'vektor' )
		),
		'show_admin_column' => true, 
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'references_tax' ),
	)
);