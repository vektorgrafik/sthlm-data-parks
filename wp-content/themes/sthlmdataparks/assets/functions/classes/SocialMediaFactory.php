<?php
use \Exception as Exception;

class SocialMediaFactory {

	public $Twitter;
	public $LinkedIn;

	public function __construct( $args = [] ) {

		$this->registerModules( $args );

	}

	private function registerModules( $args ) {
		
		// Interface for modules
		require_once('Module_Interface.php');

		// Loop through arguments (modules)
		foreach( $args as $module => $is_active ) {

			if( $is_active ) {

				// Require and instatiate locally scoped classes
				require_once('Module_' . $module . '.php');
				$moduleName = 'Module_' . $module;
				$this->$module = new $moduleName();

			}

		}

	}

	/**
	 * Validates that a key is set and is not falsy
	 * in an associative array
	 * 
	 * @param  string $argument
	 * @param  array $args
	 * @return mixed
	 */
	protected function validateArgument( $argument, $args ) {

		if( array_key_exists( $argument, $args ) && $args[$argument] ) {
			return $args[$argument];
		}

		throw new Exception(
			'Missing argument ' . $argument . 
			' while calling method: ' . 
			debug_backtrace()[1]['function'] . 
			' on class: ' . 
			get_called_class()
		);

	}

	protected function loadByTransient( $type, $identifier, $index = null ) {

		$index = ( $index ) ? '_idx_' . $index : null;

		$transient = 'vektor_' . $type . '_' . $identifier . $index;

		$data = get_transient( $transient );

		//error_log( print_r( 'loaded ' . $type . ' by transient', true ));

		return ( $data === false ) ? false : $data;

	}

	protected function storeTransient( $type, $identifier, $index = null ) {

		$index = ( $index ) ? '_idx_' . $index : null;

		set_transient(
			'vektor_' . $type . '_' . $identifier . $index,
			$this->data,
			15 * MINUTE_IN_SECONDS 
		);

	}
}