<?php

class Module_Twitter extends SocialMediaFactory implements SocialMediaModule {

	private $url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
	
	protected $requestMethod = 'GET';
	protected $consumerKey;
	protected $consumerSecret;
	protected $oauthAccessToken;
	protected $oauthAccessTokenSecret;

	protected $accountName;
	protected $hashTag;

	public $data;

	public function __construct() {

		// Silence is golden? Really?

	}

	public function setApiKeys( $keys ) {
		
		$this->consumerKey = $this->validateArgument('consumerKey', $keys );
		$this->consumerSecret = $this->validateArgument('consumerSecret', $keys );
		$this->oauthAccessToken = $this->validateArgument('oauthAccessToken', $keys );
		$this->oauthAccessTokenSecret = $this->validateArgument('oauthAccessTokenSecret', $keys );

	}

	public function byAccount( $accountName ) {

		$this->accountName = $accountName;

		return $this;

	}

	public function load( $offset = null ) {

		if( ! class_exists( 'TwitterAPIExchange' ))
			require get_template_directory() . '/vendor/twitter-api/TwitterAPIExchange.php';

		$identifier = $this->accountName ?? $this->hashTag;

		$this->data = $this->loadByTransient( 'Twitter', $identifier, $offset );

		if( $this->data ) {
			
			return $this;
		}

		if( $this->accountName )
			$getfield = '?screen_name=' . urlencode( $this->accountName ) . '';

		if( $offset ) {
			error_log( print_r( 'HAS OFFSET: ' . $offset, true ));
			$getfield = $getfield . '&max_id=' . $offset;
		}

		$twitter = new TwitterAPIExchange([
			'consumer_key'              => $this->consumerKey,
			'consumer_secret'           => $this->consumerSecret,
			'oauth_access_token'        => $this->oauthAccessToken,
			'oauth_access_token_secret' => $this->oauthAccessTokenSecret
		]);

		$this->data = $twitter->setGetfield( $getfield )
				      ->buildOauth($this->url, $this->requestMethod)
				      ->performRequest();

		$this->storeTransient('Twitter', $identifier, $offset );

		return $this;
	}

	public function get( $numItems = 9999 ) {

		$data = json_decode( $this->data );
		return array_slice($data, 0, $numItems );

	}
	
}