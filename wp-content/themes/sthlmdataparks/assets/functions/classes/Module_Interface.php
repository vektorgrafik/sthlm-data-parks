<?php
interface SocialMediaModule {

	public function setApiKeys( $keys );
	public function load( $offset );
	public function get( $numItems );

}