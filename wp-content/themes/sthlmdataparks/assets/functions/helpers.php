<?php
/*
 * ------------------------------------------------------------
 * Helper functions
 * ------------------------------------------------------------
*/

function dd($_) {
	
	dump( $_ , true);
	
}

function dump($_, $die = false)
{
	$db_trace = debug_backtrace();

	$trace_idx = ( $die ) ? 1 : 0;

	$file = str_replace(ABSPATH, "", $db_trace[$trace_idx]["file"]);
	$line = $db_trace[$trace_idx]["line"];

	if( $die ) {
		$garbage = ob_get_clean();
	}

	echo '<div style="background: #F5F2F0; border: 1px solid #ddd; padding: 20px; color: #333; margin: 20px 0">';
	echo 'Dumping at row <span style="background: #e74c3c; color: white; padding:3px; border-radius: 3px; text-shadow: 1px 1px 0 rgba(0, 0, 0, .25);">' . $line . '</span> from <span style="background: #e74c3c; color: white; padding: 3px; border-radius: 3px;text-shadow: 1px 1px 0 rgba(0, 0, 0, .25);">' . $file . '</span><br><br>';
	echo '<pre>';
		
		ob_start();
		
		switch( $_ ) {
			
			case ( $_ === null ) :
			case is_scalar( $_ ) :
				var_dump( $_ );
				break;
			
			default : 
				print_r( $_ );
				break;
		}

		$dump = ob_get_clean();
		prettify( $dump );
	
	echo '</pre>';
	echo '</div>';

	if( $die )
		die();
}

function prettify($_)
{
	$_ = preg_replace("/(\[.*\])/", '<span style="color:#669900; font-size: 16px; text-shadow: 0 1px 0 white;">$1</span>', $_);
	$_ = preg_replace("/(\=>.*)/", '<span style="color:#888; font-size: 16px; text-shadow: 0 1px 0 white;">$1</span>', $_);
	$_ = preg_replace("/(\=>)/", '<span style="color:#333; font-size: 16px; text-shadow: 0 1px 0 white;">$1</span>', $_);
	$_ = preg_replace("/(stdClass Object)/", '<strong style="color: #333">$1</strong>', $_);
	$_ = preg_replace("/(Array)/", '<strong style="color: #E74C3C; font-size: 16px;text-shadow: 0 1px 0 white; ">$1</strong>', $_);
	echo $_;
}
/*
* -------------------------------------------------------------
*/

function is_localhost() {

	if( $_SERVER['HTTP_HOST'] == 'localhost') {
		return true;
	}
	
	if( $_SERVER["REMOTE_ADDR"] == '::1' ) {
		return true;
	}

	if( substr( $_SERVER["REMOTE_ADDR"], 0, 7 ) == '192.168' ) {
		return true;
	}

	if( $_SERVER["REMOTE_ADDR"] == '127.0.0.1' ) {
		return true;
	}
	
	return false;

}

function is_dev() {
	
	if( strpos($_SERVER['HTTP_HOST'], 'vgdev') > 0 ) {
		return true;
	}

	return is_localhost();

}

/*
|--------------------------------------------------------------------------
| Google Tag Manager Code
|--------------------------------------------------------------------------
*/
/*
function vg_google_tag_manager() {

	if( ! is_dev() && defined( 'VG_GOOGLE_TAG_MANAGER_ID' )) {
		echo "<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','" . VG_GOOGLE_TAG_MANAGER_ID . "');</script>";
	}

}
*/

/*
|--------------------------------------------------------------------------
| Google Tag Manager NoScript Code
|--------------------------------------------------------------------------
*/
/*
function vg_google_tag_manager_ns() {

	if( ! is_dev() && defined( 'VG_GOOGLE_TAG_MANAGER_ID' )) {
		echo '<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=' . VG_GOOGLE_TAG_MANAGER_ID . '" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>';
	}

}
*/


/*
|--------------------------------------------------------------------------
| Google Analytics Code
|--------------------------------------------------------------------------
*/
/*
function vg_google_analytics() {

	if( ! is_dev() && defined( 'VG_GOOGLE_ANALYTICS_ID' )) {
		echo "<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', '" . VG_GOOGLE_ANALYTICS_ID . "', 'auto');
  ga('send', 'pageview');

</script>";
	}

}
*/

function is_vektor_logged_in() {
	if(is_user_logged_in()) {
		
		$user_obj = wp_get_current_user();
		
		if($user_obj->user_login == 'vektorgrafik') {
			return true;
		}
		
		if(preg_match('/@vektorgrafik\.se/i', $user_obj->user_email)) {
			return true;
		}
	}
	
	return false;
}



/*
|--------------------------------------------------------------------------
| is_blog
|--------------------------------------------------------------------------
*/
function is_blog () {
    return ( is_archive() || is_author() || is_category() || is_home() || is_single() || is_tag()) && 'post' == get_post_type();
}