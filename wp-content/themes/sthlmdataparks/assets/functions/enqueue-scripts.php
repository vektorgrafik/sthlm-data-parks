<?php

define('VG_VERSION', '1.6.4');

function site_scripts() {
  global $wp_styles; // Call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way

    $suffix = ( defined('ENVIRONMENT') && ENVIRONMENT == 'production' ) ? '.min' : '';

    // jQuery in header
    wp_deregister_script( 'jquery' );
    wp_enqueue_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js', array(), '', false );

    // Adding Vendor scripts file in the footer
    wp_enqueue_script( 'vendor-js', get_template_directory_uri() . '/assets/js/vendor' . $suffix . '.js', array(), VG_VERSION, true );

    // Adding scripts file in the footer
    wp_enqueue_script( 'app-js', get_template_directory_uri() . '/assets/js/app' . $suffix . '.js', array(), VG_VERSION, true );

    // Localize AJAX url
    wp_localize_script( 'app-js', 'vektor', array(
        'ajaxurl' => admin_url( 'admin-ajax.php' ),
    ));

    // Register main stylesheet
    wp_enqueue_style( 'site-css', get_template_directory_uri() . '/assets/css/style' . $suffix . '.css', array(), VG_VERSION, 'all' );

    // Comment reply script for threaded comments
    if ( is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
      wp_enqueue_script( 'comment-reply' );
    }
}
add_action('wp_enqueue_scripts', 'site_scripts', 999);
