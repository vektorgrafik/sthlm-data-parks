<?php

// Fire all our initial functions at the start
add_action('after_setup_theme', 'vektor_start', 16);

function vektor_start() {

    // Launching operation cleanup
    add_action( 'init', 'vektor_head_cleanup');
    
    // Remove pesky injected css for recent comments widget
    add_filter( 'wp_head', 'vektor_remove_wp_widget_recent_comments_style', 1);
    
    // Clean up comment styles in the head
    add_action( 'wp_head', 'vektor_remove_recent_comments_style', 1);
    
    // Clean up gallery output in wp
    add_filter( 'gallery_style', 'vektor_gallery_style');

    // Launching this stuff after theme setup
    vektor_theme_support();

    // Hide admin bar
    show_admin_bar(false);

    // Adding sidebars to Wordpress
    add_action( 'widgets_init', 'vektor_register_sidebars' );
    
    // Cleaning up excerpt
    add_filter( 'excerpt_more', 'vektor_excerpt_more');

}

//The default wordpress head is a mess. Let's clean it up by removing all the junk we don't need.
function vektor_head_cleanup() {
	
	// Remove category feeds
	// remove_action( 'wp_head', 'feed_links_extra', 3 );
	// Remove post and comment feeds
	// remove_action( 'wp_head', 'feed_links', 2 );
	
	// Remove EditURI link
	remove_action( 'wp_head', 'rsd_link' );
	
	// Remove Windows live writer
	remove_action( 'wp_head', 'wlwmanifest_link' );
	
	// Remove index link
	remove_action( 'wp_head', 'index_rel_link' );
	
	// Remove previous link
	remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
	
	// Remove start link
	remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
	
	// Remove links for adjacent posts
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
	
	// Remove WP version
	remove_action( 'wp_head', 'wp_generator' );

	// Gets rid of the retarded emoji scripts
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'wp_print_styles', 'print_emoji_styles' ); 

} /* end Joints head cleanup */

// Remove injected CSS for recent comments widget
function vektor_remove_wp_widget_recent_comments_style() {
   if ( has_filter('wp_head', 'wp_widget_recent_comments_style') ) {
      remove_filter('wp_head', 'wp_widget_recent_comments_style' );
   }
}

// Remove injected CSS from recent comments widget
function vektor_remove_recent_comments_style() {
  global $wp_widget_factory;
  if (isset($wp_widget_factory->widgets['WP_Widget_Recent_Comments'])) {
    remove_action('wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'));
  }
}

// Remove injected CSS from gallery
function vektor_gallery_style($css) {
  return preg_replace("!<style type='text/css'>(.*?)</style>!s", '', $css);
}

// This removes the annoying […] to a Read More link
function vektor_excerpt_more($more) {
	global $post;
	
	// edit here if you like
	if ( isset( $post ) && $post instanceof WP_Post ) {
    	return '<a class="excerpt-read-more" href="' . get_permalink( $post->ID ) . '" title="' . __('Read', 'vektor') . get_the_title( $post->ID ) . '">' . __('... Read more &raquo;', 'vektor') . '</a>';
	} else {
    	return ''; // Or handle the case when $post is not available.
	}
}

//  Stop WordPress from using the sticky class (which conflicts with Foundation), and style WordPress sticky posts using the .wp-sticky class instead
function remove_sticky_class($classes) {
	$classes = array_diff($classes, array("sticky"));
	$classes[] = 'wp-sticky';
	return $classes;
}
add_filter('post_class','remove_sticky_class');

//This is a modified the_author_posts_link() which just returns the link. This is necessary to allow usage of the usual l10n process with printf()
function vektor_get_the_author_posts_link() {
	global $authordata;
	if ( !is_object( $authordata ) )
		return false;
	$link = sprintf(
		'<a href="%1$s" title="%2$s" rel="author">%3$s</a>',
		get_author_posts_url( $authordata->ID, $authordata->user_nicename ),
		esc_attr( sprintf( __( 'Posts by %s', 'vektor' ), get_the_author() ) ), // No further l10n needed, core will take care of this one
		get_the_author()
	);
	return $link;
}

/* Removes certain menu items for anyone but ourselves */
function vektor_remove_menu_pages(){
	global $current_user;
	
	//remove_menu_page('edit.php');
	remove_menu_page('link-manager.php');
	remove_menu_page('edit-comments.php');
  
	if( ! is_vektor_logged_in() ) {
		remove_menu_page('themes.php');
	}
	
	add_submenu_page('edit.php?post_type=page', __('Menus'), __('Menus'), 'manage_options', 'nav-menus.php');
	add_submenu_page('edit.php?post_type=page', __('Widgets'), __('Widgets'), 'manage_options', 'widgets.php');
	
	wp_get_current_user();
	
	if( ! is_vektor_logged_in() ) {
		remove_menu_page('edit.php?post_type=acf');
		remove_menu_page('edit.php?post_type=acf-field-group');
	}
}
add_action( 'admin_menu', 'vektor_remove_menu_pages' );