<?php
require_once FUNCTIONS_FOLDER . 'classes/SocialMediaFactory.php';

global $socialMedia;

/*
 * ------------------------------------------------------------
 * Actions
 * ------------------------------------------------------------
*/

// This loads more tweets
add_action('wp_ajax_twitter_load_more', 'twitter_load_more');
add_action('wp_ajax_nopriv_twitter_load_more', 'twitter_load_more');

// Opens the whitepaper modal
add_action('wp_ajax_nopriv_whitepaper_modal_html', 'whitepaper_modal_html');
add_action('wp_ajax_whitepaper_modal_html', 'whitepaper_modal_html');

// Saves form from whitepaper modal
add_action('wp_ajax_nopriv_save_whitepaper_lead', 'save_whitepaper_lead');
add_action('wp_ajax_save_whitepaper_lead', 'save_whitepaper_lead');

// Loads more news via ajax
add_action('wp_ajax_load_more_news', 'load_more_news');
add_action('wp_ajax_nopriv_load_more_news', 'load_more_news');

// Loads a property via ajax
add_action('wp_ajax_load_property', 'load_property');
add_action('wp_ajax_nopriv_load_property', 'load_property');

// Saves a newsletter signup
add_action('wp_ajax_newsletter_signup', 'newsletter_signup');
add_action('wp_ajax_nopriv_newsletter_signup', 'newsletter_signup');

/*
* -------------------------------------------------------------
*/


// Instatiate SocialMediaFactory
$socialMedia = new SocialMediaFactory([
    'LinkedIn' => false,
    'Twitter' => true
]);

// Set up keys for Twitter
$socialMedia->Twitter->setApiKeys([
    'consumerKey' => TWITTER_CONSUMER_KEY,
    'consumerSecret' => TWITTER_CONSUMER_SECRET,
    'oauthAccessToken' => TWITTER_OAUTH_ACCESS_TOKEN,
    'oauthAccessTokenSecret' => TWITTER_OAUTH_ACCESS_TOKEN_SECRET
]);

/**
 * Twitter feed stuff
 */
function twitter_load_more()
{

    $idx = (isset($_POST['index'])) ? (int)$_POST['index'] : null;
    $hashtag = (isset($_POST['hashtag'])) ? $_POST['hashtag'] : null;

    if (!$idx)
        die();

    $transient = 'vektor_twitter_' . $hashtag . '_idx_' . $idx;
    $transient_previous = 'vektor_twitter_' . $hashtag . '_idx_' . ($idx - 1);

    $twitter_json = get_transient($transient);

    if ($twitter_json === false) {

        $data = json_decode(get_transient($transient_previous));

        // Parse next results string
        if (property_exists($data->search_metadata, 'next_results')) {

            $next = $data->search_metadata->next_results;
            $next = substr($next, 0, strpos($next, "&"));
            $next = substr($next, 8);

        } else {

            // Next results didn't exist, return response.
            wp_send_json_error('Nothing more to load');

        }

        // Fetch new data based on max_index from previous call
        $twitter_json = twitter_get(TWITTER_ACCOUNT_NAME, $next, $hashtag);

        // Store transient
        set_transient($transient, $twitter_json, 15 * MINUTE_IN_SECONDS);

    }

    // Is there anything more to load?
    $hide_button = true;
    $twitter_data = json_decode($twitter_json);
    if (is_object($twitter_data) && property_exists($twitter_data->search_metadata, 'next_results')) {
        $hide_button = false;
    }

    // Generate html
    $html = ($twitter_json) ? generate_twitter_html($twitter_json) : null;

    // Set up response array
    $response = array(
        'html' => $html,
        'hide_button' => $hide_button
    );

    wp_send_json_success($response);

}


function remove_twitter_transients($hashtag = "")
{

    global $wpdb;

    $sql = "delete from `wp_options` where `option_name` like %s";
    $data = '%vektor_twitter_' . $hashtag . '%';

    return $wpdb->query($wpdb->prepare($sql, $data));

}

function generate_twitter_html($json)
{

    $item_template = '<div class="twitter-item"><div class="photo-wrap"><img src="%s"><a href="%s">@%s</a></div></div>';

    $data = json_decode($json);

    $output = "";
    if (!empty($data->statuses)) :
        foreach ($data->statuses as $tweet) :

            $image = "";
            $screen_name = "";

            if (!empty($tweet->entities->media)) {
                $image = $tweet->entities->media[0]->media_url;
            }

            $screen_name = $tweet->user->screen_name;
            $href = 'https://twitter.com/' . $screen_name;

            $output .= sprintf($item_template, $image, $href, $screen_name);

        endforeach;

    else :
        return false;
    endif;

    return $output;

}

function twitter_get($account_name, $max_id = null)
{

    require get_template_directory() . '/vendor/twitter-api/TwitterAPIExchange.php';

    // Set up Twitter Auth
    $settings = array(
        'consumer_key' => TWITTER_CONSUMER_KEY,
        'consumer_secret' => TWITTER_CONSUMER_SECRET,
        'oauth_access_token' => TWITTER_OAUTH_ACCESS_TOKEN,
        'oauth_access_token_secret' => TWITTER_OAUTH_ACCESS_TOKEN_SECRET
    );

    $url = 'https://api.twitter.com/1.1/search/tweets.json';
    $getfield = '?q=from:@' . urlencode($account_name);

    if ($max_id)
        $getfield = $getfield . '&max_id=' . $max_id;

    $requestMethod = 'GET';

    $twitter = new TwitterAPIExchange($settings);

    return $twitter->setGetfield($getfield)
        ->buildOauth($url, $requestMethod)
        ->performRequest();

}

function newsletter_signup()
{
    check_ajax_referer('newsletter-nonce', 'security');

    $post = wp_parse_args($_POST, [
        'email' => null,
    ]);

    $valid = is_email($post['email']);

    if (!$valid )
        wp_send_json_error('Invalid email address');

    if($post['consent'] == false)
        wp_send_json_error('You did not give your consent for us to store your email');


    $api_key = get_field('mailchimp_api_key', 'option');
    $list_id = get_field('mailchimp_list_id', 'option');
    $email = sanitize_text_field($post['email']);
    $status = 'subscribed'; // subscribed, cleaned, pending


    $args = array(
        'method' => 'PUT',
        'headers' => array(
            'Authorization' => 'Basic ' . base64_encode('user:' . $api_key)
        ),
        'body' => json_encode(array(
            'email_address' => $email,
            'status' => $status
        ))
    );
    $response = wp_remote_post('https://' . substr($api_key, strpos($api_key, '-') + 1) . '.api.mailchimp.com/3.0/lists/' . $list_id . '/members/' . md5(strtolower($email)), $args);

    $body = json_decode($response['body']);
    if ($response['response']['code'] == 200 && $body->status == $status) {
        wp_send_json_success('true');
    } else {
        if(strpos(json_decode($response['body'])->detail, 'unsubscribe') !== false){
            wp_send_json_error(json_decode($response['body'])->detail);
        }

        wp_send_json_error('Something went wrong, please try again later');
    }

}

function whitepaper_modal_html()
{

    ob_start();

    $whitepaper = $_POST['whitepaper'];
    $whitepaper['url'] = sanitize_text_field($whitepaper['url']);
    $whitepaper['name'] = sanitize_text_field($whitepaper['name']);
    $whitepaper['type'] = sanitize_text_field($whitepaper['type']);

    if (substr($whitepaper['type'], -1) == 's') {
        $whitepaper['type'] = substr($whitepaper['type'], 0, strlen($whitepaper['type']) - 1);
    }

    require get_template_directory() . '/templates/part-whitepaper-modal.php';

    $data = ob_get_clean();

    wp_send_json_success($data);
    wp_die();

}

function save_whitepaper_lead()
{

    global $wpdb;

    check_ajax_referer('whitepaper-nonce', 'security');

    $post = wp_parse_args($_POST, [
        'name' => null,
        'email' => null,
        'company' => null,
        'position' => null
    ]);

    $valid = true;
    foreach ($post as $item) {
        if (!$item)
            $valid = false;
    }

    if (!$valid)
        wp_send_json_error('All fields are required');

    $wpdb->insert('vg_whitepaper_leads',
        [
            'name' => sanitize_text_field($post['name']),
            'email' => sanitize_text_field($post['email']),
            'company' => sanitize_text_field($post['company']),
            'position' => sanitize_text_field($post['position']),
            'whitepaper' => sanitize_text_field($post['whitepaper']),
            'downloaded' => date('Y-m-d H:i:s')
        ],
        [
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s'
        ]
    );

    wp_send_json_success($post);

}

function vektor_content_field($content_array, $section_classes)
{

    $content_title = $content_array['content_title'] ?? null;
    $content_intro = $content_array['content_intro'] ?? null;
    $content_text = $content_array['content_text'] ?? null;

    $classes = $section_classes ?? null;

    if ($classes) {
        $title_class = $classes . '-section__title';
        $intro_class = $classes . '-section__intro';
        $content_class = $classes . '-section__title';

    } else {
        $title_class = '';
        $intro_class = $classes . '-section__title';
        $content_class = $classes . '-section__content';
    }

    $content = '';

    if ($content_title) {
        $content .= '<h2 class="section__title ' . $title_class . '">' . $content_title . '</h2>';
    }

    if ($content_intro) {

        $content_intro = str_replace('<p>', '<p class="intro">', $content_intro);
        $content .= '<div class="section__intro ' . $intro_class . '">' . $content_intro . '</div>';
    }

    if ($content_text) {
        $content .= '<div class="section__content ' . $content_class . '">' . $content_text . '</div>';
    }

    return $content;

}

function get_news_posts($args = [])
{

    $args = wp_parse_args($args, [
        'offset' => 0,
        'post_type' => 'post',
        'posts_per_page' => NEWS_POSTS_PER_PAGE,

    ]);

    // Fetch posts
    $news_posts = new Wp_Query($args);

    if (defined('DOING_AJAX')) :

        // Start output buffer
        ob_start();

        // Loop posts through template part
        if ($news_posts->have_posts()) while ($news_posts->have_posts()) : $news_posts->the_post();

            get_template_part('templates/loop', 'news');

        endwhile;

        // Return JSON response
        wp_send_json_success([
            'html' => ob_get_clean(),
            'offset' => $args['offset'] + NEWS_POSTS_PER_PAGE,
            'hide_button' => ($news_posts->found_posts <= (NEWS_POSTS_PER_PAGE + $args['offset']))
        ]);

    else:


		//error_log(print_r($news_posts,1));
		
        return $news_posts;

    endif;

}

function load_more_news()
{

    // Get offset
    $offset = $_POST['offset'] ?? false;

    // Bail early if no offset exists
    if (!$offset)
        wp_send_json_error('No offset set');

    // This gets returned as JSON automagically.
    get_news_posts([
        'offset' => $offset,
        'posts_per_page' => NEWS_POSTS_PER_PAGE
    ]);

}


/**
 * Loads a property through AJAX call
 * @return JSON
 */
function load_property()
{

    $property_id = $_POST['property_id'] ?? null;

    // Bail if no property ID
    if (!$property_id)
        wp_send_json_error('Missing property ID');

    $post = get_post($property_id);
    setup_postdata($post);

    ob_start();
    include get_template_directory() . '/templates/property-single.php';

    wp_send_json_success([
        'html' => ob_get_clean(),
        'title' => htmlentities($post->post_title) . ' - ' . wp_get_document_title()
    ]);

}

function get_site_locations($args)
{

    $args = wp_parse_args($args, [
        'asJson' => false,
        'forMap' => false
    ]);

    $sites = get_terms([
        'hide_empty' => false,
        'taxonomy' => 'sites'
    ]);

    foreach ($sites as &$site) {

        $site->permalink = get_term_link($site->term_id, 'sites');
        $site->address = get_field('site_address', 'sites_' . $site->term_id);
        $site->label = get_field('site_pin_label', 'sites_' . $site->term_id);
        $site->pin_color = get_field('site_pin_color', 'sites_' . $site->term_id);

        // Only for map-view, let's strip stuff we don't need.
        if ($args['forMap'] == true) :

            unset($site->slug);
            unset($site->count);
            unset($site->filter);
            unset($site->parent);
            unset($site->term_id);
            unset($site->taxonomy);
            unset($site->term_group);
            unset($site->description);
            unset($site->term_taxonomy_id);

        endif;

    }

    return ($args['asJson'] == false) ? $sites : json_encode($sites, JSON_HEX_QUOT);

}