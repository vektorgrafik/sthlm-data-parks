<?php
// This file handles the admin area and functions - You can use this file to make changes to the dashboard.

function vg_table_views() {
	
	require_once CLASS_FOLDER . '/VgTableView/VgTableConfig.php';
	require_once CLASS_FOLDER . '/VgTableView/VgTableView.php';
	
	// Whitepaper Leads View
	$config = new VgTableConfig;
	$view 	= new VgTableView;
	
	$config->table('vg_whitepaper_leads',
		[
			[ 'name' => 'name', 			'title' => 'Name', 				'type' => 'string' ],
			[ 'name' => 'company', 			'title' => 'Company', 			'type' => 'string' ],
			[ 'name' => 'position', 		'title' => 'Position', 			'type' => 'string' ],
			[ 'name' => 'email', 			'title' => 'Email', 			'type' => 'string' ],
			[ 'name' => 'whitepaper', 		'title' => 'Whitepaper', 		'type' => 'string' ],
			[ 'name' => 'downloaded', 		'title' => 'Downloaded', 		'type' => 'string' ]
		])
		->menuOrder(20)
		->rowsPerPage(25)
		->menuTitle('Whitepaper Leads')
		->icon('media-spreadsheet')
		->order('downloaded', 'desc')
		->description('All downloads of whitepapers across the site.');

	$view->create( $config );

	// Newsletter View
	$config = new VgTableConfig;
	$view 	= new VgTableView;
	
	$config->table('vg_newsletter_leads',
		[
			[ 'name' => 'email', 		'title' => 'E-mail address', 	'type' => 'string' ],
			[ 'name' => 'signed_up',	'title' => 'Signed up', 		'type' => 'string' ]
		])
		->menuOrder(21)
		->rowsPerPage(50)
		->menuTitle('Newsletter Leads')
		->icon('media-spreadsheet')
		->order('signed_up', 'desc')
		->description('All newsletter signups from the footer of the site.');

	$view->create( $config );

}
add_action( 'admin_menu', 'vg_table_views' );
add_action( 'wp_ajax_get_vg_table_data', 'vg_table_views' );


/************* DASHBOARD WIDGETS *****************/
// Disable default dashboard widgets
function disable_default_dashboard_widgets() {
	
	// Remove_meta_box('dashboard_right_now', 'dashboard', 'core');    // Right Now Widget
	remove_meta_box('dashboard_recent_comments', 'dashboard', 'core'); // Comments Widget
	remove_meta_box('dashboard_incoming_links', 'dashboard', 'core');  // Incoming Links Widget
	remove_meta_box('dashboard_plugins', 'dashboard', 'core');         // Plugins Widget

	// Remove_meta_box('dashboard_quick_press', 'dashboard', 'core');  // Quick Press Widget
	remove_meta_box('dashboard_recent_drafts', 'dashboard', 'core');   // Recent Drafts Widget
	remove_meta_box('dashboard_primary', 'dashboard', 'core');         //
	remove_meta_box('dashboard_secondary', 'dashboard', 'core');       //

	// Removing plugin dashboard boxes
	remove_meta_box('yoast_db_widget', 'dashboard', 'normal');         // Yoast's SEO Plugin Widget

}

// RSS Dashboard Widget
function vektor_rss_dashboard_widget() {
	if(function_exists('fetch_feed')) {
		include_once(ABSPATH . WPINC . '/feed.php');               	// include the required file
		$feed = fetch_feed('http://vektor.com/feed/rss/');			// specify the source feed
		$limit = $feed->get_item_quantity(5);                      	// specify number of items
		$items = $feed->get_items(0, $limit);                     	// create an array of items
	}
	if ($limit == 0) echo '<div>' . __( 'The RSS Feed is either empty or unavailable.', 'vektor' ) . '</div>';   // fallback message
	else foreach ($items as $item) { ?>

	<h4 style="margin-bottom: 0;">
		<a href="<?php echo $item->get_permalink(); ?>" title="<?php echo mysql2date(__('j F Y @ g:i a', 'vektor'), $item->get_date('Y-m-d H:i:s')); ?>" target="_blank">
			<?php echo $item->get_title(); ?>
		</a>
	</h4>
	<p style="margin-top: 0.5em;">
		<?php echo substr($item->get_description(), 0, 200); ?>
	</p>
	<?php }
}


// Calling all custom dashboard widgets
function vektor_custom_dashboard_widgets() {

	// wp_add_dashboard_widget('vektor_rss_dashboard_widget', __('Custom RSS Feed (Customize in admin.php)', 'vektor'), 'vektor_rss_dashboard_widget');

}
// removing the dashboard widgets
add_action('admin_menu', 'disable_default_dashboard_widgets');

// adding any custom widgets
add_action('wp_dashboard_setup', 'vektor_custom_dashboard_widgets');

/************* CUSTOMIZE ADMIN *******************/
// Custom Backend Footer
function vektor_custom_admin_footer() {
	_e('<span id="footer-thankyou"><a href="http://www.vektorgrafik.se" target="_blank">Vektorgrafik</a></span>.', 'vektor');
}

// adding it to the admin area
add_filter('admin_footer_text', 'vektor_custom_admin_footer');

// Sanitize uploaded file names
function vektor_sanitize_file_name( $filename ) {
	
	$filename = str_replace( array( 'å', 'ä', 'Å', 'Ä' ), 'a', $filename );
	$filename = str_replace( array( 'ö', 'Ö' ), 'o', $filename );
	
	$filename = preg_replace( '/[^a-zA-Z0-9\_\-\.]+/i', '', $filename );
	
	$filename = mb_strtolower( $filename );
	
	return $filename;
}
add_filter('sanitize_file_name', 'vektor_sanitize_file_name', 10, 1);

/* No more folders in uploads directory */
function vektor_theme_activation() {
	update_option('uploads_use_yearmonth_folders', 0);
}
add_action( 'after_switch_theme', 'vektor_theme_activation');
add_filter( 'option_uploads_use_yearmonth_folders', '__return_false', 100 );

// Add ACF option pages
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page('Other Settings');
	
}

// Callback function to insert 'styleselect' into the $buttons array
function my_mce_buttons_2( $buttons ) {
	array_unshift( $buttons, 'styleselect' );
	return $buttons;
}
// Register our callback to the appropriate filter
add_filter( 'mce_buttons_2', 'my_mce_buttons_2' );


/*
 * Adding custom formats to TinyMCE editor.
 */
function vektor_tiny_mce_before_init( $settings ){
	
	$style_formats = array(
		array(	'title' 	=> 'Intro', 
				'inline' 	=> 'span', 
				'classes' 	=> 'preamble color-primary strong'
		)
        
	);

	$settings['style_formats'] = json_encode( $style_formats );
	return $settings;
}

add_filter('tiny_mce_before_init', 'vektor_tiny_mce_before_init');


/*
 * Modify TinyMCE editor to remove H1.
 */
function tiny_mce_remove_unused_formats($init) {

	$init['block_formats'] = 'Paragraph=p;Heading 2=h2;Heading 3=h3;Heading 4=h4;Pre=pre';

	return $init;

}

add_filter('tiny_mce_before_init', 'tiny_mce_remove_unused_formats' );