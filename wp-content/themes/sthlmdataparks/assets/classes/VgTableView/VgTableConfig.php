<?php
if ( ! defined( 'ABSPATH' ) ) die();

class VgTableConfig {

	public $order;
	public $where;
	public $columns;
	public $operator;
	public $tableName;
	public $menuTitle;
	public $description;
	public $menuOrder = 30; 
	public $rowsPerPage = 25;
	// Declare the property explicitly
	public $icon;

	/**
	 * Setter for Table Name
	 * @param String $tableName
	 * @param Array  $columns
	 * @return Class
	 */
	public function table( String $tableName, Array $columns )
	{
	    
		$this->tableName = $tableName;
		$this->columns = $columns;
	    return $this;
	
	}

	public function where( Array $args = [], String $operator = "and" ) {

		$this->operator = $operator;
		$this->where = $args;
		return $this;

	}

	public function order( String $columnName, String $direction ) {

		$this->order = $columnName . ' ' . $direction;
		return $this;
	}

	public function icon( String $icon ) {

		$this->icon = $icon;
		return $this;

	}

	public function description( String $description ) {

		$this->description = $description;
		return $this;

	}

	public function rowsPerPage( Int $rowsPerPage ) {

		$this->rowsPerPage = $rowsPerPage;
		return $this;

	}

	public function menuTitle( String $menuTitle ) {

		$this->menuTitle = $menuTitle;
		return $this;

	}

	public function menuOrder( Int $menuOrder ) {

		$this->menuOrder = $menuOrder;
		return $this;

	}

}