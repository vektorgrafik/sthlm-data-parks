var app = new Vue({

	el: '#app',

	data: {
		entries: '',
		headers: '',
		numPages: 0,
		currentPage: 1,
		rowsPerPage: 25,
		activeEntries: '',
		activeEntriesEnd: 0,
		filteredEntries: '',
		activeEntriesStart: 0,
		searchString: '',
		filterableKeys: [],
		reverse: false,
		sortKey: ''
	},

	watch: {

		searchString: function( newFilter ) {

			this.filterBySearch( newFilter );

		}

	},

	computed: {

		maxEntries: function() {
			return ( this.activeEntriesEnd > this.entries.length ) ? this.entries.length : this.activeEntriesEnd;
		},

		dynNumPages: function() {
			return ( this.searchString.length > 0 ) ? Math.ceil( this.filteredEntries.length / this.rowsPerPage ) : this.numPages;
		}

	},

	methods: {

		setSortKey: function( key ) {

			if( key == this.sortKey )
				this.reverse = !this.reverse;

			this.sortKey = key;

			this.updatePageData();

		},

		filterBySearch: function( text ) {
			
			var self = this;

			this.filteredEntries = this.entries.filter( function( item ) {

				return self.regexMatch( item );

			});
			
			this.updatePageData();

		},

		regexMatch: function( item ) {
			
			var regex = new RegExp( this.searchString, 'i' );

			return this.filterableKeys.some(function(key) {

				return regex.test( item[key] );

			});
		},

		setPage: function( page ) {
			this.currentPage = page;
			this.updatePageData();
		},

		updatePageData: function() {

      var self = this;

			if( self.sortKey ) {
        this.filteredEntries = _.sortBy( this.filteredEntries, function( o ) {
          if( self.sortKey.type == 'int' ) {
            return parseInt(o[self.sortKey.name]);
          } else {
            return o[self.sortKey.name];
          }
        });
      }

      if( this.reverse )
        this.filteredEntries.reverse();

			this.activeEntriesStart = parseInt( this.currentPage-1 ) * parseInt( this.rowsPerPage );
			this.activeEntriesEnd = ( this.activeEntriesStart + parseInt( this.rowsPerPage ));
			
			this.activeEntries = this.filteredEntries.slice(
				this.activeEntriesStart,
				this.activeEntriesEnd
			);

		},

		setFilterableKeys: function() {

			for( key in this.entries[0] )
				this.filterableKeys.push( key );

		}

	},

	mounted: function() {

		jQuery.ajax({
			type: 'POST',
			dataType: 'JSON',
			url: ajaxurl,
			data: {"action" : "get_vg_table_data", "identifier" : vg_table_view.identifier },
			success: function( response ) {
				app.entries = response.data.entries;
				app.headers = response.data.headers;
				app.numPages = response.data.numPages;
				app.rowsPerPage = response.data.rowsPerPage;
				app.filteredEntries = app.entries;
				app.setPage(1);
				app.setFilterableKeys();
			}
		});

	}

});