<?php
if ( ! defined( 'ABSPATH' ) ) die();

class VgTableView {

	private $config;
	private $data;

	public function __construct() {

		add_action( 'admin_enqueue_scripts', array( $this, 'attachScripts' ));

	}

	public function isCorrectScreen() {

		$screen = get_current_screen();

		return ( str_replace( 'toplevel_page_', '', $screen->id ) == sanitize_title( $this->config->menuTitle ?? '' ));

	}

	public function attachScripts() {

		if( $this->isCorrectScreen() ) {
			
			wp_enqueue_script( 'lodash', get_template_directory_uri() . '/assets/classes/VgTableView/lodash.min.js', '', '', true );
			wp_enqueue_script( 'vue', get_template_directory_uri() . '/assets/classes/VgTableView/vue.js', '', '', true );
			wp_enqueue_script( 'vg-table-view', get_template_directory_uri() . '/assets/classes/VgTableView/vg-table-view.js', '', '', true );
		    
		    wp_localize_script( 'vg-table-view', 'vg_table_view',[
		        'identifier' => sanitize_title( $this->config->menuTitle ?? '' )
		    ]);

		}
	}

	public function addAdminPage() {

		add_menu_page(
			$this->config->menuTitle,
			$this->config->menuTitle,
			'manage_options',
			sanitize_title( $this->config->menuTitle ?? '' ),
			array( $this, 'renderTable' ),
			'dashicons-' . $this->config->icon,
			$this->config->menuOrder
		);

	}

	public function create( VgTableConfig $config ) {

		$this->config = $config;
		$this->addAdminPage();
		$this->getEntries();

	}

	private function getSqlColumns() {

		$columns = [];

		foreach( $this->config->columns as $columnData ) {

			array_push( $columns, '`' . $columnData['name'] . '`' );

		}

		return implode(',', $columns);

	}

	private function getWhereClause() {

		$whereClause = [];

		//if( count( $this->config->where ) > 0 ) foreach( $this->config->where as $column => $value ) {
		if( isset($this->config->where) && is_array($this->config->where) && count( $this->config->where ) > 0 ) foreach( $this->config->where as $column => $value ) {

			array_push( $whereClause, $column . '=' . $value );

		}

		if( count( $whereClause ) > 0 ) {
			return " where " . implode(' ' . $this->config->operator . ' ', $whereClause );
		}

		return "";

	}

	public function isDataGettable() {
		return (
			defined( 'DOING_AJAX' ) &&
			$_POST['action'] == 'get_vg_table_data' &&
			$_POST['identifier'] == sanitize_title( $this->config->menuTitle ?? '' )
		);
	}

	/**
	 * 
	 * Fetches all entries from table
	 * 
	 * @return Array
	 */
	public function getEntries() {

		if ( ! $this->isDataGettable() ) {
			return;
		}

		global $wpdb;

		$data = array();

		$limit = $this->getLimit();

		$data['num_rows'] 	= $wpdb->get_var(
								'select count(*) from ' . 
								$this->config->tableName .
								$this->getWhereClause()
							);
		
		$data['data'] 		= $wpdb->get_results('
								select ' .
								$this->getSqlColumns() . ' 
								from ' . $this->config->tableName .  
								$this->getWhereClause() . '
								order by ' . $this->config->order,
								OBJECT
							);

		foreach( $data['data'] as $row ) {
			foreach( $this->config->columns as $column ) {
				$row = apply_filters( 'VgTableColumn_' . $column['name'], $row );
			}
		}

	    $this->data = $data;

		
		wp_send_json_success([
			'entries' => $this->data['data'],
			'headers' => $this->config->columns,
			'numRows' => $this->data['num_rows'],
			'rowsPerPage' => $this->config->rowsPerPage,
			'numPages' => $this->getNumPages( $this->data['num_rows'] )
		]);
	
	}

	private function getLimit()
	{
	    
	    $offset = ( $this->getCurrentPage() == 1 ) ? 0 : (( $this->getCurrentPage()-1 ) * $this->config->rowsPerPage );
	    
	    return $offset . ',' . $this->config->rowsPerPage;
	
	}

	public function getCurrentPage()
	{
	    
	    return ( isset( $_GET['curPage'] )) ? (int) $_GET['curPage'] : 1;
	
	}

	public function getCurrentScope( $totalRows )
	{
	    
	    $curPage = $this->getCurrentPage();
	    $perPage = $this->config->rowsPerPage;

	    $firstItem = ( $curPage == 1 ) ? 1 : ( ($curPage * $perPage) - ($perPage-1) );
	    $lastItem  = (( $firstItem + $perPage ) < $perPage ) ? $perPage : ($firstItem + ($perPage-1) );

	    if( $lastItem > $totalRows )
	    	$lastItem = $totalRows;

	    return $firstItem . ' till ' . $lastItem;
	
	}

	public function getNumPages( $totalRows ) {

		return ceil( $totalRows / $this->config->rowsPerPage );

	}

	public function renderTable() {

	?>

	<style>

		.multi-actions {
			padding: 15px;
			background: white;
		}

		.pagination {
			padding: 20px;
		}
		.pagination a {
			display: inline-block;
			width: 30px;
			height: 30px;
			padding: 0;
			text-align: center;
			line-height: 30px;
			color: #333;
			margin: 0 .2em;
			text-decoration: none;
		}
		.pagination a:hover,
		.pagination a.active {

			background: #3ccb93;
			color: white;

		}

	</style>

	<div class="wrap">

		<div id="app">

			<h2><?=$this->config->menuTitle?></h2>

			<p class="small">
				<?=$this->config->description?><br>
			</p>

			<h4>Visar {{ activeEntriesStart+1 }} till {{ maxEntries }} av {{ entries.length }} rader</h4>

			<input type="text" v-model="searchString">Filter</button>

			<div class="pagination">
				<a href="#" 
					v-for="n in dynNumPages"
					v-on:click.prevent="setPage(n)"
					v-bind:class="{ active: n == currentPage }">
					{{ n }}
				</a>
			</div>

			<table class="wp-list-table widefat fixed posts striped" id="submissions-table">
				
				<thead>
					<tr>
						<th class="sortable"
							v-for="header in headers"
							v-on:click.prevent="setSortKey(header)"
							v-bind:class="{ 'asc': !reverse, 'desc': reverse }">
							<a href="#">
								<span>{{ header.title }}</span>
								<span class="sorting-indicator"></span>
							</a>
						</th>
					</tr>
				</thead>

				<tbody>
					
					<tr v-for="entry in activeEntries">
						<td v-for="column in entry">{{ column }}</td>
					</tr>

				</tbody>

				<tfoot>
					<tr>
						<th class="sortable"
							v-for="header in headers"
							v-on:click.prevent="setSortKey(header)"
							v-bind:class="{ 'asc': !reverse, 'desc': reverse }">
							<a href="#">
								<span>{{ header.title }}</span>
								<span class="sorting-indicator"></span>
							</a>
						</th>
					</tr>
				</tfoot>

			</table>

			<div class="pagination">
				<a href="#" 
					v-for="n in dynNumPages"
					v-on:click.prevent="setPage(n)"
					v-bind:class="{ active: n == currentPage }">
					{{ n }}
				</a>
			</div>

		</div>

	</div>

	<?php

	}
}
?>