'use strict';

/*
 * App
 */

var App = function App(jQuery) {

	var self = this,
	    $ = jQuery,
	    $win = $(window),
	    $body = $('body'),
	    $header = $('.page-header'),
	    currentScroll,
	    lastScroll,
	    headerHeight = $header.height(),
	    hashURL = location.hash;
	self.resizeActions = function () {}, self.bindWindowEvents = function () {

		// Unthrottled events
		$(window).on('scroll', self.unthrottledScrollActions);

		// Throttled events
		$(window).on('scroll', $.throttle(50, self.throttledScrollActions));
		$(window).on('resize', $.debounce(250, self.resizeActions));
	}, self.unthrottledScrollActions = function () {}, self.throttledScrollActions = function () {

		currentScroll = $win.scrollTop();

		if (currentScroll > headerHeight) {
			$body.addClass('state--scrolled');
		} else {
			$body.removeClass('state--scrolled');
		}

		if (currentScroll > lastScroll) {

			$body.addClass('state--scrolled-down');
			$body.removeClass('state--scrolled-up');
		} else {

			if (currentScroll < lastScroll - 10) {
				$body.removeClass('state--scrolled-down');
				$body.addClass('state--scrolled-up');
			}
		}

		lastScroll = currentScroll;

		if (currentScroll >= $(window).height() - 200) {
			$body.addClass('state--scrolled-past-height');
		} else {
			$body.removeClass('state--scrolled-past-height');
		}

		self.scrollMenuSticky(currentScroll);
	}, self.resizeActions = function () {

		self.scrollMenuDisplay();
	};
	/*
  * ------------------------------------------------------------
  * Newsfeed
  * ------------------------------------------------------------
 */

	self.newsFeed = {

		init: function init() {

			this.bindClickEvents();
		},

		bindClickEvents: function bindClickEvents() {

			$('#load-more-news').off('click');
			$('#load-more-news').on('click', function (e) {

				e.preventDefault();

				var $btn = $(this),
				    offset = $btn.attr('data-offset');

				$btn.addClass('--is-loading');

				$.ajax({
					type: 'POST',
					dataType: 'JSON',
					url: vektor.ajaxurl,
					data: { "action": "load_more_news", "offset": offset },
					success: function success(response) {

						$btn.removeClass('--is-loading');

						// Attach HTML to DOM
						var $item = $(response.data.html);

						$item.addClass('animate-in').appendTo($('.news-feed'));

						// Update offset
						$btn.attr('data-offset', response.data.offset);

						// Hide button if no more posts to show
						if (response.data.hide_button) $btn.hide();

						// Rebind buttons
						self.newsFeed.bindClickEvents();
					}
				});
			});

			$('.news-article__toggle').off('click');
			$('.news-article__toggle').on('click', function (e) {

				var $bth = $(this),
				    $post = $('#' + $(this).attr("data-post-id")),
				    $postContent = $post.children('.news-article__content'),
				    $postHeader = $post.children('.news-article__header'),
				    postOffset = $post.offset(),
				    headerHeight = $header.height();

				$post.addClass('is-animating');

				if ($post.hasClass('is-expanded')) {

					$post.removeClass('is-expanded');

					$postHeader.find('.social-share').hide();

					$postContent.slideUp();
					$postHeader.slideUp();

					setTimeout(function () {

						$postHeader.slideDown();
						$post.removeClass('is-animating');
					}, 800);
				} else {

					setTimeout(function () {

						$post.addClass('is-expanded');

						$post.removeClass('is-animating');

						$postHeader.find('.social-share').show();

						$postContent.slideDown();
						$postHeader.slideDown();
					}, 450);
				}

				$("html, body").animate({
					scrollTop: postOffset.top - headerHeight

				}, '500', function () {});
			});
		}

	},

	/*
 * -------------------------------------------------------------
 */
	/*
  * ------------------------------------------------------------
  * Properties
  * ------------------------------------------------------------
 */

	self.properties = {

		/**
   * Local variables
   */
		isLoading: false,
		propertyTitle: '',
		propertyData: null,
		defProp: $.Deferred(),
		currentProperty: null,
		initialPropertyURI: '',

		init: function init() {

			this.setInitialProperty();
			this.setInitialPropertyURI();
			this.bindClickEvents();
			this.scrollReveal();
			this.popHistoryState();
			this.setInitialHistoryState();
			this.setPropertyTitle();
		},

		/**
   * Gets attribute on active anchor element
   * and sets it to our local variable
   *
   * @return Int
   */
		setInitialProperty: function setInitialProperty() {

			this.currentProperty = parseInt($('.property-list__link.is-active').data('property-id'));
		},

		setInitialPropertyURI: function setInitialPropertyURI() {

			this.initialPropertyURI = $('.property-list__link.is-active').attr('href');
		},

		bindClickEvents: function bindClickEvents() {

			var $navElements = $('.property-list__link.js-ajax');

			$navElements.off('click');
			$navElements.on('click', function (e) {

				e.preventDefault();

				if (self.properties.isLoading) return false;

				var propertyId = $(this).data('property-id');

				if (!self.properties.isCurrentProperty(propertyId)) {
					self.properties.setHistoryState(propertyId, $(this).attr('href'));
					self.properties.switchProperty(propertyId);
				}
			});
		},

		setPropertyTitle: function setPropertyTitle(title) {

			this.propertyTitle = title ? title : $('title').html();
			$('title').html(this.propertyTitle);
		},

		setInitialHistoryState: function setInitialHistoryState() {

			var state = {
				'propertyId': this.currentProperty,
				'title': this.propertyTitle
			};

			history.pushState(state, state.title, this.initialPropertyURI);
		},

		setHistoryState: function setHistoryState(propertyId, uri) {

			var state = {
				'propertyId': propertyId
			};

			history.pushState(state, 'test', uri);
		},

		/**
   * { popHistoryState }
   *
   * The popstate event is fired when the user
   * clicks either forward or back in their 
   * browser. So we simply switch the property
   * based on the ID stored in the history API.
   * 
   * @return Void
   */
		popHistoryState: function popHistoryState() {

			window.addEventListener('popstate', function (e) {

				self.properties.switchProperty(e.state.propertyId);
			});
		},

		setCurrentProperty: function setCurrentProperty(propertyId) {

			this.currentProperty = propertyId;
			this.updateNavigation();
		},

		updateNavigation: function updateNavigation() {

			$('.property-list__link').removeClass('is-active');
			$('.property-list__link[data-property-id="' + this.currentProperty + '"]').addClass('is-active');
		},

		isCurrentProperty: function isCurrentProperty(propertyId) {

			return propertyId == this.currentProperty;
		},

		resetDeferredElement: function resetDeferredElement() {

			this.defProp = $.Deferred();
		},

		animateOutCurrent: function animateOutCurrent() {

			$('.top-section__bg').removeClass('animate-in').addClass('animate-out');
			$('.property-item').removeClass('animate-in-alternate').addClass('animate-out-alternate');
		},

		animateInCurrent: function animateInCurrent() {

			$('.top-section__bg').removeClass('animate-out').addClass('animate-in');
			$('.property-item').removeClass('animate-out-alternate').addClass('animate-in-alternate');
		},

		showLoadingScreen: function showLoadingScreen() {

			var $elm = $('<div></div>'),
			    $s1 = $('<div></div>'),
			    $s2 = $('<div></div>'),
			    $spinner = $('<div></div>');

			$elm.addClass('loading-overlay');

			$s1.addClass('loading-overlay__section').addClass('loading-overlay__section--left');
			$s2.addClass('loading-overlay__section').addClass('loading-overlay__section--right');

			$spinner.addClass('spinner');
			$spinner.html('<span></span>');

			$s1.appendTo($elm);
			$s2.appendTo($elm);
			$spinner.appendTo($elm);

			$elm.insertAfter('body');

			setTimeout(function () {
				$elm.addClass('start-anim');
			}, 150, $elm);
		},

		hideLoadingScreen: function hideLoadingScreen() {

			var $elm = $('.loading-overlay');

			$elm.removeClass('start-anim');

			setTimeout(function () {
				$elm.remove();
			}, 750, $elm);
		},

		switchProperty: function switchProperty(propertyId) {

			if (!propertyId) return false;

			this.isLoading = true;
			this.animateOutCurrent();
			this.showLoadingScreen();
			this.resetDeferredElement();
			this.setCurrentProperty(propertyId);
			this.loadPropertyData();

			$.when(self.properties.defProp).done(function () {

				self.properties.propertyLoaded();
			});
		},

		loadPropertyData: function loadPropertyData() {

			$.ajax({
				type: 'POST',
				dataType: 'JSON',
				url: vektor.ajaxurl,
				data: { "action": "load_property", 'property_id': self.properties.currentProperty },
				success: function success(response) {

					self.properties.propertyData = response.data;
					self.properties.defProp.resolve('Loaded');
				}
			});
		},

		parseHtml: function parseHtml() {

			var elems = [];

			$.each($.parseHTML(this.propertyData.html), function (idx, elm) {

				if (elm.nodeType != 3 && elm.className != 'scroll-menu__anchor' && elm.className != 'breadcrumb') {
					elems.push(elm);
				}
			});

			return elems;
		},

		attachNewHtml: function attachNewHtml() {

			var $html = this.parseHtml();

			$('.top-section--image').html($html[0].innerHTML);
			$('section.property-item').html($html[1].innerHTML);
			$('section.property-connections').html($html[2].innerHTML);
		},

		propertyLoaded: function propertyLoaded() {

			this.isLoading = false;
			this.bindClickEvents();
			this.attachNewHtml();
			this.hideLoadingScreen();
			this.animateInCurrent();
			this.scrollReveal();
			this.setPropertyTitle(this.propertyData.title);
		},

		scrollReveal: function scrollReveal() {

			ScrollReveal().reveal('.property-detail-item', {
				duration: 1400,
				distance: '5%'
			});
		}

	},

	/*
 * -------------------------------------------------------------
 */
	/*
  * ------------------------------------------------------------
  * Module for Smooth Scrolling on page!
  *
  * ------------------------------------------------------------
 */

	self.smoothScroll = {

		checkHash: function checkHash() {

			if (hashURL != "" && hashURL.length > 1) {

				this.smoothScrollTo(hashURL);
			}
		},

		smoothScrollTo: function smoothScrollTo(anchor) {
			var duration = 1500; //time (milliseconds) it takes to reach anchor point
			var targetY = $(anchor).offset().top - 74;
			$("html, body").animate({
				"scrollTop": targetY
			}, duration);
		}

	},

	/*
 * -------------------------------------------------------------
 */
	/*
  * ------------------------------------------------------------
  * Module for Sliders!
  *
  * ------------------------------------------------------------
 */

	self.sliders = function () {

		$('.top-section__slider').owlCarousel({
			items: 1,
			autoplay: true,
			loop: true,
			nav: true,
			animateOut: 'fadeOut',
			autoplayTimeout: 4500
		});

		$('.social-feed__slider').owlCarousel({
			items: 1,
			autoplay: true,
			loop: true,
			nav: false,
			animateOut: 'fadeOut',
			autoplayTimeout: 4500,
			autoplayHoverPause: true,
			mouseDrag: false,
			touchDrag: false,
			pullDrag: false
		});
	},

	/*
 * -------------------------------------------------------------
 */
	/*
  * ------------------------------------------------------------
  * Load-more related calls
  * ------------------------------------------------------------
 */

	self.overlay = {

		open: function open() {

			$('.default-overlay').addClass('is-active');
		},

		close: function close() {

			$('.default-overlay').removeClass('is-active');
		}

	},

	/*
 * -------------------------------------------------------------
 */
	/*
  * ------------------------------------------------------------
  * Load-more related calls
  * ------------------------------------------------------------
 */

	self.whitepaperModal = {

		$modal: $('.whitepaper-modal'),
		$clickables: $('.lead-form'),
		whitepaper: {
			'name': null,
			'url': null,
			'type': null
		},

		init: function init() {

			this.bindClicks();
		},

		loadHtml: function loadHtml() {

			$.ajax({
				type: 'POST',
				dataType: 'JSON',
				url: vektor.ajaxurl,
				data: { 'action': 'whitepaper_modal_html', 'whitepaper': self.whitepaperModal.whitepaper },
				success: function success(response) {

					self.whitepaperModal.$modal.html(response.data);
					self.whitepaperModal.setupFormListener();
				}
			});
		},

		setupFormListener: function setupFormListener() {

			$('.close-modal').on('click', function (e) {
				e.preventDefault();

				if (self.whitepaperModal.isOpen()) self.whitepaperModal.close();
			});

			$('.whitepaper-download-button').on('click', function (e) {
				//e.preventDefault();

				if (self.whitepaperModal.isOpen()) self.whitepaperModal.close();
			});

			$('#whitepaper-form').on('submit', function (e) {
				e.preventDefault();

				$('.whitepaper-errors').hide();

				var data = $(this).serialize();

				data += '&action=save_whitepaper_lead&security=' + $('#_nonce').val() + '&whitepaper=' + self.whitepaperModal.whitepaper.name;

				$.ajax({
					type: 'POST',
					dataType: 'JSON',
					url: vektor.ajaxurl,
					data: data,
					success: function success(response) {

						if (response.success == true) {

							$('#whitepaper-form').hide();
							$('.thanks-message').show();
							$('a.whitepaper-download-button').attr('href', self.whitepaperModal.whitepaper.url);
							$('a.whitepaper-download-button').show();
						} else {

							$('.whitepaper-errors').show();
						}
					}
				});
			});
		},

		bindClicks: function bindClicks() {

			this.$clickables.on('click', function (e) {

				e.preventDefault();

				self.whitepaperModal.whitepaper.name = $(this).attr('data-name');
				self.whitepaperModal.whitepaper.url = '/wp-content/uploads/' + $(this).data('file');
				self.whitepaperModal.whitepaper.type = $(this).attr('data-type');

				if (!self.whitepaperModal.whitepaper.name) {
					self.whitepaperModal.whitepaper.name = $(this).find('a').attr('data-name');
					self.whitepaperModal.whitepaper.url = '/wp-content/uploads/' + $(this).find('a').data('file');
					self.whitepaperModal.whitepaper.type = $(this).find('a').attr('data-type');
				}

				self.whitepaperModal.open();
				self.whitepaperModal.loadHtml();
			});

			$('html').on('keyup', function (e) {

				if (e.keyCode == 27 && self.whitepaperModal.isOpen()) {
					self.whitepaperModal.close();
				}
			});
		},

		isOpen: function isOpen() {

			return this.$modal.hasClass('is-active');
		},

		open: function open() {

			self.overlay.open();
			this.$modal.addClass('is-active');
		},

		close: function close() {

			self.overlay.close();
			this.$modal.removeClass('is-active');
			this.$modal.html('');
		}

	},

	/*
 * -------------------------------------------------------------
 */
	/*
  * ------------------------------------------------------------
  * Newsletter Signup
  * ------------------------------------------------------------
 */

	self.newsletterSignup = {

		$email: $('#newsletter_signup_email'),
		$button: $('#newsletter_signup'),

		init: function init() {

			this.bindClickEvents();
		},

		bindClickEvents: function bindClickEvents() {

			this.$button.on('click', function (e) {
				e.preventDefault();

				$('#consent-msg').remove();
				if ($('#gdpr_checkbox').is(':checked')) {
					self.newsletterSignup.signUp();
				} else {
					$('.newsletter-error-message').html('<p id="consent-msg">You did not give your consent for us to store your email</p>').show();
				}
			});
		},

		signUp: function signUp() {

			$('.newsletter-error-message').hide();
			$('.newsletter-success-message').hide();

			$.ajax({
				type: 'POST',
				dataType: 'JSON',
				url: vektor.ajaxurl,
				data: { "action": "newsletter_signup", "security": $('#_newsletter_nonce').val(), "email": $('#newsletter_signup_email').val(), "consent": $('#gdpr_checkbox').is(":checked") },
				success: function success(response) {

					if (response.success == true) {
						$('.signup-wrap').slideUp();
						$('.newsletter-success-message').slideDown();
					} else {
						$('.newsletter-error-message').html(response.data).show();
					}
				}
			});
		}

	},

	/*
 * -------------------------------------------------------------
 */
	/*
  * ------------------------------------------------------------
  * Module for Scroll menu!
  *
  * ------------------------------------------------------------
 */

	self.scrollMenus = function () {

		var newMenuScroll;

		var $srollBtn = $('.scroll-menu__nav'),
		    $srollLeftBtn = $('.scroll-menu__nav--left'),
		    $srollRighBtn = $('.scroll-menu__nav--right');

		$srollBtn.on('click', function () {

			var $scrollMenu = $(this).parent('.scroll-menu'),
			    $scrollMenuInner = $scrollMenu.find('.scroll-menu__inner'),
			    currMenuScroll = $scrollMenuInner.scrollLeft(),
			    scrollWidth = $scrollMenu.width();

			if ($(this).is($srollLeftBtn)) {

				newMenuScroll = currMenuScroll - scrollWidth * 0.75;
			} else {

				newMenuScroll = currMenuScroll + scrollWidth * 0.75;
			}

			$scrollMenuInner.animate({
				scrollLeft: newMenuScroll
			}, 250);
		});
	}, self.scrollMenuDisplay = function () {

		if ($body.find('.scroll-menu').length > 0) {

			var $scrollMenu = $('.scroll-menu'),
			    $scrollMenuInner = $scrollMenu.find('.scroll-menu__inner'),
			    scrollTotalWidth = $scrollMenuInner[0].scrollWidth;

			if (scrollTotalWidth > $win.width()) {

				$scrollMenu.addClass('has-scroll-nav');
			} else {

				$scrollMenu.removeClass('has-scroll-nav');
			}
		}
	}, self.scrollMenuSticky = function (currScroll) {

		if ($body.find('.scroll-menu').length > 0) {

			var scrollMenu = $('.scroll-menu'),
			    scrollMenuOffset = $('.scroll-menu__anchor').offset(),
			    headersHeight = $header.height();

			if (currScroll >= scrollMenuOffset.top - headersHeight) {

				scrollMenu.addClass('is-fixed').css('top', headersHeight);
				$('.scroll-menu__anchor').height(scrollMenu.height());
			} else {

				scrollMenu.removeClass('is-fixed').css('top', 0);
			}
		}
	};

	/*
 * -------------------------------------------------------------
 */
	/*
  * ------------------------------------------------------------
  * Module for Maps
  * 
  * ------------------------------------------------------------
 */

	self.gMaps = function () {

		if ($('body').find('#main-map').length > 0) {

			var lat = $('#main-map').attr('data-lat'),
			    lng = $('#main-map').attr('data-lng');

			// Initialize map
			var map = new google.maps.Map(document.getElementById('main-map'), {
				zoom: 8,
				scrollwheel: false,
				center: new google.maps.LatLng(lat, lng),
				disableDefaultUI: true
			});

			// Map styling
			var mapStyle = new google.maps.StyledMapType([{ "featureType": "water", "elementType": "geometry.fill", "stylers": [{ "color": "#d3d3d3" }] }, { "featureType": "transit", "stylers": [{ "color": "#808080" }, { "visibility": "off" }] }, { "featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{ "visibility": "on" }, { "color": "#b3b3b3" }] }, { "featureType": "road.highway", "elementType": "geometry.fill", "stylers": [{ "color": "#ffffff" }] }, { "featureType": "road.local", "elementType": "geometry.fill", "stylers": [{ "visibility": "on" }, { "color": "#ffffff" }, { "weight": 1.8 }] }, { "featureType": "road.local", "elementType": "geometry.stroke", "stylers": [{ "color": "#d7d7d7" }] }, { "featureType": "poi", "elementType": "geometry.fill", "stylers": [{ "visibility": "on" }, { "color": "#ebebeb" }] }, { "featureType": "administrative", "elementType": "geometry", "stylers": [{ "color": "#a7a7a7" }] }, { "featureType": "road.arterial", "elementType": "geometry.fill", "stylers": [{ "color": "#ffffff" }] }, { "featureType": "road.arterial", "elementType": "geometry.fill", "stylers": [{ "color": "#ffffff" }] }, { "featureType": "landscape", "elementType": "geometry.fill", "stylers": [{ "visibility": "on" }, { "color": "#efefef" }] }, { "featureType": "road", "elementType": "labels.text.fill", "stylers": [{ "color": "#696969" }] }, { "featureType": "administrative", "elementType": "labels.text.fill", "stylers": [{ "visibility": "on" }, { "color": "#737373" }] }, { "featureType": "poi", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi", "elementType": "labels", "stylers": [{ "visibility": "off" }] }, { "featureType": "road.arterial", "elementType": "geometry.stroke", "stylers": [{ "color": "#d6d6d6" }] }, { "featureType": "road", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, {}, { "featureType": "poi", "elementType": "geometry.fill", "stylers": [{ "color": "#dadada" }] }]);
			map.mapTypes.set('styled_map', mapStyle);
			map.setMapTypeId('styled_map');

			var path = 'http://sthlmdataparks.vgdev.se/wp-content/themes/sthlmdataparks/assets/images/map-pin-',
			    pins = $('#main-map').data('pins');

			// Iterate over pins and attach them to the map
			$.each(pins, function (idx, pin) {

				var LatLng = new google.maps.LatLng(pin.address.lat, pin.address.lng);

				var pos = idx == 0 ? '--align-left' : '--align-right';

				var marker = new MarkerWithLabel({
					position: LatLng,
					map: map,
					labelClass: 'main-map-label ' + pos,
					labelContent: pin.label,
					icon: path + pin.pin_color + '.png'
				});

				marker.addListener('click', function () {
					window.location.href = pin.permalink;
				});
			});
		}
	},

	/*
 * -------------------------------------------------------------
 */
	/*
  * ------------------------------------------------------------
  * PhotoSwipe related stuff to this site
  * ------------------------------------------------------------
 */

	self.photoSwipe = {

		init: function init() {

			// if ($('.gallery').length > 0) {

			// 	$('.gallery').each(function() {

			// 		var pswpElement = $('.pswp')[0];
			// 		var items = [];

			// 		$('.gallery__image', this).each(function() {
			// 			var image = $(this);
			// 			var size = image.data('size').split('x');
			// 			items.push({ src: image.attr('href'), w: parseInt(size[0]), h: parseInt(size[1]) });
			// 		});

			// 		console.log(items);

			// 		var options = {
			// 		  index: 0 // start at first slide
			// 		};

			// 		// Initializes and opens PhotoSwipe
			// 		var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
			// 		gallery.init();

			// 	});
			// }


			$('.gallery .gallery__image').on('click', function (e) {

				e.preventDefault();

				var clickedIndex = parseInt($(this).data('index'));
				var pswpElement = $('.pswp')[0];
				var items = [];

				var gallery = $(this).closest('.gallery');

				$('.gallery__image', gallery).each(function () {
					var image = $(this);
					var size = image.data('size').split('x');
					var imageCaption = image.data('caption');

					items.push({ src: image.attr('href'), w: parseInt(size[0]), h: parseInt(size[1]), title: imageCaption });
				});

				var options = {
					index: clickedIndex,
					closeOnScroll: false
				};

				// Initializes and opens PhotoSwipe
				var pswpGallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
				pswpGallery.init();
			});
		}

	},

	/*
 * -------------------------------------------------------------
 */

	/*
 	 * ------------------------------------------------------------
 	 * Module for making DOM Element clickable. 
 	 * target link must have class="clickable_link_source" and wrapping Dom element should have class="clickable"
 	 * 
 	 * ------------------------------------------------------------
 	*/

	self.makeDivClickable = {

		init: function init() {
			this.makeDomElementsClickable();
		},

		makeDomElementsClickable: function makeDomElementsClickable() {

			$('.clickable').click(function () {

				var the_link;

				the_link = $(this).find(".clickable_link_source").attr("href");

				if (the_link) {
					window.location = the_link;
					return false;
				}
			});
		}

		/*
  * -------------------------------------------------------------
  */

		/*
  	 * ------------------------------------------------------------
  	 * Module for Social Sharing 
  	 * 
  	 * ------------------------------------------------------------
  	*/

	};self.socialShare = {

		init: function init() {
			this.socialShareModule();
		},

		socialShareModule: function socialShareModule() {

			$('.social-link').on('click', function (e) {

				if ($(this).hasClass('mail')) return;

				e.preventDefault();

				var Networks = {
					facebook: 'https://www.facebook.com/sharer.php?u={url}',
					twitter: 'https://twitter.com/intent/tweet?url={url}',
					linkedin: 'https://www.linkedin.com/shareArticle?mini=true&url={url}'
				};

				var url = Networks[$(this).data('type')],
				    url = url.replace(new RegExp('{url}', 'g'), encodeURIComponent($(this).data('href')));

				var width = 800,
				    height = 500,
				    px = Math.floor(((screen.availWidth || 1024) - width) / 2),
				    py = Math.floor(((screen.availHeight || 700) - height) / 2);

				var popup = window.open(url, "social", "width=" + width + ",height=" + height + ",left=" + px + ",top=" + py + ",location=0,menubar=0,toolbar=0,status=0,scrollbars=1,resizable=1");

				if (popup) {
					popup.focus();
					if (e.preventDefault) e.preventDefault();
					e.returnValue = false;
				}
			});
		}

		/*
  * -------------------------------------------------------------
  */

	};self.init = function () {

		// Run resize events once on load.
		self.resizeActions();

		self.bindWindowEvents();

		self.smoothScroll.checkHash();

		self.sliders();

		self.gMaps();

		self.scrollMenus();

		self.newsFeed.init();

		self.photoSwipe.init();

		self.whitepaperModal.init();

		self.newsletterSignup.init();

		if ($('body').hasClass('single-properties')) self.properties.init();

		self.makeDivClickable.init();

		self.socialShare.init();
	};
};
if (typeof jQuery(document).foundation == 'function') {
	jQuery(document).foundation();
}
/* 
These functions make sure WordPress 
and Foundation play nice together.
*/

jQuery(document).ready(function () {

	// Remove empty P tags created by WP inside of Accordion and Orbit
	jQuery('.accordion p:empty, .orbit p:empty').remove();

	// Makes sure last grid item floats left
	jQuery('.archive-grid .columns').last().addClass('end');

	// Adds Flex Video to YouTube and Vimeo Embeds
	jQuery('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]').wrap("<div class='flex-video'/>");

	var app = new App(window.jQuery);
	app.init();
});