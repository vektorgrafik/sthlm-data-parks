(function( $, window, undefined ) {

    $.vektor_load_more = function( element, options, selector ) {

        var defaults = {
            'show_remaining' : false
        }

        var plugin = this,
        	selector = selector;

        plugin.settings = {}

        var $element = $(element),
             element = element;

        plugin.init = function() {

            plugin.settings = $.extend({}, defaults, options);

           	plugin.bindClickEvent( $element, selector );

        },

        plugin.bindClickEvent = function( $element, selector ) {

        	$element.off('click');

        	if( ! $element.hasClass('is-loading-more')) {

	        	$element.on('click', function(e) {
					e.preventDefault();
					var $btn = $element;

					$(this).off('click');

					$btn.addClass('is-loading-more');

					$.ajax({
						type: "POST",
						url: vektor.ajaxurl,
						data: {
							"action" 	: "vektor_load_more",
							"post_type" : $btn.data('post-type'),
							"offset" 	: $btn.data('offset'),
							"num_posts" : $btn.data('num-posts'),
							"template" 	: $btn.data('template')
						},
						success: function( response ) {

							var $target = $($btn.data('append-to'));
							$target.append( response.data.html );

							var cur_offset = parseInt( $btn.data('offset')),
								new_offset = cur_offset + parseInt( $btn.data('increment'));

							$btn.remove();

							if( response.data.hide_button == true ) {

								$target.find(selector).remove();

							} else {

								$target.find(selector).attr('data-offset', new_offset);

								if( plugin.settings.show_remaining == true ) {
									$target.find(selector).find('span').html( response.data.remaining_posts );
								}

								$(selector).vektor_load_more(plugin.settings);

							}
						}
					});
	        	});
        	}

        }

        plugin.init();

    }

    $.fn.vektor_load_more = function( options ) {

    	var selector = this.selector;

        return this.each(function() {
            if (undefined == $(this).data('vektor_load_more')) {
                var plugin = new $.vektor_load_more(this, options, selector);
                $(this).data('vektor_load_more', plugin);
            }
        });

    }

})(jQuery, window);
