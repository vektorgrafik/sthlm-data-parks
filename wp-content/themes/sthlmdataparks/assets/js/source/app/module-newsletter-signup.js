	/*
	 * ------------------------------------------------------------
	 * Newsletter Signup
	 * ------------------------------------------------------------
	*/

    self.newsletterSignup = {

        $email: $('#newsletter_signup_email'),
        $button: $('#newsletter_signup'),

        init: function init() {

            this.bindClickEvents();
        },

        bindClickEvents: function bindClickEvents() {

            this.$button.on('click', function (e) {
                e.preventDefault();

                $('#consent-msg').remove();
                if($('#gdpr_checkbox').is(':checked')){
                    self.newsletterSignup.signUp();
                }else{
                    $('.newsletter-error-message').html('<p id="consent-msg">You did not give your consent for us to store your email</p>').show();

                }
            });
        },

        signUp: function signUp() {

            $('.newsletter-error-message').hide();
            $('.newsletter-success-message').hide();

            $.ajax({
                type: 'POST',
                dataType: 'JSON',
                url: vektor.ajaxurl,
                data: { "action": "newsletter_signup", "security": $('#_newsletter_nonce').val(), "email": $('#newsletter_signup_email').val(),"consent": $('#gdpr_checkbox').is(":checked")},
                success: function success(response) {

                    if (response.success == true) {
                        $('.signup-wrap').slideUp();
                        $('.newsletter-success-message').slideDown();
                    } else {
                        $('.newsletter-error-message').html(response.data).show();
                    }
                }
            });
        }

    },

	/*
	* -------------------------------------------------------------
	*/