	/*
	 * ------------------------------------------------------------
	 * Newsfeed
	 * ------------------------------------------------------------
	*/

	self.newsFeed = {

		init: function() {
			
			this.bindClickEvents();

		},

		bindClickEvents: function() {
			
			$('#load-more-news').off('click');
			$('#load-more-news').on('click', function(e) {

				e.preventDefault();

				var $btn = $(this),
					offset = $btn.attr('data-offset');

				$btn.addClass('--is-loading');

				$.ajax({
					type: 'POST',
					dataType: 'JSON',
					url: vektor.ajaxurl,
					data: {"action" : "load_more_news", "offset" : offset },
					success: function( response ) {

						$btn.removeClass('--is-loading');
						
						// Attach HTML to DOM
						var $item = $( response.data.html );

						$item.addClass('animate-in')
							 .appendTo($('.news-feed'));

						// Update offset
						$btn.attr('data-offset', response.data.offset );

						// Hide button if no more posts to show
						if( response.data.hide_button )
							$btn.hide();

						// Rebind buttons
						self.newsFeed.bindClickEvents();

					}
				});

			});

			$('.news-article__toggle').off('click');
			$('.news-article__toggle').on('click', function(e) {

				var $bth = $(this),
					$post = $( '#' + $(this).attr("data-post-id") ),
					$postContent = $post.children('.news-article__content'),
					$postHeader = $post.children('.news-article__header'),
					postOffset = $post.offset(),
					headerHeight = $header.height();

				$post.addClass('is-animating');

				if ( $post.hasClass('is-expanded') ) {

					$post.removeClass('is-expanded');
					
					$postHeader.find('.social-share').hide();
					
					$postContent.slideUp();
					$postHeader.slideUp();

					setTimeout(function(){ 

						$postHeader.slideDown();
						$post.removeClass('is-animating');

					}, 800);


				} else {

					setTimeout(function(){ 

						$post.addClass('is-expanded');

						$post.removeClass('is-animating');
						
						$postHeader.find('.social-share').show();

						$postContent.slideDown();
						$postHeader.slideDown();

					}, 450);

				}

				$("html, body").animate({ 
						scrollTop: ( postOffset.top - headerHeight )

					}, '500', function(){

				});
				
			});

		},

	},

	/*
	* -------------------------------------------------------------
	*/