	/*
	 * ------------------------------------------------------------
	 * Module for Scroll menu!
	 *
	 * ------------------------------------------------------------
	*/

	self.scrollMenus = function() {

		var newMenuScroll;

		var $srollBtn = $('.scroll-menu__nav'),
			$srollLeftBtn = $('.scroll-menu__nav--left'),
			$srollRighBtn = $('.scroll-menu__nav--right');

		$srollBtn.on('click', function(){

			var $scrollMenu = $(this).parent('.scroll-menu'),
				$scrollMenuInner = $scrollMenu.find('.scroll-menu__inner'),
				currMenuScroll = $scrollMenuInner.scrollLeft(),
				scrollWidth = $scrollMenu.width();
			
			if ( $(this).is( $srollLeftBtn ) ) {
				
				newMenuScroll = currMenuScroll - ( scrollWidth * 0.75 );

			} else {
				
				newMenuScroll = currMenuScroll + ( scrollWidth * 0.75 );

			}

			$scrollMenuInner.animate({
				scrollLeft: newMenuScroll
			}, 250 );

 
		});

	},

	self.scrollMenuDisplay = function() {

		if ( $body.find('.scroll-menu').length > 0 ) {

			var $scrollMenu = $('.scroll-menu'),
				$scrollMenuInner = $scrollMenu.find('.scroll-menu__inner'),
				scrollTotalWidth = $scrollMenuInner[0].scrollWidth;

			if ( scrollTotalWidth > $win.width() ) {

				$scrollMenu.addClass('has-scroll-nav');
				
			} else {

				$scrollMenu.removeClass('has-scroll-nav');

			}

		}

	},

	self.scrollMenuSticky = function( currScroll ) {

		if ( $body.find('.scroll-menu').length > 0 ) {

			var scrollMenu = $('.scroll-menu'),
				scrollMenuOffset = $('.scroll-menu__anchor').offset(),
				headersHeight = $header.height();

			if ( currScroll >= ( scrollMenuOffset.top - headersHeight ) ) {

				scrollMenu.addClass('is-fixed').css('top', headersHeight);
				$('.scroll-menu__anchor').height( scrollMenu.height() );

			} else {

				scrollMenu.removeClass('is-fixed').css('top', 0);

			}

		}

	}

	/*
	* -------------------------------------------------------------
	*/