/*
 * App
 */

var App = function( jQuery ) {
	
	var self = this,
		$ = jQuery,
		$win = $(window),
		$body = $('body'),
		$header = $('.page-header'),
		currentScroll,
		lastScroll,
		headerHeight = $header.height(),
		hashURL = location.hash;