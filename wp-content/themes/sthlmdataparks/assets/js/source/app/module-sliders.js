	/*
	 * ------------------------------------------------------------
	 * Module for Sliders!
	 *
	 * ------------------------------------------------------------
	*/

	self.sliders = function() {

		$('.top-section__slider').owlCarousel({
		    items: 1,
		    autoplay: true,
		    loop: true,
		    nav: true,
		    animateOut: 'fadeOut',
		    autoplayTimeout: 4500
		});

		$('.social-feed__slider').owlCarousel({
			items: 1,
		    autoplay: true,
		    loop: true,
		    nav: false,
		    animateOut: 'fadeOut',
		    autoplayTimeout: 4500,
		    autoplayHoverPause:true,
		    mouseDrag: false,
		    touchDrag: false,
		    pullDrag: false
		});

	},

	/*
	* -------------------------------------------------------------
	*/