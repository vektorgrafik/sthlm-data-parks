	/*
	 * ------------------------------------------------------------
	 * Properties
	 * ------------------------------------------------------------
	*/

	self.properties = {

		/**
		 * Local variables
		 */
		isLoading: false,
		propertyTitle: '',
		propertyData: null,
		defProp: $.Deferred(),
		currentProperty: null,
		initialPropertyURI: '',

		init: function() {
			
			this.setInitialProperty();
			this.setInitialPropertyURI();
			this.bindClickEvents();
			this.scrollReveal();
			this.popHistoryState();
			this.setInitialHistoryState();
			this.setPropertyTitle();

		},

		/**
		 * Gets attribute on active anchor element
		 * and sets it to our local variable
		 *
		 * @return Int
		 */
		setInitialProperty: function() {

			this.currentProperty = parseInt( $('.property-list__link.is-active').data('property-id') );

		},

		setInitialPropertyURI: function() {

			this.initialPropertyURI = $('.property-list__link.is-active').attr('href');

		},

		bindClickEvents: function() {
			
			var $navElements = $('.property-list__link.js-ajax');

			$navElements.off('click');
			$navElements.on('click', function( e ) {

				e.preventDefault();

				if( self.properties.isLoading )
					return false;

				var propertyId = $(this).data('property-id');

				if( ! self.properties.isCurrentProperty( propertyId )) {
					self.properties.setHistoryState( propertyId, $(this).attr('href') );
					self.properties.switchProperty( propertyId );
				}

			});

		},

		setPropertyTitle: function( title ) {
			
			this.propertyTitle = ( title ) ? title : $('title').html()
			$('title').html( this.propertyTitle );

		},

		setInitialHistoryState: function() {

			var state = {
				'propertyId': this.currentProperty,
				'title': this.propertyTitle
			};

			history.pushState(state, state.title, this.initialPropertyURI );

		},

		setHistoryState: function( propertyId, uri ) {

			var state = {
				'propertyId': propertyId
			};

			history.pushState( state, 'test', uri );

		},

		/**
		 * { popHistoryState }
		 *
		 * The popstate event is fired when the user
		 * clicks either forward or back in their 
		 * browser. So we simply switch the property
		 * based on the ID stored in the history API.
		 * 
		 * @return Void
		 */
		popHistoryState: function() {

			window.addEventListener('popstate', function(e) {

				self.properties.switchProperty( e.state.propertyId );

			});

		},

		setCurrentProperty: function( propertyId ) {
			
			this.currentProperty = propertyId;
			this.updateNavigation();

		},

		updateNavigation: function() {

			$('.property-list__link').removeClass('is-active');
			$('.property-list__link[data-property-id="' + this.currentProperty + '"]').addClass('is-active');

		},

		isCurrentProperty: function( propertyId ) {
			
			return ( propertyId == this.currentProperty );
		
		},

		resetDeferredElement: function() {
			
			this.defProp = $.Deferred();
		
		},

		animateOutCurrent: function() {

			$('.top-section__bg').removeClass('animate-in').addClass('animate-out');
			$('.property-item').removeClass('animate-in-alternate').addClass('animate-out-alternate');

		},

		animateInCurrent: function() {

			$('.top-section__bg').removeClass('animate-out').addClass('animate-in');
			$('.property-item').removeClass('animate-out-alternate').addClass('animate-in-alternate');

		},

		showLoadingScreen: function() {

			var $elm = $('<div></div>'),
				$s1 = $('<div></div>'),
				$s2 = $('<div></div>'),
				$spinner = $('<div></div>');

			$elm.addClass('loading-overlay');

			$s1.addClass('loading-overlay__section').addClass('loading-overlay__section--left');
			$s2.addClass('loading-overlay__section').addClass('loading-overlay__section--right');

			$spinner.addClass('spinner');
			$spinner.html('<span></span>');

			$s1.appendTo( $elm );
			$s2.appendTo( $elm );
			$spinner.appendTo( $elm );

			$elm.insertAfter('body');

			setTimeout(function() {
				$elm.addClass('start-anim');
			}, 150, $elm);

		},

		hideLoadingScreen: function() {

			var $elm = $('.loading-overlay');

			$elm.removeClass('start-anim');

			setTimeout(function() {
				$elm.remove();
			}, 750, $elm);

		},

		switchProperty: function( propertyId ) {

			if( ! propertyId )
				return false;

			this.isLoading = true;
			this.animateOutCurrent();
			this.showLoadingScreen();
			this.resetDeferredElement();
			this.setCurrentProperty( propertyId );
			this.loadPropertyData();

			$.when(

				self.properties.defProp

			).done(function() {

				self.properties.propertyLoaded();
			
			});

		},

		loadPropertyData: function() {

			$.ajax({
				type: 'POST',
				dataType: 'JSON',
				url: vektor.ajaxurl,
				data: {"action" : "load_property", 'property_id' : self.properties.currentProperty },
				success: function( response ) {
					
					self.properties.propertyData = response.data;
					self.properties.defProp.resolve( 'Loaded' );

				}
			});

		},

		parseHtml: function() {

			var elems = [];

			$.each( $.parseHTML( this.propertyData.html ), function( idx, elm ) {

				if( elm.nodeType != 3 && elm.className != 'scroll-menu__anchor' && elm.className != 'breadcrumb' ) {
					elems.push(elm);
				}

			});

			return elems;

		},

		attachNewHtml: function() {
			
			var $html = this.parseHtml();

			$('.top-section--image').html( $html[0].innerHTML );
			$('section.property-item').html( $html[1].innerHTML );
			$('section.property-connections').html( $html[2].innerHTML );

		},

		propertyLoaded: function() {
			
			this.isLoading = false;
			this.bindClickEvents();
			this.attachNewHtml();
			this.hideLoadingScreen();
			this.animateInCurrent();
			this.scrollReveal();
			this.setPropertyTitle( this.propertyData.title );

		},

		scrollReveal: function() {

			ScrollReveal().reveal('.property-detail-item', {
				duration: 1400,
				distance: '5%'
			});

		}

	},

	/*
	* -------------------------------------------------------------
	*/