	/*
	 * ------------------------------------------------------------
	 * Load-more related calls
	 * ------------------------------------------------------------
	*/

	self.whitepaperModal =  {

		$modal: $('.whitepaper-modal'),
		$clickables: $('.lead-form'),
		whitepaper: {
			'name': null,
			'url': null,
			'type': null
		},

		init: function() {

			this.bindClicks();

		},

		loadHtml: function() {

			$.ajax({
				type: 'POST',
				dataType: 'JSON',
				url: vektor.ajaxurl,
				data: {'action' : 'whitepaper_modal_html', 'whitepaper' : self.whitepaperModal.whitepaper },
				success: function( response ) {
					
					self.whitepaperModal.$modal.html( response.data );
					self.whitepaperModal.setupFormListener();

				}
			});

		},

		setupFormListener: function() {

			$('.close-modal').on('click', function( e ) {
				e.preventDefault();

				if( self.whitepaperModal.isOpen() )
					self.whitepaperModal.close();

			});
			
			
			$('.whitepaper-download-button').on('click', function( e ) {
				//e.preventDefault();

				if( self.whitepaperModal.isOpen() )
					self.whitepaperModal.close();

			});
			
			

			$('#whitepaper-form').on('submit', function( e ) {
				e.preventDefault();
				
				$('.whitepaper-errors').hide();

				var data = $(this).serialize();

				data += '&action=save_whitepaper_lead&security=' + $('#_nonce').val() + '&whitepaper=' + self.whitepaperModal.whitepaper.name;

				$.ajax({
					type: 'POST',
					dataType: 'JSON',
					url: vektor.ajaxurl,
					data: data,
					success: function( response ) {
						
						if( response.success == true ) {

							$('#whitepaper-form').hide();
							$('.thanks-message').show();
							$('a.whitepaper-download-button').attr('href', self.whitepaperModal.whitepaper.url);
							$('a.whitepaper-download-button').show();

						} else {

							$('.whitepaper-errors').show();

						}

					}
				});

			});

		},

		bindClicks: function() {

			this.$clickables.on('click', function( e ) {

				e.preventDefault();

				self.whitepaperModal.whitepaper.name = $(this).attr('data-name');				
				self.whitepaperModal.whitepaper.url = '/wp-content/uploads/' + $(this).data('file');				
				self.whitepaperModal.whitepaper.type = $(this).attr('data-type');

				if( !self.whitepaperModal.whitepaper.name ) {
					self.whitepaperModal.whitepaper.name = $(this).find('a').attr('data-name');					
					self.whitepaperModal.whitepaper.url = '/wp-content/uploads/' + $(this).find('a').data('file');					
					self.whitepaperModal.whitepaper.type = $(this).find('a').attr('data-type');					
				}

				self.whitepaperModal.open();
				self.whitepaperModal.loadHtml();

			});

			$('html').on('keyup', function( e ) {

				if( e.keyCode == 27 && self.whitepaperModal.isOpen() ) {
					self.whitepaperModal.close();
				}

			});

		},

		isOpen: function() {

			return this.$modal.hasClass('is-active');

		},

		open: function() {

			self.overlay.open();
			this.$modal.addClass('is-active');

		},

		close: function() {

			self.overlay.close();
			this.$modal.removeClass('is-active');
			this.$modal.html('');

		},

	},

	/*
	* -------------------------------------------------------------
	*/