	self.resizeActions = function() {

		

	},

	self.bindWindowEvents = function() {

		// Unthrottled events
		$(window).on('scroll', self.unthrottledScrollActions );

		// Throttled events
		$(window).on('scroll', $.throttle(50, self.throttledScrollActions ));
		$(window).on('resize', $.debounce(250, self.resizeActions ));

	},

	self.unthrottledScrollActions = function() {



	},

	self.throttledScrollActions = function() {

		currentScroll = $win.scrollTop();

		if( currentScroll > headerHeight ) {
			$body.addClass('state--scrolled');
		} else {
			$body.removeClass('state--scrolled');
		}
		
		if( currentScroll > lastScroll ) {
			
			$body.addClass('state--scrolled-down');
			$body.removeClass('state--scrolled-up');
			
		} else {
			
			if( currentScroll < lastScroll - 10 ) {
				$body.removeClass('state--scrolled-down');
				$body.addClass('state--scrolled-up');
			}

		}
		
		lastScroll = currentScroll;

		if( currentScroll >= ( $(window).height() -200 )) {
			$body.addClass('state--scrolled-past-height');
		} else {
			$body.removeClass('state--scrolled-past-height');
		}

		self.scrollMenuSticky( currentScroll );

	},

	self.resizeActions = function() {

		self.scrollMenuDisplay();

	}