	/*
	 * ------------------------------------------------------------
	 * PhotoSwipe related stuff to this site
	 * ------------------------------------------------------------
	*/

	self.photoSwipe = {

		init: function() {

			// if ($('.gallery').length > 0) {

			// 	$('.gallery').each(function() {

			// 		var pswpElement = $('.pswp')[0];
			// 		var items = [];

			// 		$('.gallery__image', this).each(function() {
			// 			var image = $(this);
			// 			var size = image.data('size').split('x');
			// 			items.push({ src: image.attr('href'), w: parseInt(size[0]), h: parseInt(size[1]) });
			// 		});

			// 		console.log(items);

			// 		var options = {
			// 		  index: 0 // start at first slide
			// 		};

			// 		// Initializes and opens PhotoSwipe
			// 		var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
			// 		gallery.init();

			// 	});
			// }


			$('.gallery .gallery__image').on('click', function(e) {

			    e.preventDefault();

			    var clickedIndex = parseInt($(this).data('index'));
			    var pswpElement = $('.pswp')[0];
			    var items = [];

			    var gallery = $(this).closest('.gallery');

			    $('.gallery__image', gallery).each(function() {
			      var image = $(this);
			      var size = image.data('size').split('x');
			      var imageCaption = image.data('caption');

			      items.push({ src: image.attr('href'), w: parseInt(size[0]), h: parseInt(size[1]), title: imageCaption });
			    });

			    var options = {
			      index: clickedIndex,
			      closeOnScroll: false
			    };

			    // Initializes and opens PhotoSwipe
			    var pswpGallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
			    pswpGallery.init();

			});

		},

	},

	/*
	* -------------------------------------------------------------
	*/
