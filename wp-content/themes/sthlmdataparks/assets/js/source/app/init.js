	self.init = function() {

		// Run resize events once on load.
		self.resizeActions();

		self.bindWindowEvents();

		self.smoothScroll.checkHash();

		self.sliders();

		self.gMaps();

		self.scrollMenus();

		self.newsFeed.init();
		
		self.photoSwipe.init();

		self.whitepaperModal.init();

		self.newsletterSignup.init();

		if( $('body').hasClass('single-properties') )
			self.properties.init();
			
		self.makeDivClickable.init();
		
		self.socialShare.init();

	}