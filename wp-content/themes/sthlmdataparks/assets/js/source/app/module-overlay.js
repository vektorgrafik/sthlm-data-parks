	/*
	 * ------------------------------------------------------------
	 * Load-more related calls
	 * ------------------------------------------------------------
	*/

	self.overlay =  {

		open: function() {

			$('.default-overlay').addClass('is-active');

		},

		close: function() {

			$('.default-overlay').removeClass('is-active');

		}

	},

	/*
	* -------------------------------------------------------------
	*/