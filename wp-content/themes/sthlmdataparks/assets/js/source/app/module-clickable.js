/*
	 * ------------------------------------------------------------
	 * Module for making DOM Element clickable. 
	 * target link must have class="clickable_link_source" and wrapping Dom element should have class="clickable"
	 * 
	 * ------------------------------------------------------------
	*/

	self.makeDivClickable = {
		
		init: function() {
			this.makeDomElementsClickable();
		},
		
		makeDomElementsClickable: function() {
		
			$('.clickable').click(function() {

				var the_link;
				
				the_link = $(this).find(".clickable_link_source").attr("href");
				
				if(the_link){
					window.location = the_link;
					return false;
				}
				
			});
	
		}		
		
	}
	

	/*
	* -------------------------------------------------------------
	*/

