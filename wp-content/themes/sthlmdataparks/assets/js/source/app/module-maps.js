	/*
	 * ------------------------------------------------------------
	 * Module for Maps
	 * 
	 * ------------------------------------------------------------
	*/

		self.gMaps = function() {

			if ( $('body').find('#main-map').length > 0 ) {

				var lat = $('#main-map').attr('data-lat'),
					lng = $('#main-map').attr('data-lng');
				
				// Initialize map
				var map = new google.maps.Map(document.getElementById('main-map'), {
				    zoom: 8,
				    scrollwheel: false,
				    center: new google.maps.LatLng(lat, lng),
				    disableDefaultUI: true
			    });

				// Map styling
				var mapStyle = new google.maps.StyledMapType(
					[{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#d3d3d3"}]},{"featureType":"transit","stylers":[{"color":"#808080"},{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"visibility":"on"},{"color":"#b3b3b3"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"weight":1.8}]},{"featureType":"road.local","elementType":"geometry.stroke","stylers":[{"color":"#d7d7d7"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#ebebeb"}]},{"featureType":"administrative","elementType":"geometry","stylers":[{"color":"#a7a7a7"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#efefef"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#696969"}]},{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"visibility":"on"},{"color":"#737373"}]},{"featureType":"poi","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"color":"#d6d6d6"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"color":"#dadada"}]}]
				);
			    map.mapTypes.set('styled_map', mapStyle);
			    map.setMapTypeId('styled_map');

			    var path = 'http://sthlmdataparks.vgdev.se/wp-content/themes/sthlmdataparks/assets/images/map-pin-',
					pins = $('#main-map').data('pins');

				// Iterate over pins and attach them to the map
				$.each(pins, function( idx, pin ) {

					var LatLng = new google.maps.LatLng( pin.address.lat, pin.address.lng );

					var pos = (idx==0) ? '--align-left' : '--align-right';

					var marker = new MarkerWithLabel({
						position: LatLng,
						map: map,
						labelClass: 'main-map-label ' + pos,
						labelContent: pin.label,
						icon: path + pin.pin_color + '.png'
					});

					marker.addListener('click', function() {
						window.location.href = pin.permalink;
					});		

				});

			}

		},

	/*
	* -------------------------------------------------------------
	*/