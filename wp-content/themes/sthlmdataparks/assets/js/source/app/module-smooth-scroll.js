	/*
	 * ------------------------------------------------------------
	 * Module for Smooth Scrolling on page!
	 *
	 * ------------------------------------------------------------
	*/

	self.smoothScroll = {

		checkHash: function() {

			if ( hashURL != "" && hashURL.length > 1 ){

		        this.smoothScrollTo(hashURL);
		    }

		},

	  	smoothScrollTo: function(anchor) {
		    var duration= 1500; //time (milliseconds) it takes to reach anchor point
		    var targetY = $(anchor).offset().top - 74;
		    $("html, body").animate({
		        "scrollTop" : targetY
		    }, duration );

		}

	},

	/*
	* -------------------------------------------------------------
	*/