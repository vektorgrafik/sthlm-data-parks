/*
	 * ------------------------------------------------------------
	 * Module for Social Sharing 
	 * 
	 * ------------------------------------------------------------
	*/

	self.socialShare = {
		
		init: function() {
			this.socialShareModule();
		},
		
		socialShareModule: function() {

			$('.social-link').on('click', function(e) {
	
				if( $(this).hasClass('mail'))
					return;
	
				e.preventDefault();
	
				var Networks = {
					facebook 	: 'https://www.facebook.com/sharer.php?u={url}',
					twitter 	: 'https://twitter.com/intent/tweet?url={url}',
					linkedin	: 'https://www.linkedin.com/shareArticle?mini=true&url={url}'
				};
	
				var url = Networks[$(this).data('type')],
					url = url.replace(new RegExp('{url}', 'g'), encodeURIComponent( $(this).data('href') ));
	
				var
					width = 800,
					height = 500,
					px = Math.floor(((screen.availWidth || 1024) - width) / 2),
					py = Math.floor(((screen.availHeight || 700) - height) / 2);
	
				var popup = window.open(url, "social",
					"width="+width+",height="+height+
					",left="+px+",top="+py+
					",location=0,menubar=0,toolbar=0,status=0,scrollbars=1,resizable=1");
	
				if ( popup ) {
					popup.focus();
					if (e.preventDefault) e.preventDefault();
					e.returnValue = false;
				}
	
			});
		}

	}

	/*
	* -------------------------------------------------------------
	*/
